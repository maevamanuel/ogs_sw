#ifndef ABSTRACTMODEL_H
#define ABSTRACTMODEL_H

/*! \class AbstractModel
 *  \brief Class representing the abstract model of the MVC
 */
class AbstractModel
{
public:
    /*!
     *  \brief Destructor
     */
    virtual ~AbstractModel(){};
};

#endif // ABSTRACTMODEL_H
