#ifndef ABSTRACTCONTROLLER_H
#define ABSTRACTCONTROLLER_H

#include "model.h"

/*! \class AbstractController
 *  \brief Class representing the abstract controller of the MVC
 */
class AbstractController
{
public:
    /*!
     *  \brief Constructor
     *  \param model : Model
     */
    AbstractController(Model model)
    {
        this->_model = model;
    }

protected:
    Model _model; /*!< Model member */
};

#endif // ABSTRACTCONTROLLER_H
