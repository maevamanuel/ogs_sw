﻿#include "view.h"
#include "model.h"
#include "controller.h"
#include "plog/Log.h"
#include "plog/Initializers/RollingFileInitializer.h"

#include <gtest/gtest.h>
#include <QApplication>

//////////////////////TEST TELESCOPE FUNCTIONS///////////////////////

TEST(file_tests, read_file_telescope) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(15, w.read_file_config());
}

TEST(user_level_tests, config_admin_display_telescope) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(0, w.config_admin().second);
}

//////////////////////TEST DOME FUNCTIONS///////////////////////////

TEST(file_tests, read_file_dome) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(12, w.read_file_config_dome());
}

TEST(user_level_tests, config_admin_display_dome) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(0, w.config_admin_dome().second);
}

//////////////////////TEST ACTUATORS FUNCTIONS/////////////////////

TEST(file_tests, read_file_backlash) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(6, w.read_file_backlash());
}

//////////////////////TEST CAMERA FUNCTIONS/////////////////////

TEST(file_tests, read_file_config_camera) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(11, w.read_file_config_camera());
}

/////////////////////TEST WEATHEAR FUNCTIONS///////////////////////

TEST(file_tests, read_file_weather) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(18, w.read_file_config_weather());
}

TEST(user_level_tests, config_admin_display_weather) {
    Model *model = new Model();
    Controller *controller = new Controller(*model);
    View w(*controller);
    ASSERT_EQ(0, w.buttons_weather().second);
}

//////////////////////////MAIN FUNCTION///////////////////////////

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    QApplication a(argc, argv);
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../logs_IHM.txt");
    plog::init(plog::debug, str1);
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the model of MVC";
    Model *model = new Model();
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the controller of MVC";
    Controller *controller = new Controller(*model);
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the view of MVC";
    View w(*controller);
    w.show();
    a.exec();
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "\n\n\n\n-----------GOOGLE TESTS---------\n\n\n";
    return RUN_ALL_TESTS();
}
