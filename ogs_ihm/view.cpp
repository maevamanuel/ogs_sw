﻿#include "view.h"

View::View(Controller controller, QWidget *parent)
    : QWidget(parent),
      _controller(controller)
{
    this->setWindowTitle("OGS IHM");
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up the widgets";
    set_tabWidgets();
    telescope_buttons();
    dome_buttons();
    actuator_buttons();
    actuator_buttons_M4_M5();
    camera_buttons();
    weather_buttons();
    position_buttons();
    imaging_buttons();

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Verify user level";
    verify_user_level();
    verify_user_level_dome();
    verify_user_level_actuator();
    verify_user_level_actuator_M4_M5();
    verify_user_level_camera();
    verify_user_level_weather();

    _timer_blinking_pub = new QTimer(this);
    _timer = new QTimer(this);
    _timer->start(1000);
    _timer_imaging = new QTimer(this);
    _timer_imaging->start(100);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Connnect signals to slots";
    QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(show_time()));
    QObject::connect(_timer_imaging, SIGNAL(timeout()), this,
                     SLOT(pos_cursor()));
    QObject::connect(_quit, SIGNAL(triggered()), qApp, SLOT(quit()));
    QObject::connect(_new, SIGNAL(triggered()), this,
                     SLOT(create_new_window()));

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up MQTT connection";
    map_command_payload_telescope();
    map_command_payload_dome();
    mqtt_subs_connect();

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up weather functionality";
    read_file_config_weather();
    _timer_weather = new QTimer(this);
    QObject::connect(_timer_weather, SIGNAL(timeout()), this,
                     SLOT(check_weather_conditions()));
    _timer_weather->start(DISPLAY_GRAPH_WEATHER);
}

////////////////////////////////IMAGING FUNCTIONS/////////////////////////

void View::pos_cursor()
{
    cursor_position = _cursor_image->pos();
    int xc = cursor_position.x() - CURSOR_PIXEL_TOP_X;
    int yc = cursor_position.y() - CURSOR_PIXEL_TOP_Y;
    QString str = "(" + QString::number(xc) + "," + QString::number(yc) + ")";
    QToolTip::showText(cursor_position, str, _imaging_tab, rect());
}

void View::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        lastPoint = event->pos();
}

void View::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
        endPoint = event->pos();
}

void View::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        endPoint = event->pos();
        /*QColor color(_imaging_captured.pixel(endPoint));
        _main_tabs->setPalette(QPalette(color));*/
        draw_rect();
        save_crop_image(QCoreApplication::applicationDirPath() +
                   "/../ogs_ihm/Images/new_image.jpg", "jpg");
    }
}

void View::draw_rect()
{
    QColor c(0, 255, 0, 255);
    int xc = lastPoint.x()-IMAGING_PIXEL_TOP_X;
    int yc = lastPoint.y()-IMAGING_PIXEL_TOP_Y;
    height_crop = abs(lastPoint.y()-endPoint.y());
    width_crop = abs(lastPoint.x()-endPoint.x());

    if((lastPoint.x() <= endPoint.x()) && (lastPoint.y() <= endPoint.y())) {
        for(int j = 0; j < width_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc + j, yc), c.rgb());
            _imaging_captured.setPixel(QPoint(xc + j, yc + height_crop),
                                       c.rgb());
        }

        for(int j = 0; j < height_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc, yc + j), c.rgb());
            _imaging_captured.setPixel(QPoint(xc + width_crop, yc + j),
                                       c.rgb());
        }
    }

    else if((lastPoint.x() > endPoint.x()) && (lastPoint.y() <= endPoint.y())){
        for(int j = 0; j < width_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc - j, yc), c.rgb());
            _imaging_captured.setPixel(QPoint(xc - j, yc + height_crop),
                                       c.rgb());
        }

        for(int j = 0; j < height_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc, yc + j), c.rgb());
            _imaging_captured.setPixel(QPoint(xc - width_crop, yc + j),
                                       c.rgb());
        }
    }

    else if((lastPoint.x() <= endPoint.x()) && (lastPoint.y() > endPoint.y())){
        for(int j = 0; j < width_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc + j, yc), c.rgb());
            _imaging_captured.setPixel(QPoint(xc + j, yc - height_crop),
                                       c.rgb());
        }

        for(int j = 0; j < height_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc, yc - j), c.rgb());
            _imaging_captured.setPixel(QPoint(xc + width_crop, yc - j),
                                       c.rgb());
        }
    }
    else if((lastPoint.x() > endPoint.x()) && (lastPoint.y() > endPoint.y())){
        for(int j = 0; j < width_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc - j, yc), c.rgb());
            _imaging_captured.setPixel(QPoint(xc - j, yc - height_crop),
                                       c.rgb());
        }

        for(int j = 0; j < height_crop; j++)
        {
            _imaging_captured.setPixel(QPoint(xc, yc - j), c.rgb());
            _imaging_captured.setPixel(QPoint(xc - width_crop, yc - j),
                                       c.rgb());
        }
    }

    _label_imaging->setPixmap(_pixmap_imaging->fromImage(_imaging_captured));
}

void View::save_crop_image(const QString &fileName, const char *fileFormat)
{
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Crop, display new image and save it";
    QImage new_image;
    if((lastPoint.x() <= endPoint.x()) && (lastPoint.y() <= endPoint.y())) {
        new_image = _imaging_captured.copy(lastPoint.x()-IMAGING_PIXEL_TOP_X+2,
                                       lastPoint.y()-IMAGING_PIXEL_TOP_Y+2,
                                       width_crop-2, height_crop-2);
    }

    else if((lastPoint.x() > endPoint.x()) && (lastPoint.y() <= endPoint.y())){
        new_image = _imaging_captured.copy(endPoint.x()-IMAGING_PIXEL_TOP_X+2,
                                       lastPoint.y()-IMAGING_PIXEL_TOP_Y+2,
                                       width_crop-2, height_crop-2);
    }

    else if((lastPoint.x() <= endPoint.x()) && (lastPoint.y() > endPoint.y())){
        new_image = _imaging_captured.copy(lastPoint.x()-IMAGING_PIXEL_TOP_X+2,
                                       endPoint.y()-IMAGING_PIXEL_TOP_Y+2,
                                       width_crop-2, height_crop-2);
    }

    else if((lastPoint.x() > endPoint.x()) && (lastPoint.y() > endPoint.y())){
        new_image = _imaging_captured.copy(endPoint.x()-IMAGING_PIXEL_TOP_X+2,
                                       endPoint.y()-IMAGING_PIXEL_TOP_Y+2,
                                       width_crop-2, height_crop-2);
    }

    new_image.save(fileName, fileFormat);
    _pixmap_imaging = new QPixmap(fileName);
    new_image = _pixmap_imaging->toImage().scaled(1600,
                                    900, Qt::KeepAspectRatio);
    _label_imaging->setPixmap(_pixmap_imaging->fromImage(new_image));
    _imaging_captured = new_image;
}

QGroupBox* View::display_imaging()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up imaging groupbox";
    QGroupBox *_display_groupbox = new QGroupBox("Imaging");
    QVBoxLayout *display_layout = new QVBoxLayout;

    _label_imaging = new QLabel(this);
    _pixmap_imaging = new QPixmap(QCoreApplication::applicationDirPath() +
                                    "/../ogs_ihm/Images/image4_jwst.jpg");
    _cursor_image = new QCursor(*_pixmap_imaging);
    _cursor_image->setShape(Qt::CrossCursor);
    _imaging_tab->setCursor(*_cursor_image);

    _imaging_captured = _pixmap_imaging->toImage().scaled(1600,
                                    900, Qt::KeepAspectRatio);
    _label_imaging->setPixmap(_pixmap_imaging->fromImage(_imaging_captured));
    display_layout->addWidget(_label_imaging);
    _display_groupbox->setLayout(display_layout);
    return _display_groupbox;
}

void View::imaging_buttons()
{
    _imaging_layout = new QGridLayout();
    _imaging_layout->addWidget(display_imaging(), 0, 0);
    _imaging_tab->setLayout(_imaging_layout);
}

////////////////////////////////POSITION FUNCTIONS/////////////////////////

void View::draw_circle(int xc, int yc, int x, int y)
{
    QColor c(255, 0, 0, 255);
    _image_telescope.setPixel(QPoint(xc+x, yc+y), c.rgb());
    _image_telescope.setPixel(QPoint(xc-x, yc+y), c.rgb());
    _image_telescope.setPixel(QPoint(xc+x, yc-y), c.rgb());
    _image_telescope.setPixel(QPoint(xc-x, yc-y), c.rgb());
    _image_telescope.setPixel(QPoint(xc+y, yc+x), c.rgb());
    _image_telescope.setPixel(QPoint(xc-y, yc+x), c.rgb());
    _image_telescope.setPixel(QPoint(xc+y, yc-x), c.rgb());
    _image_telescope.setPixel(QPoint(xc-y, yc-x), c.rgb());
}

void View::algo_bresenham(int xc, int yc, int r)
{
    int x = 0;
    int y = r;
    int d = 3 - 2 * r;
    draw_circle(xc, yc, x, y);
    while (y >= x)
    {
        x++;
        if (d > 0)
        {
            y--;
            d = d + 4 * (x - y) + 10;
        }
        else
            d = d + 4 * x + 6;
        draw_circle(xc, yc, x, y);
    }
}

void View::update_position_telescope(int xc, int yc)
{
    _image_telescope = _pixmap_telescope->toImage().scaled(1600,
                                    900, Qt::KeepAspectRatio);
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Draw circle to show telescope position (Bresenham algorithm)";
    QColor c(255, 0, 0, 255);
    int r = 25;
    algo_bresenham(xc, yc, r);

    for(int j = 0; j < (r * 2); j++)
    {
        _image_telescope.setPixel(QPoint(xc-r+j, yc), c.rgb());
        _image_telescope.setPixel(QPoint(xc, yc-r+j), c.rgb());
    }

    _label_telescope_pos->setPixmap(_pixmap_telescope->
                                    fromImage(_image_telescope));
}

QGroupBox* View::display_position()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up position telescope groupbox";
    QGroupBox *_display_groupbox = new QGroupBox("Position");
    QVBoxLayout *display_layout = new QVBoxLayout;

    _label_telescope_pos = new QLabel(this);
    _pixmap_telescope = new QPixmap(QCoreApplication::applicationDirPath() +
                                    "/../ogs_ihm/Images/position_sphere.png");

    float alt = ((POSITION_PIXEL_XW-POSITION_PIXEL_X0)*
                 (90-_degree_alt->value()))/90;
    float rad_az = (_degree_az->value() * 2 * PI)/360;
    int xc = POSITION_PIXEL_X0 + alt * sin(rad_az);
    int yc = POSITION_PIXEL_Y0 - alt * cos(rad_az);
    update_position_telescope(xc, yc);

    display_layout->addWidget(_label_telescope_pos);
    _display_groupbox->setLayout(display_layout);
    return _display_groupbox;
}

void View::position_buttons()
{
    _position_layout = new QGridLayout();
    _position_layout->addWidget(display_position(), 0, 0);
    _position_tab->setLayout(_position_layout);
}

////////////////////////////////WEATHER FUNCTIONS/////////////////////////

QGroupBox* View::display_infos_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up weather infos group box";

    QGroupBox *_infos_groupbox = new QGroupBox("Weather infos");
    QVBoxLayout *infos_layout = new QVBoxLayout;

    _wind_dir_min_label = new QLabel("Wind direction min: 0 degrees");
    infos_layout->addWidget(_wind_dir_min_label);
    _wind_dir_ave_label = new QLabel("Wind direction average: 0 degrees");
    infos_layout->addWidget(_wind_dir_ave_label);
    _wind_dir_max_label = new QLabel("Wind direction max: 0 degrees");
    infos_layout->addWidget(_wind_dir_max_label);

    _wind_speed_min_label = new QLabel("Wind speed min: 0 m/s");
    infos_layout->addWidget(_wind_speed_min_label);
    _wind_speed_ave_label = new QLabel("Wind speed average: 0 m/s");
    infos_layout->addWidget(_wind_speed_ave_label);
    _wind_speed_max_label = new QLabel("Wind speed max: 0 m/s");
    infos_layout->addWidget(_wind_speed_max_label);

    _air_temp_label = new QLabel("Air temperature: 0 °C");
    infos_layout->addWidget(_air_temp_label);
    _relat_hum_label = new QLabel("Relative humidity: 0 %RH");
    infos_layout->addWidget(_relat_hum_label);
    _dew_point_label = new QLabel("Dew point: 0 °C");
    infos_layout->addWidget(_dew_point_label);
    _air_pressure_label = new QLabel("Air pressure: 0 hPa");
    infos_layout->addWidget(_air_pressure_label);

    _rain_accum_label = new QLabel("Rain accumulation: 0 mm");
    infos_layout->addWidget(_rain_accum_label);
    _rain_duration_label = new QLabel("Rain duration: 0 s");
    infos_layout->addWidget(_rain_duration_label);
    _rain_int_label = new QLabel("Rain intensity: 0 mm/h");
    infos_layout->addWidget(_rain_int_label);

    _hail_accum_label = new QLabel("Hail accumulation: 0 hits/cm²");
    infos_layout->addWidget(_hail_accum_label);
    _hail_dur_label = new QLabel("Hail duration: 0 s");
    infos_layout->addWidget(_hail_dur_label);
    _hail_int_label = new QLabel("Hail intensity: 0 hits/cm²h");
    infos_layout->addWidget(_hail_int_label);

    _rain_peak_int_label = new QLabel("Rain peak intensity: 0 mm/h");
    infos_layout->addWidget(_rain_peak_int_label);
    _hail_peak_int_label = new QLabel("Hail peak intensity: 0 hits/cm²h");
    infos_layout->addWidget(_hail_peak_int_label);

    _heating_temp_label = new QLabel("Heating temperature: 0 °C");
    infos_layout->addWidget(_heating_temp_label);
    _heating_volt_label = new QLabel("Heating voltage: V");
    infos_layout->addWidget(_heating_volt_label);
    _supply_volt_label = new QLabel("Supply voltage: 0 V");
    infos_layout->addWidget(_supply_volt_label);
    _ref_volt_label = new QLabel("3.5V reference volatge: 0 V");
    infos_layout->addWidget(_ref_volt_label);

    _infos_groupbox->setLayout(infos_layout);
    return _infos_groupbox;
}

pair<QGroupBox*,int> View::buttons_weather()
{
    QGroupBox *_graph_groupbox = new QGroupBox("Config");
    QHBoxLayout *graph_layout = new QHBoxLayout;

    _config_weather = new QPushButton("Config thresholds");
    graph_layout->addWidget(_config_weather);
    _show_graph_weather = new QPushButton("Show graph");
    graph_layout->addWidget(_show_graph_weather);
    _graph_groupbox->setLayout(graph_layout);

    QObject::connect(_config_weather, SIGNAL(clicked()),
                     this, SLOT(config_command_weather()));
    QObject::connect(_show_graph_weather, SIGNAL(clicked()),
                     this, SLOT(show_graph_weather()));

    return make_pair(_graph_groupbox, USER_LEVEL);
}

QGroupBox* View::show_log_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up logging label scrollbar";
    _string_log_weather = "Weather logging !!!!\n";
    _logging_weather = new QLabel(_string_log_weather, this);
    _logging_weather->resize(QSize(500, 500));

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    _logging_scroll_weather = new QScrollArea(this);
    _logging_scroll_weather->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _logging_scroll_weather->setHorizontalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scroll_weather->setWidget(_logging_weather);
    _logging_scroll_weather->setWidgetResizable(true);
    _logging_scroll_weather->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scroll_weather);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::connection_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up connection weather group box";
    QGroupBox *_weather_groupbox = new QGroupBox("Connection Weather");
    QHBoxLayout *weather_layout = new QHBoxLayout;

    _connect_weather = new QPushButton("Connect");
    weather_layout->addWidget(_connect_weather);
    _disconnect_weather = new QPushButton("Disconnect");
    weather_layout->addWidget(_disconnect_weather);
    _meteoblue = new QPushButton("METEOBLUE");
    _meteoblue->setToolTip("Open meteoblue in web browser");
    weather_layout->addWidget(_meteoblue);
    _weather_groupbox->setLayout(weather_layout);

    QObject::connect(_meteoblue, SIGNAL(clicked()),
                     this, SLOT(open_meteoblue()));

    return _weather_groupbox;
}

void View::open_meteoblue()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Open Caussols meteoblue hyperlink";
    string op = string("xdg-open ").append(
        "https://www.meteoblue.com/en/weather/week/caussols_france_6620018");
    system(op.c_str());
}

void View::weather_buttons()
{
    _weather_layout = new QGridLayout();
    _weather_layout->setRowMinimumHeight(1, 600);
    _weather_layout->setRowMinimumHeight(0, 300);
    _weather_layout->setColumnMinimumWidth(1, 600);

    _weather_layout->addWidget(display_infos_weather(), 1, 1);
    _weather_layout->addWidget(show_log_weather(), 1, 0);
    _weather_layout->addWidget(connection_weather(), 0, 0);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Verify user level to display config and graph buttons";
    if(buttons_weather().second <= 0)
        _weather_layout->addWidget(buttons_weather().first, 0, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Insert weather layout into the map with the associated level";
    _map_weather.insert(make_pair(_weather_layout, USER_LEVEL));
}

void View::verify_user_level_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the weather grid layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it=_map_weather.begin();
        it != _map_weather.end(); ++it)
    {
        if(it->second <= 0)
            _weather_tab->setLayout(it->first);
    }
}

float View::dew_point_calculation(float temp, float hum)
{
    float alpha = 6.112;
    float beta = 17.62;
    float lambda = 243.12;

    return ((lambda*(log(hum/100)+((beta*temp)/(lambda+temp))))/
            (beta-(log(hum/100)+((beta*temp)/(lambda+temp)))));
}

int View::get_param_weather(vector<string> v)
{
    string command = v[0].substr(1, v[0].length()-1);
    string tmp;
    string value;
    if(!command.compare("r1"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            string data = v[i];
            char *tmp = strtok(&data[0], "=");
            if(v[i].length() > 3)
                value = v[i].substr(3, v[i].length()-4);
            else {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "data size is too small";
                return -1;
            }
            replace(value.begin(),value.end(),'.', ',');
            float tmp_float;
            try {
                tmp_float = stof(value);
            } catch (const exception & e){
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Invalid argument R1";
                return -1;
            }
            if(!string(tmp).compare("Dn"))
            {
                _wind_direction_min = tmp_float;
                _wind_dir_min_label->setText(QString::fromStdString(
                    "Wind direction min: " + value + " degrees"));
            }
            else if(!string(tmp).compare("Dm"))
            {
                _wind_direction_ave = tmp_float;
                _wind_dir_ave_label->setText(QString::fromStdString(
                    "Wind direction average: " + value + " degrees"));
            }
            else if(!string(tmp).compare("Dx"))
            {
                _wind_direction_max = tmp_float;
                _wind_dir_max_label->setText(QString::fromStdString(
                    "Wind direction max: " + value + " degrees"));
            }
            else if(!string(tmp).compare("Sn"))
            {
                _wind_speed_min = tmp_float;
                _wind_speed_min_label->setText(QString::fromStdString(
                    "Wind speed min: " + value + " m/s"));
            }
            else if(!string(tmp).compare("Sm"))
            {
                _wind_speed_ave = tmp_float;
                _wind_speed_ave_label->setText(QString::fromStdString(
                    "Wind speed average: " + value + " m/s"));
            }
            else if(!string(tmp).compare("Sx"))
            {
                _wind_speed_max = tmp_float;
                _wind_speed_max_label->setText(QString::fromStdString(
                    "Wind speed max: " + value + " m/s"));
            }
            else
            {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "unrecognized data R1";
                return -1;
            }
        }
    }
    else if(!command.compare("r2"))
    {
        for(int i = 1; i < v.size() ; i++)
        {          
            string data = v[i];
            char *tmp = strtok(&data[0], "=");
            if(v[i].length() > 3)
                value = v[i].substr(3, v[i].length()-4);
            else {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "data size is too small";
                return -1;
            }
            replace(value.begin(),value.end(),'.', ',');
            float tmp_float;
            try {
                tmp_float = stof(value);
            } catch (const exception & e){
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Invalid argument R2";
                return -1;
            }
            if(!string(tmp).compare("Ta"))
            {
                _air_temp = tmp_float;
                _air_temp_label->setText(QString::fromStdString(
                    "Air temperature: " + value + " °C"));
            }
            else if(!string(tmp).compare("Ua"))
            {
                _relat_humidity = tmp_float;
                _relat_hum_label->setText(QString::fromStdString(
                    "Relative humidity: " + value + " %RH"));

                _dew_point = dew_point_calculation(_air_temp, _relat_humidity);
                std::stringstream sstream;
                sstream << _dew_point;
                string num_str = sstream.str();
                _dew_point_label->setText(QString::fromStdString(
                    "Dew point: " + num_str + " °C"));
            }
            else if(!string(tmp).compare("Pa"))
            {
                _air_pressure = tmp_float;
                _air_pressure_label->setText(QString::fromStdString(
                    "Air pressure: " + value + " hPa"));
            }
            else
            {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "unrecognized data R2";
                return -1;
            }
        }
    }
    else if(!command.compare("r3"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            string data = v[i];
            char *tmp = strtok(&data[0], "=");
            if(v[i].length() > 3)
                value = v[i].substr(3, v[i].length()-4);
            else {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "data size is too small";
                return -1;
            }
            replace(value.begin(),value.end(),'.', ',');
            float tmp_float;
            try {
                tmp_float = stof(value);
            } catch (const exception & e){
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Invalid argument R3";
                return -1;
            }
            if(!string(tmp).compare("Rc"))
            {
                _rain_accum = tmp_float;
                _rain_accum_label->setText(QString::fromStdString(
                    "Rain accumulation: " + value + " mm"));
            }
            else if(!string(tmp).compare("Rd"))
            {
                _rain_duration = tmp_float;
                _rain_duration_label->setText(QString::fromStdString(
                    "Rain duration: " + value + " s"));
            }
            else if(!string(tmp).compare("Ri"))
            {
                _rain_intensity = tmp_float;
                _rain_int_label->setText(QString::fromStdString(
                    "Rain intensity: " + value + " mm/h"));
            }
            else if(!string(tmp).compare("Hc"))
            {
                _hail_accum = tmp_float;
                _hail_accum_label->setText(QString::fromStdString(
                    "Hail accumulation: " + value + " hits/cm²"));
            }
            else if(!string(tmp).compare("Hd"))
            {
                _hail_duration = tmp_float;
                _hail_dur_label->setText(QString::fromStdString(
                    "Hail duration: " + value + " s"));
            }
            else if(!string(tmp).compare("Hi"))
            {
                _hail_intensity = tmp_float;
                _hail_int_label->setText(QString::fromStdString(
                    "Hail intensity: " + value + " hits/cm²h"));
            }
            else if(!string(tmp).compare("Rp"))
            {
                _rain_peak_intensity = tmp_float;
                _rain_peak_int_label->setText(QString::fromStdString(
                    "Rain peak intensity: " + value + " mm/h"));
            }
            else if(!string(tmp).compare("Hp"))
            {
                _hail_peak_intensity = tmp_float;
                _hail_peak_int_label->setText(QString::fromStdString(
                    "Hail peak intensity: " + value + " hits/cm²h"));
            }
            else
            {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "unrecognized data R3";
                return -1;
            }
        }
    }
    else if(!command.compare("r5"))
    {
        for(int i = 1; i < v.size() ; i++)
        {
            string data = v[i];
            char *tmp = strtok(&data[0], "=");
            if(v[i].length() > 3)
                value = v[i].substr(3, v[i].length()-4);
            else {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "data size is too small";
                return -1;
            }
            replace(value.begin(),value.end(),'.', ',');
            float tmp_float;
            try {
                tmp_float = stof(value);
            } catch (const exception & e){
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Invalid argument R5";
                return -1;
            }
            if(!string(tmp).compare("Th"))
            {
                _heating_temp = tmp_float;
                _heating_temp_label->setText(QString::fromStdString(
                    "Heating temperature: " + value + " °C"));
            }
            else if(!string(tmp).compare("Vh"))
            {
                _heating_voltage = tmp_float;
                _heating_volt_label->setText(QString::fromStdString(
                    "Heating voltage: " + value + " V"));
            }
            else if(!string(tmp).compare("Vs"))
            {
                _supply_voltage = tmp_float;
                _supply_volt_label->setText(QString::fromStdString(
                    "Supply voltage: " + value + " V"));
            }
            else if(!string(tmp).compare("Vr"))
            {
                _ref_voltage = tmp_float;
                _ref_volt_label->setText(QString::fromStdString(
                    "3.5V reference volatge: " + value + " V"));
            }
            else
            {
                PLOG_ERROR_IF(LOGGING_ERROR == 1) << "unrecognized data R5";
                return -1;
            }
        }
    }
    else {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "string corrupted";
        return -1;
    }
    return 0;
}

void View::check_weather_conditions()
{
    QStringList engines = QTextToSpeech::availableEngines();
    _speech = new QTextToSpeech();
    _speech->setLocale(QLocale(QLocale::English,
            QLocale::LatinScript, QLocale::UnitedStates));

    string data_received;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "weather data: " << data_received;
    _string_log_weather = _string_log_weather +
            QString::fromStdString(data_received) + "\n";
    _logging_weather->setText(_string_log_weather);
    _logging_scroll_weather->verticalScrollBar()->setSliderPosition(
                _logging_weather->height());

    if(_wind_direction_max >
            stof(_list_config_thresholds.find(0)->second))
        _wind_dir_max_label->setStyleSheet("color: red");
    else
        _wind_dir_max_label->setStyleSheet("color: black");

    if(_wind_speed_max >
            stof(_list_config_thresholds.find(1)->second))
        _wind_speed_max_label->setStyleSheet("color: red");
    else
        _wind_speed_max_label->setStyleSheet("color: black");

    if(_air_temp >
            stof(_list_config_thresholds.find(2)->second))
        _air_temp_label->setStyleSheet("color: red");
    else
        _air_temp_label->setStyleSheet("color: black");

    if(_relat_humidity >
            stof(_list_config_thresholds.find(3)->second))
        _relat_hum_label->setStyleSheet("color: red");
    else
        _relat_hum_label->setStyleSheet("color: black");

    if(_dew_point >
            stof(_list_config_thresholds.find(4)->second))
        _dew_point_label->setStyleSheet("color: red");
    else
        _dew_point_label->setStyleSheet("color: black");

    if(_air_pressure <
            stof(_list_config_thresholds.find(5)->second))
        _air_pressure_label->setStyleSheet("color: red");
    else
        _air_pressure_label->setStyleSheet("color: black");

    if(_rain_accum >
            stof(_list_config_thresholds.find(6)->second))
    {
        _rain_accum_label->setStyleSheet("color: red");
        /*qApp->beep();
        _speech->say("Too much rain");*/
        _weather_tab->setStyleSheet("background-color: yellow");
    }
    else {
        _rain_accum_label->setStyleSheet("color: black");
        _weather_tab->setStyleSheet("background-color: white");
    }

    if(_rain_duration >
            stof(_list_config_thresholds.find(7)->second))
        _rain_duration_label->setStyleSheet("color: red");
    else
        _rain_duration_label->setStyleSheet("color: black");

    if(_rain_intensity >
            stof(_list_config_thresholds.find(8)->second))
        _rain_int_label->setStyleSheet("color: red");
    else
        _rain_int_label->setStyleSheet("color: black");

    if(_rain_peak_intensity >
            stof(_list_config_thresholds.find(9)->second))
        _rain_peak_int_label->setStyleSheet("color: red");
    else
        _rain_peak_int_label->setStyleSheet("color: black");

    if(_hail_accum >
            stof(_list_config_thresholds.find(10)->second))
        _hail_accum_label->setStyleSheet("color: red");
    else
        _hail_accum_label->setStyleSheet("color: black");

    if(_hail_duration >
            stof(_list_config_thresholds.find(11)->second))
        _hail_dur_label->setStyleSheet("color: red");
    else
        _hail_dur_label->setStyleSheet("color: black");

    if(_hail_intensity >
            stof(_list_config_thresholds.find(12)->second))
        _hail_int_label->setStyleSheet("color: red");
    else
        _hail_int_label->setStyleSheet("color: black");

    if(_hail_peak_intensity >
            stof(_list_config_thresholds.find(13)->second))
        _hail_peak_int_label->setStyleSheet("color: red");
    else
        _hail_peak_int_label->setStyleSheet("color: black");

    if(_heating_temp >
            stof(_list_config_thresholds.find(14)->second))
        _heating_temp_label->setStyleSheet("color: red");
    else
        _heating_temp_label->setStyleSheet("color: black");

    if(_heating_voltage >
            stof(_list_config_thresholds.find(15)->second))
        _heating_volt_label->setStyleSheet("color: red");
    else
        _heating_volt_label->setStyleSheet("color: black");

    if(_supply_voltage >
            stof(_list_config_thresholds.find(16)->second))
        _supply_volt_label->setStyleSheet("color: red");
    else
        _supply_volt_label->setStyleSheet("color: black");

    if(_ref_voltage >
            stof(_list_config_thresholds.find(17)->second))
        _ref_volt_label->setStyleSheet("color: red");
    else
        _ref_volt_label->setStyleSheet("color: black");
}

/////////////////////////CONFIG WEATHER FUNCTIONS////////////////////

int View::write_file_config_weather()
{
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_thresholds_weather.txt");
    ofstream file;

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Open and delete the file's weather content";
    file.open(str1, ios::out | ios::trunc);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Weather file not found";
        return -1;
    }

    config_weather = "config_weather;18;";
    for(map<int,string>::const_iterator it =_list_config_thresholds.begin();
        it != _list_config_thresholds.end(); ++it)
    {
        file << it->second << "\n";
        config_weather = config_weather + it->second + ";";
    }
    config_weather.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Weather serialized config file: "
                                      << config_weather;
    _string_log_weather = _string_log_weather + "Serialized config file: \"" +
            QString::fromStdString(config_weather) + "\"\n";
    _logging_weather->setText(_string_log_weather);
    _logging_scroll_weather->verticalScrollBar()->setSliderPosition(
                _logging_weather->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close weather file";
    return 0;
}

int View::read_file_config_weather()
{
    string text;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Open the weather config file";
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_thresholds_weather.txt");
    ifstream file(str1);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Weather file not found";
        return -1;
    }

    int i = 0;
    config_weather = "config_weather;18;";
    while(getline(file, text))
    {
        _list_config_thresholds.insert(make_pair(i, text));
        config_weather = config_weather + text + ";";
        i++;
    }

    if (i == 0) {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Weather file is empty";
        return -1;
    }
    config_weather.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Weather serialized config file: "
                                      << config_weather;
    _string_log_weather = _string_log_weather + "Serialized config file: \"" +
            QString::fromStdString(config_weather) + "\"\n";
    _logging_weather->setText(_string_log_weather);
    _logging_scroll_weather->verticalScrollBar()->setSliderPosition(
                _logging_weather->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close weather file";
    return i;
}

QGroupBox* View::wind_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up weather thresholds group box";

    QGroupBox *_wind_limits_groupbox = new QGroupBox("Weather Thresholds");
    QGridLayout *wind_layout = new QGridLayout;

    _limit_wind_dir = new QLabel("Threshold wind direction (°)");
    wind_layout->addWidget(_limit_wind_dir, 0, 0);
    _wind_dir_spinbox = new QDoubleSpinBox;
    _wind_dir_spinbox->setRange(0, 360);
    _wind_dir_spinbox->setSingleStep(1);
    _wind_dir_spinbox->setValue(250);
    _wind_dir_spinbox->setToolTip("Between 0 and 360 degrees");
    wind_layout->addWidget(_wind_dir_spinbox, 0, 1);

    _limit_wind_speed = new QLabel("Threshold wind speed (m/s)");
    wind_layout->addWidget(_limit_wind_speed, 1, 0);
    _wind_speed_spinbox = new QDoubleSpinBox;
    _wind_speed_spinbox->setRange(0, 30);
    _wind_speed_spinbox->setSingleStep(1);
    _wind_speed_spinbox->setValue(5);
    _wind_speed_spinbox->setToolTip("Between 0 and 30 m/s");
    wind_layout->addWidget(_wind_speed_spinbox, 1, 1);

    _limit_temp = new QLabel("Threshold temperature (°C)");
    wind_layout->addWidget(_limit_temp, 2, 0);
    _temp_spinbox = new QDoubleSpinBox;
    _temp_spinbox->setRange(-20, 40);
    _temp_spinbox->setSingleStep(1);
    _temp_spinbox->setValue(18);
    _temp_spinbox->setToolTip("Between -20 and 40 °C");
    wind_layout->addWidget(_temp_spinbox, 2, 1);

    _limit_hum = new QLabel("Threshold humidity (%RH)");
    wind_layout->addWidget(_limit_hum, 3, 0);
    _hum_spinbox = new QDoubleSpinBox;
    _hum_spinbox->setRange(0, 100);
    _hum_spinbox->setSingleStep(1);
    _hum_spinbox->setValue(45);
    _hum_spinbox->setToolTip("Between 0 and 100 %RH");
    wind_layout->addWidget(_hum_spinbox, 3, 1);

    _limit_dew_point = new QLabel("Threshold dew point (°C)");
    wind_layout->addWidget(_limit_dew_point, 4, 0);
    _dew_point_spinbox = new QDoubleSpinBox;
    _dew_point_spinbox->setRange(-20, 40);
    _dew_point_spinbox->setSingleStep(1);
    _dew_point_spinbox->setValue(2);
    _dew_point_spinbox->setToolTip("Between -20 and 40 °C");
    wind_layout->addWidget(_dew_point_spinbox, 4, 1);

    _limit_pressure = new QLabel("Threshold pressure (hPa)");
    wind_layout->addWidget(_limit_pressure, 5, 0);
    _pressure_spinbox = new QDoubleSpinBox;
    _pressure_spinbox->setRange(700, 1300);
    _pressure_spinbox->setSingleStep(1);
    _pressure_spinbox->setValue(950);
    _pressure_spinbox->setToolTip("Between 700 and 1300 hPa");
    wind_layout->addWidget(_pressure_spinbox, 5, 1);

    _set_limit_wind = new QPushButton("SET");
    wind_layout->addWidget(_set_limit_wind, 6, 0);
    _reset_limit_wind = new QPushButton("RESET");
    wind_layout->addWidget(_reset_limit_wind, 6, 1);
    _wind_limits_groupbox->setLayout(wind_layout);

    QObject::connect(_set_limit_wind, SIGNAL(clicked()),
        this, SLOT(update_threshold_wind()));
    QObject::connect(_reset_limit_wind, SIGNAL(clicked()),
        this, SLOT(reset_threshold_wind()));

    return _wind_limits_groupbox;
}

void View::update_threshold_wind()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update wind threshold in config window";
    _list_config_thresholds.find(0)->second =
            to_string(_wind_dir_spinbox->value());
    _list_config_thresholds.find(1)->second =
            to_string(_wind_speed_spinbox->value());
    _list_config_thresholds.find(2)->second =
            to_string(_temp_spinbox->value());
    _list_config_thresholds.find(3)->second =
            to_string(_hum_spinbox->value());
    _list_config_thresholds.find(4)->second =
            to_string(_dew_point_spinbox->value());
    _list_config_thresholds.find(5)->second =
            to_string(_pressure_spinbox->value());
    write_file_config_weather();
    update_thresholds_config();
}

void View::reset_threshold_wind()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset wind threshold in config window";
    _list_config_thresholds.find(0)->second = to_string(250);
    _list_config_thresholds.find(1)->second = to_string(5);
    _list_config_thresholds.find(2)->second = to_string(18);
    _list_config_thresholds.find(3)->second = to_string(45);
    _list_config_thresholds.find(4)->second = to_string(2);
    _list_config_thresholds.find(5)->second = to_string(950);

    _wind_dir_spinbox->setValue(250);
    _wind_speed_spinbox->setValue(5);
    _temp_spinbox->setValue(18);
    _hum_spinbox->setValue(45);
    _dew_point_spinbox->setValue(2);
    _pressure_spinbox->setValue(950);

    write_file_config_weather();
    update_thresholds_config();
}

QGroupBox* View::rain_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up rain thresholds group box";

    QGroupBox *_rain_limits_groupbox = new QGroupBox("Rain Thresholds");
    QGridLayout *rain_layout = new QGridLayout;

    _limit_rain_accum = new QLabel("Threshold rain accumulation (mm)");
    rain_layout->addWidget(_limit_rain_accum, 0, 0);
    _rain_accum_spinbox = new QDoubleSpinBox;
    _rain_accum_spinbox->setRange(0, 50);
    _rain_accum_spinbox->setSingleStep(1);
    _rain_accum_spinbox->setValue(0.5);
    _rain_accum_spinbox->setToolTip("Between 0 and 50 mm");
    rain_layout->addWidget(_rain_accum_spinbox, 0, 1);

    _limit_rain_dur = new QLabel("Threshold rain duration (s)");
    rain_layout->addWidget(_limit_rain_dur, 1, 0);
    _rain_dur_spinbox = new QDoubleSpinBox;
    _rain_dur_spinbox->setRange(0, 3600);
    _rain_dur_spinbox->setSingleStep(1);
    _rain_dur_spinbox->setValue(20);
    _rain_dur_spinbox->setToolTip("Between 0 and 1h");
    rain_layout->addWidget(_rain_dur_spinbox, 1, 1);

    _limit_rain_int = new QLabel("Threshold rain intensity (mm/h)");
    rain_layout->addWidget(_limit_rain_int, 2, 0);
    _rain_int_spinbox = new QDoubleSpinBox;
    _rain_int_spinbox->setRange(0, 40);
    _rain_int_spinbox->setSingleStep(1);
    _rain_int_spinbox->setValue(5);
    _rain_int_spinbox->setToolTip("Between 0 and 40 mm/h");
    rain_layout->addWidget(_rain_int_spinbox, 2, 1);

    _limit_rain_peak = new QLabel("Threshold rain peak (mm/h)");
    rain_layout->addWidget(_limit_rain_peak, 3, 0);
    _rain_peak_spinbox = new QDoubleSpinBox;
    _rain_peak_spinbox->setRange(0, 40);
    _rain_peak_spinbox->setSingleStep(1);
    _rain_peak_spinbox->setValue(5);
    _rain_peak_spinbox->setToolTip("Between 0 and 40 mm/h");
    rain_layout->addWidget(_rain_peak_spinbox, 3, 1);

    _set_limit_rain = new QPushButton("SET");
    rain_layout->addWidget(_set_limit_rain, 4, 0);
    _reset_limit_rain = new QPushButton("RESET");
    rain_layout->addWidget(_reset_limit_rain, 4, 1);
    _rain_limits_groupbox->setLayout(rain_layout);

    QObject::connect(_set_limit_rain, SIGNAL(clicked()),
        this, SLOT(update_threshold_rain()));
    QObject::connect(_reset_limit_rain, SIGNAL(clicked()),
        this, SLOT(reset_threshold_rain()));

    return _rain_limits_groupbox;
}

void View::update_threshold_rain()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update rain threshold in config window";
    _list_config_thresholds.find(6)->second =
            to_string(_rain_accum_spinbox->value());
    _list_config_thresholds.find(7)->second =
            to_string(_rain_dur_spinbox->value());
    _list_config_thresholds.find(8)->second =
            to_string(_rain_int_spinbox->value());
    _list_config_thresholds.find(9)->second =
            to_string(_rain_peak_spinbox->value());
    write_file_config_weather();
    update_thresholds_config();
}

void View::reset_threshold_rain()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset rain threshold in config window";
    _list_config_thresholds.find(6)->second = to_string(0.5);
    _list_config_thresholds.find(7)->second = to_string(20);
    _list_config_thresholds.find(8)->second = to_string(5);
    _list_config_thresholds.find(9)->second = to_string(5);

    _rain_accum_spinbox->setValue(0.5);
    _rain_dur_spinbox->setValue(20);
    _rain_int_spinbox->setValue(5);
    _rain_peak_spinbox->setValue(5);

    write_file_config_weather();
    update_thresholds_config();
}

QGroupBox* View::hail_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up hail thresholds group box";

    QGroupBox *_hail_limits_groupbox = new QGroupBox("Hail Thresholds");
    QGridLayout *hail_layout = new QGridLayout;

    _limit_hail_accum = new QLabel("Threshold hail accumulation (hits/cm²)");
    hail_layout->addWidget(_limit_hail_accum, 0, 0);
    _hail_accum_spinbox = new QDoubleSpinBox;
    _hail_accum_spinbox->setRange(0, 50);
    _hail_accum_spinbox->setSingleStep(1);
    _hail_accum_spinbox->setValue(0.5);
    _hail_accum_spinbox->setToolTip("Between 0 and 50 hits/cm²");
    hail_layout->addWidget(_hail_accum_spinbox, 0, 1);

    _limit_hail_dur = new QLabel("Threshold hail duration (s)");
    hail_layout->addWidget(_limit_hail_dur, 1, 0);
    _hail_dur_spinbox = new QDoubleSpinBox;
    _hail_dur_spinbox->setRange(0, 3600);
    _hail_dur_spinbox->setSingleStep(1);
    _hail_dur_spinbox->setValue(20);
    _hail_dur_spinbox->setToolTip("Between 0 and 1h");
    hail_layout->addWidget(_hail_dur_spinbox, 1, 1);

    _limit_hail_int = new QLabel("Threshold hail intensity (hits/cm²h)");
    hail_layout->addWidget(_limit_hail_int, 2, 0);
    _hail_int_spinbox = new QDoubleSpinBox;
    _hail_int_spinbox->setRange(0, 40);
    _hail_int_spinbox->setSingleStep(1);
    _hail_int_spinbox->setValue(5);
    _hail_int_spinbox->setToolTip("Between 0 and 40 hits/cm²h");
    hail_layout->addWidget(_hail_int_spinbox, 2, 1);

    _limit_hail_peak = new QLabel("Threshold hail peak (hits/cm²h)");
    hail_layout->addWidget(_limit_hail_peak, 3, 0);
    _hail_peak_spinbox = new QDoubleSpinBox;
    _hail_peak_spinbox->setRange(0, 40);
    _hail_peak_spinbox->setSingleStep(1);
    _hail_peak_spinbox->setValue(5);
    _hail_peak_spinbox->setToolTip("Between 0 and 40 hits/cm²h");
    hail_layout->addWidget(_hail_peak_spinbox, 3, 1);

    _set_limit_hail = new QPushButton("SET");
    hail_layout->addWidget(_set_limit_hail, 4, 0);
    _reset_limit_hail = new QPushButton("RESET");
    hail_layout->addWidget(_reset_limit_hail, 4, 1);
    _hail_limits_groupbox->setLayout(hail_layout);

    QObject::connect(_set_limit_hail, SIGNAL(clicked()),
        this, SLOT(update_threshold_hail()));
    QObject::connect(_reset_limit_hail, SIGNAL(clicked()),
        this, SLOT(reset_threshold_hail()));

    return _hail_limits_groupbox;
}

void View::update_threshold_hail()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update hail threshold in config window";
    _list_config_thresholds.find(10)->second =
            to_string(_hail_accum_spinbox->value());
    _list_config_thresholds.find(11)->second =
            to_string(_hail_dur_spinbox->value());
    _list_config_thresholds.find(12)->second =
            to_string(_hail_int_spinbox->value());
    _list_config_thresholds.find(13)->second =
            to_string(_hail_peak_spinbox->value());
    write_file_config_weather();
    update_thresholds_config();
}

void View::reset_threshold_hail()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset hail threshold in config window";
    _list_config_thresholds.find(10)->second = to_string(0.5);
    _list_config_thresholds.find(11)->second = to_string(20);
    _list_config_thresholds.find(12)->second = to_string(5);
    _list_config_thresholds.find(13)->second = to_string(5);

    _hail_accum_spinbox->setValue(0.5);
    _hail_dur_spinbox->setValue(20);
    _hail_int_spinbox->setValue(5);
    _hail_peak_spinbox->setValue(5);

    write_file_config_weather();
    update_thresholds_config();
}

QGroupBox* View::volt_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up voltage thresholds group box";

    QGroupBox *_volt_limits_groupbox = new QGroupBox("Voltage Thresholds");
    QGridLayout *volt_layout = new QGridLayout;

    _limit_heat_temp = new QLabel("Threshold heating temperature (°C)");
    volt_layout->addWidget(_limit_heat_temp, 0, 0);
    _heat_temp_spinbox = new QDoubleSpinBox;
    _heat_temp_spinbox->setRange(0, 50);
    _heat_temp_spinbox->setSingleStep(1);
    _heat_temp_spinbox->setValue(15);
    _heat_temp_spinbox->setToolTip("Between 0 and 50 °C");
    volt_layout->addWidget(_heat_temp_spinbox, 0, 1);

    _limit_heat_volt = new QLabel("Threshold heating voltage (V)");
    volt_layout->addWidget(_limit_heat_volt, 1, 0);
    _heat_volt_spinbox = new QDoubleSpinBox;
    _heat_volt_spinbox->setRange(0, 30);
    _heat_volt_spinbox->setSingleStep(1);
    _heat_volt_spinbox->setValue(25);
    _heat_volt_spinbox->setToolTip("Between 0 and 30 V");
    volt_layout->addWidget(_heat_volt_spinbox, 1, 1);

    _limit_supp_volt = new QLabel("Threshold supply voltage (V)");
    volt_layout->addWidget(_limit_supp_volt, 2, 0);
    _supp_volt_spinbox = new QDoubleSpinBox;
    _supp_volt_spinbox->setRange(0, 30);
    _supp_volt_spinbox->setSingleStep(1);
    _supp_volt_spinbox->setValue(25);
    _supp_volt_spinbox->setToolTip("Between 0 and 30 V");
    volt_layout->addWidget(_supp_volt_spinbox, 2, 1);

    _limit_ref_volt = new QLabel("Threshold reference voltage (V)");
    volt_layout->addWidget(_limit_ref_volt, 3, 0);
    _ref_volt_spinbox = new QDoubleSpinBox;
    _ref_volt_spinbox->setRange(0, 10);
    _ref_volt_spinbox->setSingleStep(1);
    _ref_volt_spinbox->setValue(3.5);
    _ref_volt_spinbox->setToolTip("Between 0 and 10 V");
    volt_layout->addWidget(_ref_volt_spinbox, 3, 1);

    _set_limit_volt = new QPushButton("SET");
    volt_layout->addWidget(_set_limit_volt, 4, 0);
    _reset_limit_volt = new QPushButton("RESET");
    volt_layout->addWidget(_reset_limit_volt, 4, 1);
    _volt_limits_groupbox->setLayout(volt_layout);

    QObject::connect(_set_limit_volt, SIGNAL(clicked()),
        this, SLOT(update_threshold_volt()));
    QObject::connect(_reset_limit_volt, SIGNAL(clicked()),
        this, SLOT(reset_threshold_volt()));

    return _volt_limits_groupbox;
}

void View::update_threshold_volt()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update voltage threshold in config window";
    _list_config_thresholds.find(14)->second =
            to_string(_heat_temp_spinbox->value());
    _list_config_thresholds.find(15)->second =
            to_string(_heat_volt_spinbox->value());
    _list_config_thresholds.find(16)->second =
            to_string(_supp_volt_spinbox->value());
    _list_config_thresholds.find(17)->second =
            to_string(_ref_volt_spinbox->value());
    write_file_config_weather();
    update_thresholds_config();
}

void View::reset_threshold_volt()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Reset voltage threshold in config window";
    _list_config_thresholds.find(14)->second = to_string(15);
    _list_config_thresholds.find(15)->second = to_string(25);
    _list_config_thresholds.find(16)->second = to_string(25);
    _list_config_thresholds.find(17)->second = to_string(3.5);

    _heat_temp_spinbox->setValue(15);
    _heat_volt_spinbox->setValue(25);
    _supp_volt_spinbox->setValue(25);
    _ref_volt_spinbox->setValue(3.5);

    write_file_config_weather();
    update_thresholds_config();
}

QGroupBox* View::thresholds_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up thresholds weather group box";

    QGroupBox *_limits_groupbox = new QGroupBox("");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    _wind_dir_config = new QLabel("Threshold wind direction:");
    limits_layout->addWidget(_wind_dir_config);
    _wind_speed_config = new QLabel("Threshold wind speed:");
    limits_layout->addWidget(_wind_speed_config);
    _temp_config = new QLabel("Threshold temperature:");
    limits_layout->addWidget(_temp_config);
    _hum_config = new QLabel("Threshold humidity:");
    limits_layout->addWidget(_hum_config);
    _dew_point_config = new QLabel("Threshold dew point:");
    limits_layout->addWidget(_dew_point_config);
    _pressure_config = new QLabel("Threshold pressure:");
    limits_layout->addWidget(_pressure_config);

    _rain_accum_config = new QLabel("Threshold rain accumulation:");
    limits_layout->addWidget(_rain_accum_config);
    _rain_dur_config = new QLabel("Threshold rain duration:");
    limits_layout->addWidget(_rain_dur_config);
    _rain_int_config = new QLabel("Threshold rain intensity:");
    limits_layout->addWidget(_rain_int_config);
    _rain_peak_int_config = new QLabel("Threshold rain peak:");
    limits_layout->addWidget(_rain_peak_int_config);

    _hail_accum_config = new QLabel("Threshold hail accumulation:");
    limits_layout->addWidget(_hail_accum_config);
    _hail_dur_config = new QLabel("Threshold hail duration:");
    limits_layout->addWidget(_hail_dur_config);
    _hail_int_config = new QLabel("Threshold hail intensity:");
    limits_layout->addWidget(_hail_int_config);
    _hail_peak_int_config = new QLabel("Threshold hail peak:");
    limits_layout->addWidget(_hail_peak_int_config);

    _heating_temp_config = new QLabel("Threshold heating temperature:");
    limits_layout->addWidget(_heating_temp_config);
    _heating_volt_config = new QLabel("Threshold heating voltage:");
    limits_layout->addWidget(_heating_volt_config);
    _supply_volt_config = new QLabel("Threshold supply voltage:");
    limits_layout->addWidget(_supply_volt_config);
    _ref_volt_config = new QLabel("Threshold reference voltage:");
    limits_layout->addWidget(_ref_volt_config);
    _limits_groupbox->setLayout(limits_layout);

    return _limits_groupbox;
}

void View::update_thresholds_config()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update weather thresholds displayed in config window";

    _wind_dir_config->setText(QString::fromStdString(
        "Threshold wind direction: " + _list_config_thresholds.find(0)->second+ "degrees"));
    _wind_speed_config->setText(QString::fromStdString(
        "Threshold wind speed: " + _list_config_thresholds.find(1)->second + "m/s"));
    _temp_config->setText(QString::fromStdString(
        "Threshold temperature: " + _list_config_thresholds.find(2)->second + "°C"));
    _hum_config->setText(QString::fromStdString(
        "Threshold humidity: " + _list_config_thresholds.find(3)->second + "%RH"));
    _dew_point_config->setText(QString::fromStdString(
        "Threshold dew point: " + _list_config_thresholds.find(4)->second + "°C"));
    _pressure_config->setText(QString::fromStdString(
        "Threshold pressure: " + _list_config_thresholds.find(5)->second + "hPa"));

    _rain_accum_config->setText(QString::fromStdString(
        "Threshold rain accumulation: " + _list_config_thresholds.find(6)->second + "mm"));
    _rain_dur_config->setText(QString::fromStdString(
        "Threshold rain duration: " + _list_config_thresholds.find(7)->second + "s"));
    _rain_int_config->setText(QString::fromStdString(
        "Threshold rain intensity: " + _list_config_thresholds.find(8)->second + "mm/h"));
    _rain_peak_int_config->setText(QString::fromStdString(
        "Threshold rain peak: " + _list_config_thresholds.find(9)->second + "mm/h"));

    _hail_accum_config->setText(QString::fromStdString(
        "Threshold hail accumulation: " + _list_config_thresholds.find(10)->second + "hits/cm²"));
    _hail_dur_config->setText(QString::fromStdString(
        "Threshold hail duration: " + _list_config_thresholds.find(11)->second + "s"));
    _hail_int_config->setText(QString::fromStdString(
        "Threshold hail intensity: " + _list_config_thresholds.find(12)->second + "hits/cm²h"));
    _hail_peak_int_config->setText(QString::fromStdString(
        "Threshold hail peak: " + _list_config_thresholds.find(13)->second + "hits/cm²h"));

    _heating_temp_config->setText(QString::fromStdString(
        "Threshold heating temperature: " + _list_config_thresholds.find(14)->second + "°C"));
    _heating_volt_config->setText(QString::fromStdString(
        "Threshold heating voltage: " + _list_config_thresholds.find(15)->second + "V"));
    _supply_volt_config->setText(QString::fromStdString(
        "Threshold supply voltage: " + _list_config_thresholds.find(16)->second + "V"));
    _ref_volt_config->setText(QString::fromStdString(
        "Threshold reference voltage: " + _list_config_thresholds.find(17)->second + "V"));
}

void View::config_command_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Create new window for weather configurations"
            << "Config button clicked";
    _string_log_weather = _string_log_weather + "config button clicked\n";
    _logging_weather->setText(_string_log_weather);
    _logging_scroll_weather->verticalScrollBar()->setSliderPosition(
                _logging_weather->height());

    _new_window = new QWidget();
    _new_window->setWindowTitle("THRESHOLDS CONFIG");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    QGroupBox *_limits_groupbox = new QGroupBox("Limits");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    limits_layout->addWidget(wind_limits());
    limits_layout->addWidget(rain_limits());
    limits_layout->addWidget(hail_limits());
    limits_layout->addWidget(volt_limits());
    _limits_groupbox->setLayout(limits_layout);

    QGroupBox *_display_groupbox = new QGroupBox("Actual data");
    QVBoxLayout *display_layout = new QVBoxLayout;

    display_layout->addWidget(thresholds_infos());
    _display_groupbox->setLayout(display_layout);

    _config_weather_layout = new QGridLayout();
    _config_weather_layout->addWidget(_limits_groupbox, 0, 0);
    _config_weather_layout->addWidget(_display_groupbox, 0, 1);

    _new_window->setLayout(_config_weather_layout);
    read_file_config_weather();
    update_thresholds_config();
    _new_window->show();
}

//////////////////////GRAPH WINDOW WEATHER//////////////////////////

void View::fill_graph_wind_dir()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create wind direction serie";
    _curves_wind_dir = new QLineSeries();
    _curves_wind_dir->append(0, _wind_direction_ave);
    _curves_wind_dir->setName("Wind direction");

    QPen pen_wind_dir(QColor(255, 0, 0, 255));
    pen_wind_dir.setWidth(2);
    pen_wind_dir.setStyle(Qt::SolidLine);
    _curves_wind_dir->setPen(pen_wind_dir);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_wind_dir = new QChart();
    _graph_wind_dir->addSeries(_curves_wind_dir);
    _graph_wind_dir->setTitle("Wind direction in function of time");
    _graph_wind_dir->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_wind_dir = new QValueAxis;
    axeX_wind_dir->setTitleText("Number of the measurement");
    axeX_wind_dir->setRange(0, NB_POINTS_GRAPH);
    _graph_wind_dir->addAxis(axeX_wind_dir, Qt::AlignBottom);
    _curves_wind_dir->attachAxis(axeX_wind_dir);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_wind_dir = new QValueAxis;
    axeY_wind_dir->setTitleText("Wind direction");
    axeY_wind_dir->setRange(y_error_min_wind_dir, y_error_max_wind_dir);
    _graph_wind_dir->addAxis(axeY_wind_dir, Qt::AlignLeft);
    _curves_wind_dir->attachAxis(axeY_wind_dir);
}

void View::fill_graph_wind_speed()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create wind speed serie";
    _curves_wind_speed = new QLineSeries();
    _curves_wind_speed->append(0, _wind_speed_ave);
    _curves_wind_speed->setName("Wind speed");

    QPen pen_wind_speed(QColor(0, 0, 255, 255));
    pen_wind_speed.setWidth(2);
    pen_wind_speed.setStyle(Qt::SolidLine);
    _curves_wind_speed->setPen(pen_wind_speed);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_wind_speed = new QChart();
    _graph_wind_speed->addSeries(_curves_wind_speed);
    _graph_wind_speed->setTitle("Wind speed in function of time");
    _graph_wind_speed->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_wind_speed = new QValueAxis;
    axeX_wind_speed->setTitleText("Number of the measurement");
    axeX_wind_speed->setRange(0, NB_POINTS_GRAPH);
    _graph_wind_speed->addAxis(axeX_wind_speed, Qt::AlignBottom);
    _curves_wind_speed->attachAxis(axeX_wind_speed);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_wind_speed = new QValueAxis;
    axeY_wind_speed->setTitleText("Wind speed");
    axeY_wind_speed->setRange(y_error_min_wind_speed, y_error_max_wind_speed);
    _graph_wind_speed->addAxis(axeY_wind_speed, Qt::AlignLeft);
    _curves_wind_speed->attachAxis(axeY_wind_speed);
}

void View::fill_graph_temp()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create temperature serie";
    _curves_temp = new QLineSeries();
    _curves_temp->append(0, _air_temp);
    _curves_temp->setName("Temperature");

    QPen pen_temp(QColor(0, 255, 255, 255));
    pen_temp.setWidth(2);
    pen_temp.setStyle(Qt::SolidLine);
    _curves_temp->setPen(pen_temp);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_temp = new QChart();
    _graph_temp->addSeries(_curves_temp);
    _graph_temp->setTitle("Temperature in function of time");
    _graph_temp->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_temp = new QValueAxis;
    axeX_temp->setTitleText("Number of the measurement");
    axeX_temp->setRange(0, NB_POINTS_GRAPH);
    _graph_temp->addAxis(axeX_temp, Qt::AlignBottom);
    _curves_temp->attachAxis(axeX_temp);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_temp = new QValueAxis;
    axeY_temp->setTitleText("Temperature");
    axeY_temp->setRange(y_error_min_temp, y_error_max_temp);
    _graph_temp->addAxis(axeY_temp, Qt::AlignLeft);
    _curves_temp->attachAxis(axeY_temp);
}

void View::fill_graph_dew_point()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create dew point serie";
    _curves_dew_point = new QLineSeries();
    _curves_dew_point->append(0, _dew_point);
    _curves_dew_point->setName("Dew point");

    QPen pen_dew_point(QColor(255, 150, 0, 255));
    pen_dew_point.setWidth(2);
    pen_dew_point.setStyle(Qt::SolidLine);
    _curves_dew_point->setPen(pen_dew_point);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_dew_point = new QChart();
    _graph_dew_point->addSeries(_curves_dew_point);
    _graph_dew_point->setTitle("Dew point in function of time");
    _graph_dew_point->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_dew_point = new QValueAxis;
    axeX_dew_point->setTitleText("Number of the measurement");
    axeX_dew_point->setRange(0, NB_POINTS_GRAPH);
    _graph_dew_point->addAxis(axeX_dew_point, Qt::AlignBottom);
    _curves_dew_point->attachAxis(axeX_dew_point);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_dew_point = new QValueAxis;
    axeY_dew_point->setTitleText("Dew point");
    axeY_dew_point->setRange(y_error_min_dew_point, y_error_max_dew_point);
    _graph_dew_point->addAxis(axeY_dew_point, Qt::AlignLeft);
    _curves_dew_point->attachAxis(axeY_dew_point);
}

void View::fill_graph_hum()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create humidity serie";
    _curves_hum = new QLineSeries();
    _curves_hum->append(0, _relat_humidity);
    _curves_hum->setName("Humidity");

    QPen pen_hum(QColor(150, 150, 255, 255));
    pen_hum.setWidth(2);
    pen_hum.setStyle(Qt::SolidLine);
    _curves_hum->setPen(pen_hum);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_hum = new QChart();
    _graph_hum->addSeries(_curves_hum);
    _graph_hum->setTitle("Humidity in function of time");
    _graph_hum->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_hum = new QValueAxis;
    axeX_hum->setTitleText("Number of the measurement");
    axeX_hum->setRange(0, NB_POINTS_GRAPH);
    _graph_hum->addAxis(axeX_hum, Qt::AlignBottom);
    _curves_hum->attachAxis(axeX_hum);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_hum = new QValueAxis;
    axeY_hum->setTitleText("Humidity");
    axeY_hum->setRange(y_error_min_hum, y_error_max_hum);
    _graph_hum->addAxis(axeY_hum, Qt::AlignLeft);
    _curves_hum->attachAxis(axeY_hum);
}

void View::fill_graph_pressure()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create pressure serie";
    _curves_press = new QLineSeries();
    _curves_press->append(0, _air_pressure);
    _curves_press->setName("Pressure");

    QPen pen_press(QColor(0, 150, 150, 255));
    pen_press.setWidth(2);
    pen_press.setStyle(Qt::SolidLine);
    _curves_press->setPen(pen_press);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_press = new QChart();
    _graph_press->addSeries(_curves_press);
    _graph_press->setTitle("Pressure in function of time");
    _graph_press->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_press = new QValueAxis;
    axeX_press->setTitleText("Number of the measurement");
    axeX_press->setRange(0, NB_POINTS_GRAPH);
    _graph_press->addAxis(axeX_press, Qt::AlignBottom);
    _curves_press->attachAxis(axeX_press);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_press = new QValueAxis;
    axeY_press->setTitleText("Pressure");
    axeY_press->setRange(y_error_min_press, y_error_max_press);
    _graph_press->addAxis(axeY_press, Qt::AlignLeft);
    _curves_press->attachAxis(axeY_press);
}

void View::fill_graph_voltage()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create voltage serie";
    _curves_volt = new QLineSeries();
    _curves_volt->append(0, _heating_voltage);
    _curves_volt->setName("Voltage");

    QPen pen_volt(QColor(150, 150, 150, 255));
    pen_volt.setWidth(2);
    pen_volt.setStyle(Qt::SolidLine);
    _curves_volt->setPen(pen_volt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_volt = new QChart();
    _graph_volt->addSeries(_curves_volt);
    _graph_volt->setTitle("Voltage in function of time");
    _graph_volt->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the serie";
    axeX_volt = new QValueAxis;
    axeX_volt->setTitleText("Number of the measurement");
    axeX_volt->setRange(0, NB_POINTS_GRAPH);
    _graph_volt->addAxis(axeX_volt, Qt::AlignBottom);
    _curves_volt->attachAxis(axeX_volt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the serie";
    axeY_volt = new QValueAxis;
    axeY_volt->setTitleText("Voltage");
    axeY_volt->setRange(y_error_min_volt, y_error_max_volt);
    _graph_volt->addAxis(axeY_volt, Qt::AlignLeft);
    _curves_volt->attachAxis(axeY_volt);
}

void View::set_tabs_weather_graph()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up the graph tab weather";
    _graph_tabs_weather = new QTabWidget(_new_window);
    _graph_tabs_weather->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
    _graph_tabs_weather->setIconSize(QSize(20, 20));
    _wind_direction_tab = new QWidget();
    _wind_speed_tab = new QWidget();
    _temperature_tab = new QWidget();
    _dew_point_tab = new QWidget();
    _humidity_tab = new QWidget();
    _pressure_tab = new QWidget();
    _voltage_tab = new QWidget();

    _graph_tabs_weather->addTab(_wind_direction_tab,"WIND DIRECTION");
    _graph_tabs_weather->addTab(_wind_speed_tab,"WIND SPEED");
    _graph_tabs_weather->addTab(_temperature_tab,"TEMPERATURE");
    _graph_tabs_weather->addTab(_dew_point_tab,"DEW POINT");
    _graph_tabs_weather->addTab(_humidity_tab,"HUMIDITY");
    _graph_tabs_weather->addTab(_pressure_tab,"PRESSURE");
    _graph_tabs_weather->addTab(_voltage_tab,"VOLTAGE");
}

void View::show_graph_weather()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create new window to show graph";
    _new_window = new QWidget();
    _new_window->setWindowTitle("SHOW GRAPH");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
    set_tabs_weather_graph();

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Fill then display weather graphs";
    fill_graph_wind_dir();
    fill_graph_wind_speed();
    fill_graph_temp();
    fill_graph_dew_point();
    fill_graph_hum();
    fill_graph_pressure();
    fill_graph_voltage();

    _display_graph_wind_dir = new QChartView(_graph_wind_dir,
                                        _wind_direction_tab);
    _display_graph_wind_dir->setRenderHint(QPainter::Antialiasing);
    _display_graph_wind_dir->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_wind_speed = new QChartView(_graph_wind_speed,
                                        _wind_speed_tab);
    _display_graph_wind_speed->setRenderHint(QPainter::Antialiasing);
    _display_graph_wind_speed->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_temp = new QChartView(_graph_temp, _temperature_tab);
    _display_graph_temp->setRenderHint(QPainter::Antialiasing);
    _display_graph_temp->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_dew_point = new QChartView(_graph_dew_point, _dew_point_tab);
    _display_graph_dew_point->setRenderHint(QPainter::Antialiasing);
    _display_graph_dew_point->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_hum = new QChartView(_graph_hum, _humidity_tab);
    _display_graph_hum->setRenderHint(QPainter::Antialiasing);
    _display_graph_hum->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_press = new QChartView(_graph_press, _pressure_tab);
    _display_graph_press->setRenderHint(QPainter::Antialiasing);
    _display_graph_press->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_volt = new QChartView(_graph_volt, _voltage_tab);
    _display_graph_volt->setRenderHint(QPainter::Antialiasing);
    _display_graph_volt->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Set up timer weather graph, Y axis autoscale";
    _timer_graph_weather = new QTimer(_new_window);
    QObject::connect(_timer_graph_weather, SIGNAL(timeout()),
                        this, SLOT(update_graph_weather()));
    _timer_graph_weather->start(DISPLAY_GRAPH_WEATHER);
    _new_window->show();
}

void View::update_wind_dir()
{
    int y = _wind_direction_ave;
    if(number_points_wind_dir<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_wind_dir) || (y > y_error_max_wind_dir))
        {
            if(y < y_error_min_wind_dir)
                y_error_min_wind_dir = y;
            if(y > y_error_max_wind_dir)
                y_error_max_wind_dir = y;
            axeY_wind_dir->setRange(y_error_min_wind_dir-1,
                                    y_error_max_wind_dir+1);
        }
        _curves_wind_dir->append(number_points_wind_dir, y);
        number_points_wind_dir++;
    }
    else{
        number_points_wind_dir = 1;
        y_error_min_wind_dir = y;
        y_error_min_wind_dir = y;
        _curves_wind_dir->clear();
        axeY_wind_dir->setRange(y_error_min_wind_dir-1, y_error_min_wind_dir+1);
    }
}

void View::update_wind_speed()
{
    int y = _wind_speed_ave;
    if(number_points_wind_speed<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_wind_speed) || (y > y_error_max_wind_speed))
        {
            if(y < y_error_min_wind_speed)
                y_error_min_wind_speed = y;
            if(y > y_error_max_wind_speed)
                y_error_max_wind_speed = y;
            axeY_wind_speed->setRange(y_error_min_wind_speed-1,
                                    y_error_max_wind_speed+1);
        }
        _curves_wind_speed->append(number_points_wind_speed, y);
        number_points_wind_speed++;
    }
    else{
        number_points_wind_speed = 1;
        y_error_min_wind_speed = y;
        y_error_max_wind_speed = y;
        _curves_wind_speed->clear();
        axeY_wind_speed->setRange(y_error_min_wind_speed-1,
                                  y_error_max_wind_speed+1);
    }
}

void View::update_temp()
{
    int y = _air_temp;
    if(number_points_temp<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_temp) || (y > y_error_max_temp))
        {
            if(y < y_error_min_temp)
                y_error_min_temp = y;
            if(y > y_error_max_temp)
                y_error_max_temp = y;
            axeY_temp->setRange(y_error_min_temp-1, y_error_max_temp+1);
        }
        _curves_temp->append(number_points_temp, y);
        number_points_temp++;
    }
    else{
        number_points_temp = 1;
        y_error_min_temp = y;
        y_error_max_temp = y;
        _curves_temp->clear();
        axeY_temp->setRange(y_error_min_temp-1, y_error_max_temp+1);
    }
}

void View::update_dew_point()
{
    int y = _dew_point;
    if(number_points_dew_point<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_dew_point) || (y > y_error_max_dew_point))
        {
            if(y < y_error_min_dew_point)
                y_error_min_dew_point = y;
            if(y > y_error_max_dew_point)
                y_error_max_dew_point = y;
            axeY_dew_point->setRange(y_error_min_dew_point-1,
                                     y_error_max_dew_point+1);
        }
        _curves_dew_point->append(number_points_dew_point, y);
        number_points_dew_point++;
    }
    else{
        number_points_dew_point = 1;
        y_error_min_dew_point = y;
        y_error_max_dew_point = y;
        _curves_dew_point->clear();
        axeY_dew_point->setRange(y_error_min_dew_point-1,
                                 y_error_max_dew_point+1);
    }
}

void View::update_hum()
{
    int y = _relat_humidity;
    if(number_points_hum<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_hum) || (y > y_error_max_hum))
        {
            if(y < y_error_min_hum)
                y_error_min_hum = y;
            if(y > y_error_max_hum)
                y_error_max_hum = y;
            axeY_hum->setRange(y_error_min_hum-1, y_error_max_hum+1);
        }
        _curves_hum->append(number_points_hum, y);
        number_points_hum++;
    }
    else{
        number_points_hum = 1;
        y_error_min_hum = y;
        y_error_max_hum = y;
        _curves_hum->clear();
        axeY_hum->setRange(y_error_min_hum-1, y_error_max_hum+1);
    }
}

void View::update_pressure()
{
    int y = _air_pressure;
    if(number_points_press<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_press) || (y > y_error_max_press))
        {
            if(y < y_error_min_press)
                y_error_min_press = y;
            if(y > y_error_max_press)
                y_error_max_press = y;
            axeY_press->setRange(y_error_min_press-1, y_error_max_press+1);
        }
        _curves_press->append(number_points_press, y);
        number_points_press++;
    }
    else{
        number_points_press = 1;
        y_error_min_press = y;
        y_error_max_press = y;
        _curves_press->clear();
        axeY_press->setRange(y_error_min_press-1, y_error_max_press+1);
    }
}

void View::update_voltage()
{
    int y = _heating_voltage;
    if(number_points_volt<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_volt) || (y > y_error_max_volt))
        {
            if(y < y_error_min_volt)
                y_error_min_volt = y;
            if(y > y_error_max_volt)
                y_error_max_volt = y;
            axeY_volt->setRange(y_error_min_volt-1, y_error_max_volt+1);
        }
        _curves_volt->append(number_points_volt, y);
        number_points_volt++;
    }
    else{
        number_points_volt = 1;
        y_error_min_volt = y;
        y_error_max_volt = y;
        _curves_volt->clear();
        axeY_volt->setRange(y_error_min_volt-1, y_error_max_volt+1);
    }
}

void View::update_graph_weather()
{
    update_wind_dir();
    update_wind_speed();
    update_temp();
    update_dew_point();
    update_hum();
    update_pressure();
    update_voltage();
}

////////////////////////////////TELESCOPE FUNCTIONS/////////////////////////

void View::map_command_payload_telescope()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Connection with telescope MQTT publish slots";
    _signal_mapper_mqtt = new QSignalMapper(this);

    connect (_full_home_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_AZ_home_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_ALT_home_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_home, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect (_west_button_big, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_west_button_small, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_east_button_big, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_east_button_small, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_north_button_big, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_north_button_small, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_south_button_big, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_south_button_small, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_button, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect (_stop_tracking, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_sideral_velocity, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_sun_velocity, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_moon_velocity, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect (_connect_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_disconnect_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_park_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_unpark_telescope, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_park, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect (_goto_rad_dec, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_sync_ra_dec, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_ra_dec, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_goto_alt_az, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_sync_alt_az, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_alt_az, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect (_load_traj, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_follow_traj, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect (_stop_sat, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    telescope_home_full = "telescope_home_full,2,0.0,0.0";
    telescope_home_AZ = "telescope_home_AZ,1,0.0";
    telescope_home_ALT = "telescope_home_ALT,1,0.0";
    telescope_stop = "telescope_stop";

    telescope_handcontrol_west = "telescope_handcontrol_west,4,0,10.0,0.0,0.0";
    telescope_handcontrol_east = "telescope_handcontrol_east,4,0,10.0,0.0,0.0";
    telescope_handcontrol_north = "telescope_handcontrol_north,4,0,10.0,0.0,0.0";
    telescope_handcontrol_south = "telescope_handcontrol_south,4,0,10.0,0.0,0.0";

    telescope_tracking_sideral_velocity = "telescope_tracking_sideral_velocity,1,5.0";
    telescope_tracking_sun_velocity = "telescope_tracking_sun_velocity,1,3.0";
    telescope_tracking_moon_velocity = "telescope_tracking_moon_velocity,1,3.0";

    telescope_connect = "telescope_connect";
    telescope_disconnect = "telescope_disconnect";
    telescope_park = "telescope_park,2,0.0,180.0";
    telescope_unpark = "telescope_unpark";

    telescope_radec_goto = "telescope_radec_goto,7,0,0.0,0.0,0.0,0.0,0.0,0.0";
    telescope_radec_sync = "telescope_radec_sync,7,0,0.0,0.0,0.0,0.0,0.0,0.0";
    telescope_altaz_goto = "telescope_altaz_goto,6,0.0,0.0,0.0,0.0,0.0,0.0";
    telescope_altaz_sync = "telescope_altaz_sync,6,0.0,0.0,0.0,0.0,0.0,0.0";

    telescope_satellite_load_traj = "telescope_satellite_load_traj";
    telescope_satellite_follow_traj = "telescope_satellite_follow_traj";

    _signal_mapper_mqtt->setMapping(_full_home_telescope, telescope_home_full.c_str());
    _signal_mapper_mqtt->setMapping(_AZ_home_telescope, telescope_home_AZ.c_str());
    _signal_mapper_mqtt->setMapping(_ALT_home_telescope, telescope_home_ALT.c_str());
    _signal_mapper_mqtt->setMapping(_stop_home, telescope_stop.c_str());

    _signal_mapper_mqtt->setMapping(_west_button_big, telescope_handcontrol_west.c_str());
    _signal_mapper_mqtt->setMapping(_west_button_small, telescope_handcontrol_west.c_str());
    _signal_mapper_mqtt->setMapping(_east_button_big, telescope_handcontrol_east.c_str());
    _signal_mapper_mqtt->setMapping(_east_button_small, telescope_handcontrol_east.c_str());
    _signal_mapper_mqtt->setMapping(_north_button_big, telescope_handcontrol_north.c_str());
    _signal_mapper_mqtt->setMapping(_north_button_small, telescope_handcontrol_north.c_str());
    _signal_mapper_mqtt->setMapping(_south_button_big, telescope_handcontrol_south.c_str());
    _signal_mapper_mqtt->setMapping(_south_button_small, telescope_handcontrol_south.c_str());
    _signal_mapper_mqtt->setMapping(_stop_button, telescope_stop.c_str());

    _signal_mapper_mqtt->setMapping(_stop_tracking, telescope_stop.c_str());
    _signal_mapper_mqtt->setMapping(_sideral_velocity, telescope_tracking_sideral_velocity.c_str());
    _signal_mapper_mqtt->setMapping(_sun_velocity, telescope_tracking_sun_velocity.c_str());
    _signal_mapper_mqtt->setMapping(_moon_velocity, telescope_tracking_moon_velocity.c_str());

    _signal_mapper_mqtt->setMapping(_connect_telescope, telescope_connect.c_str());
    _signal_mapper_mqtt->setMapping(_disconnect_telescope, telescope_disconnect.c_str());
    _signal_mapper_mqtt->setMapping(_park_telescope, telescope_park.c_str());
    _signal_mapper_mqtt->setMapping(_unpark_telescope, telescope_unpark.c_str());
    _signal_mapper_mqtt->setMapping(_stop_park, telescope_stop.c_str());

    _signal_mapper_mqtt->setMapping(_goto_rad_dec, telescope_radec_goto.c_str());
    _signal_mapper_mqtt->setMapping(_sync_ra_dec, telescope_radec_sync.c_str());
    _signal_mapper_mqtt->setMapping(_stop_ra_dec, telescope_stop.c_str());
    _signal_mapper_mqtt->setMapping(_goto_alt_az, telescope_altaz_goto.c_str());
    _signal_mapper_mqtt->setMapping(_sync_alt_az, telescope_altaz_sync.c_str());
    _signal_mapper_mqtt->setMapping(_stop_alt_az, telescope_stop.c_str());

    _signal_mapper_mqtt->setMapping(_load_traj, telescope_satellite_load_traj.c_str());
    _signal_mapper_mqtt->setMapping(_follow_traj, telescope_satellite_follow_traj.c_str());
    _signal_mapper_mqtt->setMapping(_stop_sat, telescope_stop.c_str());

}

void View::map_command_payload_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Connection with dome MQTT publish slots";

    connect(_full_home_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_AZ_home_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_shutter_home_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_stop_home_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect(_west_button_big_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_west_button_small_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_east_button_big_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_east_button_small_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_stop_button_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect(_connect_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_disconnect_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_park_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_unpark_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_stop_park_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect(_goto_alt_az_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_sync_alt_az_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_speed_alt_az_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_stop_alt_az_dome, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    connect(_open_shutter, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_close_shutter, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_sync_shutter, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));
    connect(_stop_move_shutter, SIGNAL(clicked()), _signal_mapper_mqtt, SLOT(map()));

    dome_home_full = "dome_home_full,2,0.0,0.0";
    dome_home_AZ = "dome_home_AZ,1,0.0";
    dome_home_shutter = "dome_home_shutter,1,0.0";
    dome_stop = "dome_stop";

    dome_handcontrol_west = "dome_handcontrol_west,2,0.0,0.0";
    dome_handcontrol_east = "dome_handcontrol_east,2,0.0,0.0";

    dome_connect = "dome_connect";
    dome_disconnect = "dome_disconnect";
    dome_park = "dome_park,2,0.0,180.0";
    dome_unpark = "dome_unpark";

    dome_az_goto = "dome_az_goto,2,0.0,0.0";
    dome_az_sync = "dome_az_sync,2,0.0,0.0";
    dome_az_speed = "dome_az_speed,2,0.0,0.0";

    dome_shutter_open = "dome_shutter_open,1,100";
    dome_shutter_close = "dome_shutter_close,1,50";
    dome_shutter_sync = "dome_shutter_sync,1,0";

    _signal_mapper_mqtt->setMapping(_full_home_dome, dome_home_full.c_str());
    _signal_mapper_mqtt->setMapping(_AZ_home_dome, dome_home_AZ.c_str());
    _signal_mapper_mqtt->setMapping(_shutter_home_dome, dome_home_shutter.c_str());
    _signal_mapper_mqtt->setMapping(_stop_home_dome, dome_stop.c_str());

    _signal_mapper_mqtt->setMapping(_west_button_big_dome, dome_handcontrol_west.c_str());
    _signal_mapper_mqtt->setMapping(_west_button_small_dome, dome_handcontrol_west.c_str());
    _signal_mapper_mqtt->setMapping(_east_button_big_dome, dome_handcontrol_east.c_str());
    _signal_mapper_mqtt->setMapping(_east_button_small_dome, dome_handcontrol_east.c_str());
    _signal_mapper_mqtt->setMapping(_stop_button_dome, dome_stop.c_str());

    _signal_mapper_mqtt->setMapping(_connect_dome, dome_connect.c_str());
    _signal_mapper_mqtt->setMapping(_disconnect_dome, dome_disconnect.c_str());
    _signal_mapper_mqtt->setMapping(_park_dome, dome_park.c_str());
    _signal_mapper_mqtt->setMapping(_unpark_dome, dome_unpark.c_str());
    _signal_mapper_mqtt->setMapping(_stop_park_dome, dome_stop.c_str());

    _signal_mapper_mqtt->setMapping(_goto_alt_az_dome, dome_az_goto.c_str());
    _signal_mapper_mqtt->setMapping(_sync_alt_az_dome, dome_az_sync.c_str());
    _signal_mapper_mqtt->setMapping(_speed_alt_az_dome, dome_az_speed.c_str());
    _signal_mapper_mqtt->setMapping(_stop_alt_az_dome, dome_stop.c_str());

    _signal_mapper_mqtt->setMapping(_open_shutter, dome_shutter_open.c_str());
    _signal_mapper_mqtt->setMapping(_close_shutter, dome_shutter_close.c_str());
    _signal_mapper_mqtt->setMapping(_sync_shutter, dome_shutter_sync.c_str());
    _signal_mapper_mqtt->setMapping(_stop_move_shutter, dome_stop.c_str());

    connect(_signal_mapper_mqtt, SIGNAL(mapped(const QString &)), this,
            SLOT(mqtt_state_publish(const QString &)));
}

void View::set_tabWidgets()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up the main window tabs";
    _main_tabs = new QTabWidget(this);
    _main_tabs->setFixedSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
    _main_tabs->setIconSize(QSize(40, 40));
    _equipement_tab = new QWidget();
    _sequence_tab = new QWidget();
    _imaging_tab = new QWidget();
    _options_tab = new QWidget();
    _help_tab = new QWidget();
    _main_tabs->addTab(_equipement_tab,"EQUIPMENT");
    _main_tabs->addTab(_sequence_tab,"SEQUENCE");
    _main_tabs->addTab(_imaging_tab,"IMAGING");
    _main_tabs->addTab(_options_tab,"OPTIONS");
    _main_tabs->addTab(_help_tab,"HELP");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add icons in the main window tabs";
    _main_tabs->setTabIcon(0,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/equipment.png"));
    _main_tabs->setTabIcon(1,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/prioritize.png"));
    _main_tabs->setTabIcon(2,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/imaging.png"));
    _main_tabs->setTabIcon(3,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/settings.png"));
    _main_tabs->setTabIcon(4,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/help.png"));

    QPixmap newpix(QCoreApplication::applicationDirPath() +
                   "/../ogs_ihm/Images/new.png");
    QPixmap openpix(QCoreApplication::applicationDirPath() +
                   "/../ogs_ihm/Images/open.png");
    QPixmap quitpix(QCoreApplication::applicationDirPath() +
                   "/../ogs_ihm/Images/remove.png");
    QPixmap connectpix(QCoreApplication::applicationDirPath() +
                   "/../ogs_ihm/Images/connect.png");
    _toolbar = new QToolBar(_sequence_tab);
    _new = _toolbar->addAction(newpix, "New");
    _toolbar->addAction(openpix, "Open");
    _toolbar->addAction(connectpix, "Connect");
    _toolbar->addSeparator();
    _quit = _toolbar->addAction(quitpix, "Quit");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up the equipments tab";
    _equipment_tabs = new QTabWidget(_equipement_tab);
    _equipment_tabs->setIconSize(QSize(40, 40));
    _telescope_tab = new QWidget();
    _dome_tab = new QWidget();
    _actuator_tab = new QWidget();
    _actuators_M4_M5_tab = new QWidget();
    _camera_tab = new QWidget();
    _weather_tab = new QWidget();
    _position_tab = new QWidget();
    _equipment_tabs->addTab(_telescope_tab,"TELESCOPE");
    _equipment_tabs->addTab(_dome_tab,"DOME");
    _equipment_tabs->addTab(_actuator_tab,"ACTUATORS M2 M3");
    _equipment_tabs->addTab(_actuators_M4_M5_tab,"ACTUATORS M4 M5");
    _equipment_tabs->addTab(_camera_tab,"CAMERA");
    _equipment_tabs->addTab(_weather_tab,"WEATHER");
    _equipment_tabs->addTab(_position_tab,"POSITION");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add icons in the equipments tab";
    _equipment_tabs->setTabIcon(0,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/telescope.png"));
    _equipment_tabs->setTabIcon(1,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/dome.png"));
    _equipment_tabs->setTabIcon(2,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/actuator.png"));
    _equipment_tabs->setTabIcon(3,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/actuator.png"));
    _equipment_tabs->setTabIcon(4,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/camera.png"));
    _equipment_tabs->setTabIcon(5,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/climate.png"));
    _equipment_tabs->setTabIcon(6,
        QIcon(QCoreApplication::applicationDirPath() +
              "/../ogs_ihm/Images/sky_map.png"));
}

QGroupBox* View::hand_control()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up hand control group box";
    QGroupBox *_commands_groupbox = new QGroupBox("Hand control");
    QGridLayout *command_layout = new QGridLayout;

    _west_button_big = new QPushButton("W ++");
    _west_button_big->setToolTip("WEST movement");
    command_layout->addWidget(_west_button_big, 2, 0);
    _west_button_small = new QPushButton("W +");
    _west_button_small->setToolTip("Small WEST movement");
    command_layout->addWidget(_west_button_small, 2, 1);
    _east_button_big = new QPushButton("E ++");
    _east_button_big->setToolTip("EAST movement");
    command_layout->addWidget(_east_button_big, 2, 4);
    _east_button_small = new QPushButton("E +");
    _east_button_small->setToolTip("Small EAST movement");
    command_layout->addWidget(_east_button_small, 2, 3);

    _north_button_big = new QPushButton("N ++");
    _north_button_big->setToolTip("NORTH movement");
    command_layout->addWidget(_north_button_big, 0, 2);
    _north_button_small = new QPushButton("N +");
    _north_button_small->setToolTip("Small NORTH movement");
    command_layout->addWidget(_north_button_small, 1, 2);
    _south_button_big = new QPushButton("S ++");
    _south_button_big->setToolTip("SOUTH movement");
    command_layout->addWidget(_south_button_big, 4, 2);
    _south_button_small = new QPushButton("S +");
    _south_button_small->setToolTip("Small SOUTH movement");
    command_layout->addWidget(_south_button_small, 3, 2);
    _stop_button = new QPushButton("STOP");
    _stop_button->setToolTip("Emergency STOP");
    command_layout->addWidget(_stop_button, 2, 2);

    _degree_hand_control = new QDoubleSpinBox;
    _degree_hand_control->setRange(0, 360);
    _degree_hand_control->setSingleStep(1);
    _degree_hand_control->setValue(0);
    _degree_hand_control->setToolTip("Between 0 and 360 degrees");
    command_layout->addWidget(_degree_hand_control, 5, 0);

    _minute_hand_control = new QDoubleSpinBox;
    _minute_hand_control->setRange(0, 60);
    _minute_hand_control->setSingleStep(1);
    _minute_hand_control->setValue(0);
    _minute_hand_control->setToolTip("Between 0 and 60 degrees");
    command_layout->addWidget(_minute_hand_control, 5, 2);

    _second_hand_control = new QDoubleSpinBox;
    _second_hand_control->setRange(0, 60);
    _second_hand_control->setSingleStep(1);
    _second_hand_control->setValue(0);
    _second_hand_control->setToolTip("Between 0 and 60 degrees");
    command_layout->addWidget(_second_hand_control, 5, 4);

    _degree_label = new QLabel("°");
    command_layout->addWidget(_degree_label, 5, 1);
    _minute_label = new QLabel("'");
    command_layout->addWidget(_minute_label, 5, 3);
    _second_label = new QLabel("\"");
    command_layout->addWidget(_second_label, 5, 5);

    _JOG_movement = new QRadioButton("JOG");
    _JOG_movement->setChecked(true);
    command_layout->addWidget(_JOG_movement, 6, 1);
    _velocity_movement = new QRadioButton("/sec");
    command_layout->addWidget(_velocity_movement, 6, 3);

    _ratio_label = new QLabel("Ratio\nSmall/Big\nmovement");
    command_layout->addWidget(_ratio_label, 7, 1);
    _ratio_combobox = new QComboBox();
    _ratio_combobox->addItem("5");
    _ratio_combobox->addItem("10");
    _ratio_combobox->addItem("15");
    _ratio_combobox->setCurrentText("10");
    command_layout->addWidget(_ratio_combobox, 7, 3);
    _commands_groupbox->setLayout(command_layout);

    QObject::connect(_west_button_big, SIGNAL(clicked()),
                     this, SLOT(west_big_slot()));
    QObject::connect(_west_button_small, SIGNAL(clicked()),
                     this, SLOT(west_small_slot()));

    QObject::connect(_east_button_big, SIGNAL(clicked()),
                     this, SLOT(east_big_slot()));
    QObject::connect(_east_button_small, SIGNAL(clicked()),
                     this, SLOT(east_small_slot()));

    QObject::connect(_north_button_big, SIGNAL(clicked()),
                     this, SLOT(north_big_slot()));
    QObject::connect(_north_button_small, SIGNAL(clicked()),
                     this, SLOT(north_small_slot()));

    QObject::connect(_south_button_big, SIGNAL(clicked()),
                     this, SLOT(south_big_slot()));
    QObject::connect(_south_button_small, SIGNAL(clicked()),
                     this, SLOT(south_small_slot()));

    QObject::connect(_stop_button, SIGNAL(clicked()),
                     this, SLOT(stop_button_slot()));

    return _commands_groupbox;
}

void View::west_big_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    int ratio = _ratio_combobox->currentText().toInt();
    float degrees = _degree_hand_control->value()*static_cast<float>(ratio);
    float minutes = _minute_hand_control->value()*static_cast<float>(ratio);
    float seconds = _second_hand_control->value()*static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;
    std::ostringstream ss_second;
    ss_second << seconds;

    telescope_handcontrol_west = "telescope_handcontrol_west," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_west_button_big,
            telescope_handcontrol_west.c_str());
}

void View::west_small_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control->value();
    std::ostringstream ss_second;
    ss_second << _second_hand_control->value();

    telescope_handcontrol_west = "telescope_handcontrol_west," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_west_button_small,
            telescope_handcontrol_west.c_str());
}

void View::east_big_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    int ratio = _ratio_combobox->currentText().toInt();
    float degrees = _degree_hand_control->value()*static_cast<float>(ratio);
    float minutes = _minute_hand_control->value()*static_cast<float>(ratio);
    float seconds = _second_hand_control->value()*static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;
    std::ostringstream ss_second;
    ss_second << seconds;

    telescope_handcontrol_east = "telescope_handcontrol_east," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_east_button_big,
            telescope_handcontrol_east.c_str());
}

void View::east_small_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control->value();
    std::ostringstream ss_second;
    ss_second << _second_hand_control->value();

    telescope_handcontrol_east = "telescope_handcontrol_east," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_east_button_small,
            telescope_handcontrol_east.c_str());
}

void View::north_big_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    int ratio = _ratio_combobox->currentText().toInt();
    float degrees = _degree_hand_control->value()*static_cast<float>(ratio);
    float minutes = _minute_hand_control->value()*static_cast<float>(ratio);
    float seconds = _second_hand_control->value()*static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;
    std::ostringstream ss_second;
    ss_second << seconds;

    telescope_handcontrol_north = "telescope_handcontrol_north," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_north_button_big,
            telescope_handcontrol_north.c_str());
}

void View::north_small_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control->value();
    std::ostringstream ss_second;
    ss_second << _second_hand_control->value();

    telescope_handcontrol_north = "telescope_handcontrol_north," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_north_button_small,
            telescope_handcontrol_north.c_str());
}

void View::south_big_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    int ratio = _ratio_combobox->currentText().toInt();
    float degrees = _degree_hand_control->value()*static_cast<float>(ratio);
    float minutes = _minute_hand_control->value()*static_cast<float>(ratio);
    float seconds = _second_hand_control->value()*static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;
    std::ostringstream ss_second;
    ss_second << seconds;

    telescope_handcontrol_south = "telescope_handcontrol_south," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_south_button_big,
            telescope_handcontrol_south.c_str());
}

void View::south_small_slot()
{
    int movement;
    if(_JOG_movement->isChecked() == true)
        movement = 0;
    else
        movement = 1;

    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control->value();
    std::ostringstream ss_second;
    ss_second << _second_hand_control->value();

    telescope_handcontrol_south = "telescope_handcontrol_south," +
            to_string(4) + "," + to_string(movement) + "," +
            ss_degree.str() + "," + ss_minute.str() + "," + ss_second.str();

    _signal_mapper_mqtt->setMapping(_south_button_small,
            telescope_handcontrol_south.c_str());
}

void View::stop_button_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Hand control telescope STOP button clicked";
    QPalette pal = _stop_button->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_button->setAutoFillBackground(true);
    _stop_button->setPalette(pal);
    _stop_button->update();

    _string_logging = _string_logging + "Hand control STOP button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::tracking()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up tracking group box";

    QGroupBox *_tracking_groupbox = new QGroupBox("Tracking");
    QGridLayout *tracking_layout = new QGridLayout;
    _sideral_velocity = new QPushButton("Sideral");
    tracking_layout->addWidget(_sideral_velocity, 0, 0);
    _sun_velocity = new QPushButton("Sun");
    tracking_layout->addWidget(_sun_velocity, 0, 1);
    _moon_velocity = new QPushButton("Moon");
    tracking_layout->addWidget(_moon_velocity, 0, 2);
    _stop_tracking = new QPushButton("STOP");
    _stop_tracking->setToolTip("Emergency Stop tracking");
    tracking_layout->addWidget(_stop_tracking, 1, 1);
    _sync_dome = new QCheckBox("SYNC DOME");
    _sync_dome->setChecked(true);
    tracking_layout->addWidget(_sync_dome, 2, 1);
    _tracking_groupbox->setLayout(tracking_layout);

    QObject::connect(_stop_tracking, SIGNAL(clicked()),
                     this, SLOT(stop_tracking_slot()));

    return _tracking_groupbox;
}

void View::stop_tracking_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Tracking telescope stop button clicked";
    QPalette pal = _stop_tracking->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_tracking->setAutoFillBackground(true);
    _stop_tracking->setPalette(pal);
    _stop_tracking->update();

    _string_logging = _string_logging + "Tracking STOP button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::satellite_traj()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up satellite trajectory group box";

    QGroupBox *_satellite_groupbox = new QGroupBox("Satellite");
    QGridLayout *satellite_layout = new QGridLayout;
    _choose_file = new QPushButton("Choose file");
    satellite_layout->addWidget(_choose_file, 0, 0);
    _load_traj = new QPushButton("Load trajectory");
    satellite_layout->addWidget(_load_traj, 0, 1);
    _follow_traj = new QPushButton("Follow trajectory");
    satellite_layout->addWidget(_follow_traj, 0, 2);
    _config_sat = new QPushButton("Config");
    satellite_layout->addWidget(_config_sat, 1, 1);
    _stop_sat = new QPushButton("STOP");
    _stop_sat->setToolTip("Emergency Stop satellite");
    satellite_layout->addWidget(_stop_sat, 1, 2);
    _satellite_groupbox->setLayout(satellite_layout);

    QObject::connect(_stop_sat, SIGNAL(clicked()),
                     this, SLOT(stop_satellite_slot()));

    return _satellite_groupbox;
}

void View::stop_satellite_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Satellite stop button clicked";
    QPalette pal = _stop_sat->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_sat->setAutoFillBackground(true);
    _stop_sat->setPalette(pal);
    _stop_sat->update();

    _string_logging = _string_logging + "STOP satellite button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::controller_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up controller infos group box";

    QGroupBox *_controller_groupbox = new QGroupBox("Controller infos");
    QVBoxLayout *controller_layout = new QVBoxLayout;

    _error_AZ_label = new QLabel("Error AZ: 0 step 0 arc/s");
    controller_layout->addWidget(_error_AZ_label);
    _error_ALT_label = new QLabel("Error ALT: 0 step 0 arc/s");
    controller_layout->addWidget(_error_ALT_label);
    _power_AZ_label = new QLabel("Power AZ: 0 W");
    controller_layout->addWidget(_power_AZ_label);
    _power_ALT_label = new QLabel("Power ALT: 0 W");
    controller_layout->addWidget(_power_ALT_label);
    _az_brake_label = new QLabel("AZ Brake: False");
    controller_layout->addWidget(_az_brake_label);
    _dec_brake_label = new QLabel("Dec Brake: False");
    controller_layout->addWidget(_dec_brake_label);

    _AZ_display_label = new QLabel("AZ value: 0° 0' 0\"");
    controller_layout->addWidget(_AZ_display_label);
    _ALT_display_label = new QLabel("ALT value: 0° 0' 0\"");
    controller_layout->addWidget(_ALT_display_label);
    _ra_label = new QLabel("RA value: 0° 0' 0\"");
    controller_layout->addWidget(_ra_label);
    _dec_label = new QLabel("Dec value: 0° 0' 0\"");
    controller_layout->addWidget(_dec_label);
    _controller_groupbox->setLayout(controller_layout);

    return _controller_groupbox;
}

QGroupBox* View::show_loggging()
{
    _string_logging = "Telescope logging !!!!\n";
    _logging_label = new QLabel(_string_logging, this);
    _logging_label->resize(QSize(1000, 1000));

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up logging label scrollbar";
    _logging_scrollArea = new QScrollArea(this);
    _logging_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _logging_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _logging_scrollArea->setWidget(_logging_label);
    _logging_scrollArea->setWidgetResizable(true);
    _logging_scrollArea->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scrollArea);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::date_time()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up date and time group box";

    QGroupBox *_infos_groupbox = new QGroupBox("Date and time");
    QVBoxLayout *infos_layout = new QVBoxLayout;
    _date = new QLabel("Date: " +
                QDateTime::currentDateTime().toString("dddd d MMMM yyyy"));
    infos_layout->addWidget(_date);
    _timer = new QTimer(this);
    _dateTimeUTC = new
        QLabel("UTC Time: " +
               QDateTime::currentDateTimeUtc().toString("h:m:s ap"));
    infos_layout->addWidget(_dateTimeUTC);
    _dateTime_local = new
        QLabel("Local time: " +
               QDateTime::currentDateTime().toString("h:m:s ap"));
    infos_layout->addWidget(_dateTime_local);
    _infos_groupbox->setLayout(infos_layout);

    return _infos_groupbox;
}

QGroupBox* View::connection_telescope()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Set up connection telescope/MQTT group box";
    QGroupBox *_connect_groupbox = new QGroupBox("Connection");
    QVBoxLayout *connect_layout = new QVBoxLayout;

    QGroupBox *_telescope_groupbox = new QGroupBox("Connection Telescope");
    QHBoxLayout *telescope_layout = new QHBoxLayout;
    _connect_telescope = new QPushButton("Connect");
    telescope_layout->addWidget(_connect_telescope);
    _disconnect_telescope = new QPushButton("Disconnect");
    telescope_layout->addWidget(_disconnect_telescope);
    _telescope_groupbox->setLayout(telescope_layout);

    QGroupBox *_mqtt_groupbox = new QGroupBox("Connection MQTT server");
    QHBoxLayout *mqtt_layout = new QHBoxLayout;
    _connect_mqtt = new QPushButton("Connect");
    mqtt_layout->addWidget(_connect_mqtt);
    _disconnect_mqtt = new QPushButton("Disconnect");
    mqtt_layout->addWidget(_disconnect_mqtt);
    _blinking_mqtt_pub = new QPushButton("PUBLISH");
    mqtt_layout->addWidget(_blinking_mqtt_pub);
    _mqtt_groupbox->setLayout(mqtt_layout);

    connect_layout->addWidget(_telescope_groupbox);
    connect_layout->addWidget(_mqtt_groupbox);
    _connect_groupbox->setLayout(connect_layout);

    QObject::connect(_connect_telescope, SIGNAL(clicked()),
                    this, SLOT(connect_telescope_slot()));
    QObject::connect(_connect_mqtt, SIGNAL(clicked()),
                     this, SLOT(mqtt_connect_publish()));
    QObject::connect(_disconnect_mqtt, SIGNAL(clicked()),
                     this, SLOT(mqtt_disconnect_publish()));

    return _connect_groupbox;
}

void View::connect_telescope_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Connect telescope button clicked";
    QPalette pal = _connect_telescope->palette();
    pal.setColor(QPalette::Button, QColor(Qt::green));
    _connect_telescope->setAutoFillBackground(true);
    _connect_telescope->setPalette(pal);
    _connect_telescope->update();

    _string_logging = _string_logging + "Connect telescope button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::home_telescope()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up home telescope group box";

    QGroupBox *_home_groupbox = new QGroupBox("Home Telescope");
    QGridLayout *home_layout = new QGridLayout;
    _full_home_telescope = new QPushButton("FULL HOME");
    home_layout->addWidget(_full_home_telescope, 0, 0);
    _AZ_home_telescope = new QPushButton("AZ HOME");
    home_layout->addWidget(_AZ_home_telescope, 0, 1);
    _ALT_home_telescope = new QPushButton("ALT HOME");
    home_layout->addWidget(_ALT_home_telescope, 0, 2);
    _stop_home = new QPushButton("STOP");
    _stop_home->setToolTip("Emergency Stop homing");
    home_layout->addWidget(_stop_home, 1, 1);
    _home_groupbox->setLayout(home_layout);

    QObject::connect(_stop_home, SIGNAL(clicked()),
                     this, SLOT(stop_home_slot()));

    return _home_groupbox;
}

void View::stop_home_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Stop home button clicked";
    QPalette pal = _stop_home->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_home->setAutoFillBackground(true);
    _stop_home->setPalette(pal);
    _stop_home->update();

    _string_logging = _string_logging + "STOP home button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::park_telescope()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up park telescope group box";

    QGroupBox *_park_groupbox = new QGroupBox("Park Telescope");
    QGridLayout *park_layout = new QGridLayout;
    _park_telescope = new QPushButton("PARK");
    park_layout->addWidget(_park_telescope, 2, 0);
    _unpark_telescope = new QPushButton("UNPARK");
    park_layout->addWidget(_unpark_telescope, 2, 1);
    _stop_park = new QPushButton("STOP");
    _stop_park->setToolTip("Emergency Stop parking");
    park_layout->addWidget(_stop_park, 2, 2);

    _az_park_spinbox = new QDoubleSpinBox;
    _az_park_spinbox->setRange(0, 360);
    _az_park_spinbox->setSingleStep(1);
    _az_park_spinbox->setValue(180);
    _az_park_spinbox->setToolTip("Between 0 and 360 degrees");
    park_layout->addWidget(_az_park_spinbox, 0, 1);

    _alt_park_spinbox = new QDoubleSpinBox;
    _alt_park_spinbox->setRange(0, 90);
    _alt_park_spinbox->setSingleStep(1);
    _alt_park_spinbox->setValue(0);
    _alt_park_spinbox->setToolTip("Between 0 and 90 degrees");
    park_layout->addWidget(_alt_park_spinbox, 1, 1);

    _AZ_park_label = new QLabel("AZ (°)");
    _AZ_park_label->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_AZ_park_label, 0, 0);
    _ALT_park_label = new QLabel("ALT (°)");
    _ALT_park_label->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_ALT_park_label, 1, 0);
    _park_groupbox->setLayout(park_layout);

    QObject::connect(_park_telescope, SIGNAL(clicked()),
                     this, SLOT(park_telescope_slot()));
    QObject::connect(_stop_park, SIGNAL(clicked()),
                     this, SLOT(stop_park_slot()));

    return _park_groupbox;
}

void View::park_telescope_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update park telescope payload";
    std::ostringstream ss_alt;
    ss_alt << _alt_park_spinbox->value();
    std::ostringstream ss_az;
    ss_az << _az_park_spinbox->value();

    telescope_park = "telescope_park," + to_string(2) +
            "," + ss_alt.str() + "," + ss_az.str();

    _signal_mapper_mqtt->setMapping(_park_telescope,
            telescope_park.c_str());
}

void View::stop_park_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Park telescope stop button clicked";
    QPalette pal = _stop_park->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_park->setAutoFillBackground(true);
    _stop_park->setPalette(pal);
    _stop_park->update();

    _string_logging = _string_logging + "STOP park button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::ra_dec_commands()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up RA/Dec command group box";

    QGroupBox *_ra_dec_groupbox = new QGroupBox("RA/Dec Commands");
    QVBoxLayout *ra_dec_layout = new QVBoxLayout;

    QGroupBox *_ra_groupbox = new QGroupBox("RA");
    QHBoxLayout *ra_layout = new QHBoxLayout;

    _degree_ra = new QDoubleSpinBox;
    _degree_ra->setRange(0, 360);
    _degree_ra->setSingleStep(1);
    _degree_ra->setValue(0);
    _degree_ra->setToolTip("Between 0 and 360 H");
    ra_layout->addWidget(_degree_ra);
    _degree_ra_label = new QLabel("H");
    ra_layout->addWidget(_degree_ra_label);

    _minute_ra = new QDoubleSpinBox;
    _minute_ra->setRange(0, 60);
    _minute_ra->setSingleStep(1);
    _minute_ra->setValue(0);
    _minute_ra->setToolTip("Between 0 and 60 M");
    ra_layout->addWidget(_minute_ra);
    _minute_ra_label = new QLabel("M");
    ra_layout->addWidget(_minute_ra_label);

    _second_ra = new QDoubleSpinBox;
    _second_ra->setRange(0, 60);
    _second_ra->setSingleStep(1);
    _second_ra->setValue(0);
    _second_ra->setToolTip("Between 0 and 60 S");
    ra_layout->addWidget(_second_ra);
    _second_ra_label = new QLabel("S");
    ra_layout->addWidget(_second_ra_label);
    _ra_groupbox->setLayout(ra_layout);

    QGroupBox *_dec_groupbox = new QGroupBox("Dec");
    QHBoxLayout *dec_layout = new QHBoxLayout;

    _degree_dec = new QDoubleSpinBox;
    _degree_dec->setRange(0, 90);
    _degree_dec->setSingleStep(1);
    _degree_dec->setValue(0);
    _degree_dec->setToolTip("Between 0 and 90 degrees");
    dec_layout->addWidget(_degree_dec);
    _degree_dec_label = new QLabel("°");
    dec_layout->addWidget(_degree_dec_label);

    _minute_dec = new QDoubleSpinBox;
    _minute_dec->setRange(0, 60);
    _minute_dec->setSingleStep(1);
    _minute_dec->setValue(0);
    _minute_dec->setToolTip("Between 0 and 60 minutes");
    dec_layout->addWidget(_minute_dec);
    _minute_dec_label = new QLabel("'");
    dec_layout->addWidget(_minute_dec_label);

    _second_dec = new QDoubleSpinBox;
    _second_dec->setRange(0, 60);
    _second_dec->setSingleStep(1);
    _second_dec->setValue(0);
    _second_dec->setToolTip("Between 0 and 60 seconds");
    dec_layout->addWidget(_second_dec);
    _second_dec_label = new QLabel("\"");
    dec_layout->addWidget(_second_dec_label);
    _dec_groupbox->setLayout(dec_layout);

    QGroupBox *_command_groupbox = new QGroupBox("");
    QHBoxLayout *command_layout = new QHBoxLayout;

    _equinox_combobox = new QComboBox();
    _equinox_combobox->setCurrentText("J2000");
    _equinox_combobox->addItem("J2000");
    command_layout->addWidget(_equinox_combobox);
    _goto_rad_dec = new QPushButton("GOTO");
    command_layout->addWidget(_goto_rad_dec);
    _sync_ra_dec = new QPushButton("SYNC");
    command_layout->addWidget(_sync_ra_dec);
    _stop_ra_dec = new QPushButton("STOP");
    _stop_ra_dec->setToolTip("Emergency Stop RA/Dec movement");
    command_layout->addWidget(_stop_ra_dec);
    _command_groupbox->setLayout(command_layout);

    ra_dec_layout->addWidget(_ra_groupbox);
    ra_dec_layout->addWidget(_dec_groupbox);
    ra_dec_layout->addWidget(_command_groupbox);
    _ra_dec_groupbox->setLayout(ra_dec_layout);

    QObject::connect(_goto_rad_dec, SIGNAL(clicked()),
                     this, SLOT(goto_ra_dec_slot()));
    QObject::connect(_sync_ra_dec, SIGNAL(clicked()),
                     this, SLOT(sync_ra_dec_slot()));
    QObject::connect(_stop_ra_dec, SIGNAL(clicked()),
                     this, SLOT(stop_ra_dec_slot()));

    return _ra_dec_groupbox;
}

void View::goto_ra_dec_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update goto RA/DEC payload";
    string equinox = _equinox_combobox->currentText().toStdString();
    int val_equinox;
    if (equinox.compare("J2000") == 0)
        val_equinox = 0;

    std::ostringstream ss_ra_degree;
    ss_ra_degree << _degree_ra->value();
    std::ostringstream ss_ra_minute;
    ss_ra_minute << _minute_ra->value();
    std::ostringstream ss_ra_second;
    ss_ra_second << _second_ra->value();
    std::ostringstream ss_dec_degree;
    ss_dec_degree << _degree_dec->value();
    std::ostringstream ss_dec_minute;
    ss_dec_minute << _minute_dec->value();
    std::ostringstream ss_dec_second;
    ss_dec_second << _second_dec->value();

    telescope_radec_goto = "telescope_radec_goto," + to_string(7) +
            "," + to_string(val_equinox) +
            "," + ss_ra_degree.str() + "," + ss_ra_minute.str() +
            "," + ss_ra_second.str() + "," + ss_dec_degree.str() +
            "," + ss_dec_minute.str() + "," + ss_dec_second.str();

    _signal_mapper_mqtt->setMapping(_goto_rad_dec,
            telescope_radec_goto.c_str());
}

void View::sync_ra_dec_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update sync RA/DEC payload";
    string equinox = _equinox_combobox->currentText().toStdString();
    int val_equinox;
    if (equinox.compare("J2000") == 0)
        val_equinox = 0;

    std::ostringstream ss_ra_degree;
    ss_ra_degree << _degree_ra->value();
    std::ostringstream ss_ra_minute;
    ss_ra_minute << _minute_ra->value();
    std::ostringstream ss_ra_second;
    ss_ra_second << _second_ra->value();
    std::ostringstream ss_dec_degree;
    ss_dec_degree << _degree_dec->value();
    std::ostringstream ss_dec_minute;
    ss_dec_minute << _minute_dec->value();
    std::ostringstream ss_dec_second;
    ss_dec_second << _second_dec->value();

    telescope_radec_sync = "telescope_radec_sync," + to_string(7) +
            "," + to_string(val_equinox) +
            "," + ss_ra_degree.str() + "," + ss_ra_minute.str() +
            "," + ss_ra_second.str() + "," + ss_dec_degree.str() +
            "," + ss_dec_minute.str() + "," + ss_dec_second.str();

    _signal_mapper_mqtt->setMapping(_sync_ra_dec,
            telescope_radec_sync.c_str());
}

void View::stop_ra_dec_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Ra/Dec telescope stop button clicked";
    QPalette pal = _stop_ra_dec->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_ra_dec->setAutoFillBackground(true);
    _stop_ra_dec->setPalette(pal);
    _stop_ra_dec->update();

    _string_logging = _string_logging + "STOP Ra/Dec button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

QGroupBox* View::alt_az_commands()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up ALT/AZ command group box";

    QGroupBox *_alt_az_groupbox = new QGroupBox("ALT/AZ Commands");
    QVBoxLayout *alt_az_layout = new QVBoxLayout;

    QGroupBox *_alt_groupbox = new QGroupBox("ALT");
    QHBoxLayout *alt_layout = new QHBoxLayout;

    _degree_alt = new QDoubleSpinBox;
    _degree_alt->setRange(0, 90);
    _degree_alt->setSingleStep(1);
    _degree_alt->setValue(0);
    _degree_alt->setToolTip("Between 0 and 90 degrees");
    alt_layout->addWidget(_degree_alt);
    _degree_alt_label = new QLabel("°");
    alt_layout->addWidget(_degree_alt_label);

    _minute_alt = new QDoubleSpinBox;
    _minute_alt->setRange(0, 60);
    _minute_alt->setSingleStep(1);
    _minute_alt->setValue(0);
    _minute_alt->setToolTip("Between 0 and 60 minutes");
    alt_layout->addWidget(_minute_alt);
    _minute_alt_label = new QLabel("'");
    alt_layout->addWidget(_minute_alt_label);

    _second_alt = new QDoubleSpinBox;
    _second_alt->setRange(0, 60);
    _second_alt->setSingleStep(1);
    _second_alt->setValue(0);
    _second_alt->setToolTip("Between 0 and 60 seconds");
    alt_layout->addWidget(_second_alt);
    _second_alt_label = new QLabel("\"");
    alt_layout->addWidget(_second_alt_label);
    _alt_groupbox->setLayout(alt_layout);

    QGroupBox *_az_groupbox = new QGroupBox("AZ");
    QHBoxLayout *az_layout = new QHBoxLayout;

    _degree_az = new QDoubleSpinBox;
    _degree_az->setRange(0, 360);
    _degree_az->setSingleStep(1);
    _degree_az->setValue(0);
    _degree_az->setToolTip("Between 0 and 360 degrees");
    az_layout->addWidget(_degree_az);
    _degree_az_label = new QLabel("°");
    az_layout->addWidget(_degree_az_label);

    _minute_az = new QDoubleSpinBox;
    _minute_az->setRange(0, 60);
    _minute_az->setSingleStep(1);
    _minute_az->setValue(0);
    _minute_az->setToolTip("Between 0 and 60 minutes");
    az_layout->addWidget(_minute_az);
    _minute_az_label = new QLabel("'");
    az_layout->addWidget(_minute_az_label);

    _second_az = new QDoubleSpinBox;
    _second_az->setRange(0, 60);
    _second_az->setSingleStep(1);
    _second_az->setValue(0);
    _second_az->setToolTip("Between 0 and 60 seconds");
    az_layout->addWidget(_second_az);
    _second_az_label = new QLabel("\"");
    az_layout->addWidget(_second_az_label);
    _az_groupbox->setLayout(az_layout);

    QGroupBox *_command_groupbox = new QGroupBox("");
    QHBoxLayout *command_layout = new QHBoxLayout;

    _goto_alt_az = new QPushButton("GOTO");
    command_layout->addWidget(_goto_alt_az);
    _sync_alt_az = new QPushButton("SYNC");
    command_layout->addWidget(_sync_alt_az);
    _stop_alt_az = new QPushButton("STOP");
    _stop_alt_az->setToolTip("Emergency Stop ALT/AZ movement");
    command_layout->addWidget(_stop_alt_az);
    _command_groupbox->setLayout(command_layout);

    alt_az_layout->addWidget(_alt_groupbox);
    alt_az_layout->addWidget(_az_groupbox);
    alt_az_layout->addWidget(_command_groupbox);
    _alt_az_groupbox->setLayout(alt_az_layout);

    QObject::connect(_goto_alt_az, SIGNAL(clicked()),
                     this, SLOT(goto_alt_az_slot()));
    QObject::connect(_sync_alt_az, SIGNAL(clicked()),
                     this, SLOT(sync_alt_az_slot()));
    QObject::connect(_stop_alt_az, SIGNAL(clicked()),
                     this, SLOT(stop_alt_az_slot()));

    return _alt_az_groupbox;
}

void View::goto_alt_az_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update goto ALT/AZ payload";

    std::ostringstream ss_alt_degree;
    ss_alt_degree << _degree_alt->value();
    std::ostringstream ss_alt_minute;
    ss_alt_minute << _minute_alt->value();
    std::ostringstream ss_alt_second;
    ss_alt_second << _second_alt->value();
    std::ostringstream ss_az_degree;
    ss_az_degree << _degree_az->value();
    std::ostringstream ss_az_minute;
    ss_az_minute << _minute_az->value();
    std::ostringstream ss_az_second;
    ss_az_second << _second_az->value();

    telescope_altaz_goto = "telescope_altaz_goto," + to_string(6) +
            "," + ss_alt_degree.str() + "," + ss_alt_minute.str() +
            "," + ss_alt_second.str() + "," + ss_az_degree.str() +
            "," + ss_az_minute.str() + "," + ss_az_second.str();

    _signal_mapper_mqtt->setMapping(_goto_alt_az,
            telescope_altaz_goto.c_str());

    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update position of the telescope on the map";
    float alt = ((POSITION_PIXEL_XW-POSITION_PIXEL_X0)
                 *(90-_degree_alt->value()))/90;
    float rad_az = (_degree_az->value() * 2 * PI)/360;
    int xc = POSITION_PIXEL_X0 + alt * sin(rad_az);
    int yc = POSITION_PIXEL_Y0 - alt * cos(rad_az);
    update_position_telescope(xc, yc);
}

void View::sync_alt_az_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update sync ALT/AZ payload";

    std::ostringstream ss_alt_degree;
    ss_alt_degree << _degree_alt->value();
    std::ostringstream ss_alt_minute;
    ss_alt_minute << _minute_alt->value();
    std::ostringstream ss_alt_second;
    ss_alt_second << _second_alt->value();
    std::ostringstream ss_az_degree;
    ss_az_degree << _degree_az->value();
    std::ostringstream ss_az_minute;
    ss_az_minute << _minute_az->value();
    std::ostringstream ss_az_second;
    ss_az_second << _second_az->value();

    telescope_altaz_sync = "telescope_altaz_sync," + to_string(6) +
            "," + ss_alt_degree.str() + "," + ss_alt_minute.str() +
            "," + ss_alt_second.str() + "," + ss_az_degree.str() +
            "," + ss_az_minute.str() + "," + ss_az_second.str();

    _signal_mapper_mqtt->setMapping(_sync_alt_az,
            telescope_altaz_sync.c_str());
}

void View::stop_alt_az_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "ALT/AZ telescope stop button clicked";
    QPalette pal = _stop_alt_az->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_alt_az->setAutoFillBackground(true);
    _stop_alt_az->setPalette(pal);
    _stop_alt_az->update();

    _string_logging = _string_logging + "STOP ALT/AZ button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());
}

pair<QGroupBox*,int> View::config_admin()
{
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
        "Set up config/graph group box in function of user level access";

    QGroupBox *_config_groupbox = new QGroupBox("Config");
    QHBoxLayout *config_layout = new QHBoxLayout;

    _config_velocity = new QPushButton("Config commands");
    config_layout->addWidget(_config_velocity);
    _config_mqtt = new QPushButton("Config MQTT server");
    config_layout->addWidget(_config_mqtt);
    _show_graph = new QPushButton("Show graph");
    config_layout->addWidget(_show_graph);
    _config_groupbox->setLayout(config_layout);

    QObject::connect(_config_velocity, SIGNAL(clicked()),
                     this, SLOT(config_command_window()));
    QObject::connect(_show_graph, SIGNAL(clicked()),
                     this, SLOT(show_graph()));

    return make_pair(_config_groupbox, USER_LEVEL);
}

void View::telescope_buttons()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add widgets into the telescope layout";
    _telescope_layout = new QGridLayout();
    _telescope_layout->setRowMinimumHeight(2, 350);
    _telescope_layout->setColumnMinimumWidth(1, 300);

    _telescope_layout->addWidget(connection_telescope(), 0, 0);
    _telescope_layout->addWidget(hand_control(), 2, 0);
    _telescope_layout->addWidget(tracking(), 1, 1);
    _telescope_layout->addWidget(date_time(), 0, 1);
    _telescope_layout->addWidget(controller_infos(), 2, 2);
    _telescope_layout->addWidget(show_loggging(), 2, 1);
    _telescope_layout->addWidget(home_telescope(), 1, 0);
    _telescope_layout->addWidget(park_telescope(), 3, 2);
    _telescope_layout->addWidget(satellite_traj(), 0, 2);
    _telescope_layout->addWidget(ra_dec_commands(), 3, 0);
    _telescope_layout->addWidget(alt_az_commands(), 3, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Verify user level to display config telescope groupbox";
    if(config_admin().second <= 0)
        _telescope_layout->addWidget(config_admin().first, 1, 2);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Insert telescope layout into the map with associated level";
    _map_widgets.insert(make_pair(_telescope_layout, USER_LEVEL));
}

void View::verify_user_level()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the telescope layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it=_map_widgets.begin();
        it != _map_widgets.end(); ++it)
    {
        if(it->second <= 0)
            _telescope_tab->setLayout(it->first);
    }
}

void View::show_time()
{
    QString text = "UTC Time: " +
            QDateTime::currentDateTimeUtc().toString("h:m:s ap");
    _dateTimeUTC->setText(text);
    text = "Local time: " + QDateTime::currentDateTime().toString("h:m:s ap");
    _dateTime_local->setText(text);
    text = "Date: " + QDateTime::currentDateTime().toString("dddd d MMMM yyyy");
    _date->setText(text);

    if (_string_log_weather.length() > STRING_LOG_LENGTH)
        _string_log_weather = "";

    if (_string_logging_dome.length() > STRING_LOG_LENGTH)
        _string_logging_dome = "";

    if (_string_logging.length() > STRING_LOG_LENGTH)
        _string_logging = "";

    if (_string_logging_actuator.length() > STRING_LOG_LENGTH)
        _string_logging_actuator = "";

    if (_string_logging_actuator_M4_M5.length() > STRING_LOG_LENGTH)
        _string_logging_actuator_M4_M5 = "";

    if (_string_logging_camera.length() > STRING_LOG_LENGTH)
        _string_logging_camera = "";
}

/////////////////////////CONFIG TELESCOPE FUNCTIONS////////////////////

void View::create_new_window()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create new window using 'new' button";
    _new_window = new QWidget();
    _new_window->setFixedSize(800, 600);
    _new_window->show();
}

QGroupBox* View::alt_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up ALT limits group box";

    QGroupBox *_alt_limits_groupbox = new QGroupBox("ALT Limits");
    QGridLayout *alt_layout = new QGridLayout;

    _limit_alt_up = new QLabel("Limit ALT up (°)");
    alt_layout->addWidget(_limit_alt_up, 0, 0);
    _alt_up_spinbox = new QSpinBox;
    _alt_up_spinbox->setRange(0, 90);
    _alt_up_spinbox->setSingleStep(1);
    _alt_up_spinbox->setValue(90);
    _alt_up_spinbox->setToolTip("Between 0 and 90 degrees");
    alt_layout->addWidget(_alt_up_spinbox, 0, 1);

    _limit_alt_down = new QLabel("Limit ALT down (°)");
    alt_layout->addWidget(_limit_alt_down, 1, 0);
    _alt_down_spinbox = new QSpinBox;
    _alt_down_spinbox->setRange(0, 90);
    _alt_down_spinbox->setSingleStep(1);
    _alt_down_spinbox->setValue(0);
    _alt_down_spinbox->setToolTip("Between 0 and 90 degrees");
    alt_layout->addWidget(_alt_down_spinbox, 1, 1);

    _set_limit_alt = new QPushButton("SET");
    alt_layout->addWidget(_set_limit_alt, 0, 2);
    _reset_limit_alt = new QPushButton("RESET");
    alt_layout->addWidget(_reset_limit_alt, 1, 2);
    _alt_limits_groupbox->setLayout(alt_layout);

    QObject::connect(_set_limit_alt, SIGNAL(clicked()),
        this, SLOT(update_limit_alt()));
    QObject::connect(_reset_limit_alt, SIGNAL(clicked()),
        this, SLOT(reset_limit_alt()));

    return _alt_limits_groupbox;
}

int View::write_file_config()
{
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_telescope.txt");
    ofstream file;

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Open and delete the file's telescope content";
    file.open(str1, ios::out | ios::trunc);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "File telescope not found";
        return -1;
    }

    config_telescope = "config_telescope;15;";
    for(map<int,string>::const_iterator it =_list_config_value.begin();
        it != _list_config_value.end(); ++it)
    {
        file << it->second << "\n";
        config_telescope = config_telescope + it->second + ";";
    }
    config_telescope.pop_back();

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Telescope serialized config file: "
                                      << config_telescope;
    _string_logging = _string_logging + "Serialized config file: \"" +
            QString::fromStdString(config_telescope) + "\"\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close telescope file";
    return 0;
}

void View::update_limit_alt()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update ALT limits in config window";
    _list_config_value.find(0)->second = to_string(_alt_up_spinbox->value());
    _list_config_value.find(1)->second = to_string(_alt_down_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_limit_alt()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset ALT limits in config window";
    _list_config_value.find(0)->second = to_string(90);
    _list_config_value.find(1)->second = to_string(0);
    _alt_up_spinbox->setValue(90);
    _alt_down_spinbox->setValue(0);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::az_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up AZ limits group box";
    QGroupBox *_az_limits_groupbox = new QGroupBox("AZ Limits");
    QGridLayout *az_layout = new QGridLayout;

    _limit_az_up = new QLabel("Limit AZ up (°)");
    az_layout->addWidget(_limit_az_up, 0, 0);
    _az_up_spinbox = new QSpinBox;
    _az_up_spinbox->setRange(0, 360);
    _az_up_spinbox->setSingleStep(1);
    _az_up_spinbox->setValue(360);
    _az_up_spinbox->setToolTip("Between 0 and 180 degrees");
    az_layout->addWidget(_az_up_spinbox, 0, 1);

    _limit_az_down = new QLabel("Limit AZ down (°)");
    az_layout->addWidget(_limit_az_down, 1, 0);
    _az_down_spinbox = new QSpinBox;
    _az_down_spinbox->setRange(0, 360);
    _az_down_spinbox->setSingleStep(1);
    _az_down_spinbox->setValue(0);
    _az_down_spinbox->setToolTip("Between 0 and 360 degrees");
    az_layout->addWidget(_az_down_spinbox, 1, 1);

    _set_limit_az = new QPushButton("SET");
    az_layout->addWidget(_set_limit_az, 0, 2);
    _reset_limit_az = new QPushButton("RESET");
    az_layout->addWidget(_reset_limit_az, 1, 2);
    _az_limits_groupbox->setLayout(az_layout);

    QObject::connect(_set_limit_az, SIGNAL(clicked()),
        this, SLOT(update_limit_az()));
    QObject::connect(_reset_limit_az, SIGNAL(clicked()),
        this, SLOT(reset_limit_az()));

    return _az_limits_groupbox;
}

void View::update_limit_az()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update AZ limits in config window";
    _list_config_value.find(2)->second = to_string(_az_up_spinbox->value());
    _list_config_value.find(3)->second = to_string(_az_down_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_limit_az()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset AZ limits in config window";
    _list_config_value.find(2)->second = to_string(360);
    _list_config_value.find(3)->second = to_string(0);
    _az_up_spinbox->setValue(360);
    _az_down_spinbox->setValue(0);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::current_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up current limits group box";
    QGroupBox *_current_limits_groupbox = new QGroupBox("Current Limits");
    QGridLayout *current_layout = new QGridLayout;

    _limit_az_current = new QLabel("Limit AZ electric\npower (A)");
    current_layout->addWidget(_limit_az_current, 0, 0);
    _az_current_spinbox = new QSpinBox;
    _az_current_spinbox->setRange(0, 10);
    _az_current_spinbox->setSingleStep(1);
    _az_current_spinbox->setValue(1);
    _az_current_spinbox->setToolTip("Between 0 and 10 A");
    current_layout->addWidget(_az_current_spinbox, 0, 1);

    _limit_lat_current = new QLabel("Limit LAT electric\npower (A)");
    current_layout->addWidget(_limit_lat_current, 1, 0);
    _lat_currrent_spinbox = new QSpinBox;
    _lat_currrent_spinbox->setRange(0, 10);
    _lat_currrent_spinbox->setSingleStep(1);
    _lat_currrent_spinbox->setValue(1);
    _lat_currrent_spinbox->setToolTip("Between 0 and 10 A");
    current_layout->addWidget(_lat_currrent_spinbox, 1, 1);

    _set_limit_current = new QPushButton("SET");
    current_layout->addWidget(_set_limit_current, 0, 2);
    _reset_limit_current = new QPushButton("RESET");
    current_layout->addWidget(_reset_limit_current, 1, 2);
    _current_limits_groupbox->setLayout(current_layout);

    QObject::connect(_set_limit_current, SIGNAL(clicked()),
        this, SLOT(update_current()));
    QObject::connect(_reset_limit_current, SIGNAL(clicked()),
        this, SLOT(reset_current()));

    return _current_limits_groupbox;
}

void View::update_current()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update current limits in config window";
    _list_config_value.find(4)->second =
            to_string(_az_current_spinbox->value());
    _list_config_value.find(5)->second =
            to_string(_lat_currrent_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_current()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset current limits in config window";
    _list_config_value.find(4)->second = to_string(1);
    _list_config_value.find(5)->second = to_string(1);
    _az_current_spinbox->setValue(1);
    _lat_currrent_spinbox->setValue(1);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::speed_limits()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up speed limits group box";
    QGroupBox *_speed_limits_groupbox = new QGroupBox("Speed Limits");
    QGridLayout *speed_layout = new QGridLayout;

    _limit_velocity = new QLabel("Limit MAX Velocity (°/s)");
    speed_layout->addWidget(_limit_velocity, 0, 0);
    _velocity_spinbox = new QSpinBox;
    _velocity_spinbox->setRange(0, 10);
    _velocity_spinbox->setSingleStep(1);
    _velocity_spinbox->setValue(3);
    _velocity_spinbox->setToolTip("Between 0 and 10 °/s");
    speed_layout->addWidget(_velocity_spinbox, 0, 1);

    _limit_acceleration = new QLabel("Limit MAX Acceleration (°/s²)");
    speed_layout->addWidget(_limit_acceleration, 1, 0);
    _acceleration_spinbox = new QSpinBox;
    _acceleration_spinbox->setRange(0, 10);
    _acceleration_spinbox->setSingleStep(1);
    _acceleration_spinbox->setValue(3);
    _acceleration_spinbox->setToolTip("Between 0 and 10 °/s²");
    speed_layout->addWidget(_acceleration_spinbox, 1, 1);

    _set_limit_speed = new QPushButton("SET");
    speed_layout->addWidget(_set_limit_speed, 0, 2);
    _reset_limit_speed = new QPushButton("RESET");
    speed_layout->addWidget(_reset_limit_speed, 1, 2);
    _speed_limits_groupbox->setLayout(speed_layout);

    QObject::connect(_set_limit_speed, SIGNAL(clicked()),
        this, SLOT(update_speed_limit()));
    QObject::connect(_reset_limit_speed, SIGNAL(clicked()),
        this, SLOT(reset_speed_limit()));

    return _speed_limits_groupbox;
}

void View::update_speed_limit()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update speed limits in config window";
    _list_config_value.find(6)->second =
            to_string(_velocity_spinbox->value());
    _list_config_value.find(7)->second =
            to_string(_acceleration_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_speed_limit()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset speed limits in config window";
    _list_config_value.find(6)->second = to_string(3);
    _list_config_value.find(7)->second = to_string(3);
    _velocity_spinbox->setValue(3);
    _acceleration_spinbox->setValue(3);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::velocity_config()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up velocity group box";
    QGroupBox *_velocity_groupbox = new QGroupBox("Set Velocity");
    QGridLayout *velocity_layout = new QGridLayout;

    _velocity_homing = new QLabel("Velocity for home/park\nmovement (°/s)");
    velocity_layout->addWidget(_velocity_homing, 0, 0);
    _velocity_home_spinbox = new QSpinBox;
    _velocity_home_spinbox->setRange(0, 10);
    _velocity_home_spinbox->setSingleStep(1);
    _velocity_home_spinbox->setValue(3);
    _velocity_home_spinbox->setToolTip("Between 0 and 10 °/s");
    velocity_layout->addWidget(_velocity_home_spinbox, 0, 1);

    _velocity_trajectory = new QLabel("Velocity for\ntrajectory (°/s)");
    velocity_layout->addWidget(_velocity_trajectory, 1, 0);
    _velocity_traj_spinbox = new QSpinBox;
    _velocity_traj_spinbox->setRange(0, 10);
    _velocity_traj_spinbox->setSingleStep(1);
    _velocity_traj_spinbox->setValue(3);
    _velocity_traj_spinbox->setToolTip("Between 0 and 10 °/s");
    velocity_layout->addWidget(_velocity_traj_spinbox, 1, 1);

    _set_velocity = new QPushButton("SET");
    velocity_layout->addWidget(_set_velocity, 0, 2);
    _reset_velocity = new QPushButton("RESET");
    velocity_layout->addWidget(_reset_velocity, 1, 2);
    _velocity_groupbox->setLayout(velocity_layout);

    QObject::connect(_set_velocity, SIGNAL(clicked()),
        this, SLOT(update_velocity()));
    QObject::connect(_reset_velocity, SIGNAL(clicked()),
        this, SLOT(reset_velocity()));

    return _velocity_groupbox;
}

void View::update_velocity()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update velocity in config window";
    _list_config_value.find(8)->second =
            to_string(_velocity_home_spinbox->value());
    _list_config_value.find(9)->second =
            to_string(_velocity_traj_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_velocity()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset velocity in config window";
    _list_config_value.find(8)->second = to_string(3);
    _list_config_value.find(9)->second = to_string(3);
    _velocity_home_spinbox->setValue(3);
    _velocity_traj_spinbox->setValue(3);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::acceleration_config()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up acceleration group box";
    QGroupBox *_accel_groupbox = new QGroupBox("Set Acceleration");
    QGridLayout *accel_layout = new QGridLayout;

    _accel_homing = new QLabel("Acceleration for\nhome/park movement (°/s²)");
    accel_layout->addWidget(_accel_homing, 0, 0);
    _accel_home_spinbox = new QSpinBox;
    _accel_home_spinbox->setRange(0, 10);
    _accel_home_spinbox->setSingleStep(1);
    _accel_home_spinbox->setValue(3);
    _accel_home_spinbox->setToolTip("Between 0 and 10 °/s²");
    accel_layout->addWidget(_accel_home_spinbox, 0, 1);

    _accel_trajectory = new QLabel("Acceleration for\ntrajectory (°/s²)");
    accel_layout->addWidget(_accel_trajectory, 1, 0);
    _accel_traj_spinbox = new QSpinBox;
    _accel_traj_spinbox->setRange(0, 10);
    _accel_traj_spinbox->setSingleStep(1);
    _accel_traj_spinbox->setValue(3);
    _accel_traj_spinbox->setToolTip("Between 0 and 10 °/s²");
    accel_layout->addWidget(_accel_traj_spinbox, 1, 1);

    _set_accel = new QPushButton("SET");
    accel_layout->addWidget(_set_accel, 0, 2);
    _reset_accel = new QPushButton("RESET");
    accel_layout->addWidget(_reset_accel, 1, 2);
    _accel_groupbox->setLayout(accel_layout);

    QObject::connect(_set_accel, SIGNAL(clicked()),
        this, SLOT(update_acceleration()));
    QObject::connect(_reset_accel, SIGNAL(clicked()),
        this, SLOT(reset_acceleration()));

    return _accel_groupbox;
}

void View::update_acceleration()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update acceleration in config window";
    _list_config_value.find(10)->second =
            to_string(_accel_home_spinbox->value());
    _list_config_value.find(11)->second =
            to_string(_accel_traj_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_acceleration()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset acceleration in config window";
    _list_config_value.find(10)->second = to_string(3);
    _list_config_value.find(11)->second = to_string(3);
    _accel_home_spinbox->setValue(3);
    _accel_traj_spinbox->setValue(3);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::position_config()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up geographical position group box";
    QGroupBox *_position_groupbox = new QGroupBox("Geographical Position");
    QGridLayout *position_layout = new QGridLayout;

    _latitude = new QLabel("Latitude");
    position_layout->addWidget(_latitude, 0, 0);
    _latitude_spinbox = new QDoubleSpinBox;
    _latitude_spinbox->setRange(-90, 90);
    _latitude_spinbox->setSingleStep(1);
    _latitude_spinbox->setValue(43.75);
    _latitude_spinbox->setToolTip("Between -90 and 90°");
    position_layout->addWidget(_latitude_spinbox, 0, 1);

    _longitude = new QLabel("Longitude");
    position_layout->addWidget(_longitude, 1, 0);
    _longitude_spinbox = new QDoubleSpinBox;
    _longitude_spinbox->setRange(-180, 180);
    _longitude_spinbox->setSingleStep(1);
    _longitude_spinbox->setValue(6.92);
    _longitude_spinbox->setToolTip("Between -180 and 180°");
    position_layout->addWidget(_longitude_spinbox, 1, 1);

    _altitude = new QLabel("Altitude");
    position_layout->addWidget(_altitude, 2, 0);
    _altitude_spinbox = new QSpinBox;
    _altitude_spinbox->setRange(0, 3000);
    _altitude_spinbox->setSingleStep(1);
    _altitude_spinbox->setValue(1119);
    _altitude_spinbox->setToolTip("Between 0 and 3000m");
    position_layout->addWidget(_altitude_spinbox, 2, 1);

    _set_position = new QPushButton("SET");
    position_layout->addWidget(_set_position, 0, 2);
    _reset_position = new QPushButton("RESET");
    position_layout->addWidget(_reset_position, 1, 2);
    _position_groupbox->setLayout(position_layout);

    QObject::connect(_set_position, SIGNAL(clicked()),
        this, SLOT(update_position()));
    QObject::connect(_reset_position, SIGNAL(clicked()),
        this, SLOT(reset_position()));

    return _position_groupbox;
}

void View::update_position()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update geographical position in config window";
    _list_config_value.find(12)->second =
            to_string(_latitude_spinbox->value());
    _list_config_value.find(13)->second =
            to_string(_longitude_spinbox->value());
    _list_config_value.find(14)->second =
            to_string(_altitude_spinbox->value());
    write_file_config();
    update_infos_config();
}

void View::reset_position()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
                "Reset geographical position in config window";
    _list_config_value.find(12)->second = to_string(43.75);
    _list_config_value.find(13)->second = to_string(6.92);
    _list_config_value.find(14)->second = to_string(1119);
    _latitude_spinbox->setValue(43.75);
    _longitude_spinbox->setValue(6.92);
    _altitude_spinbox->setValue(1119);
    write_file_config();
    update_infos_config();
}

QGroupBox* View::limits_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up limits infos group box";

    QGroupBox *_limits_groupbox = new QGroupBox("");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    _alt_up_info = new QLabel("Limit ALT up:");
    limits_layout->addWidget(_alt_up_info);
    _alt_down_info = new QLabel("Limit ALT down:");
    limits_layout->addWidget(_alt_down_info);
    _az_up_info = new QLabel("Limit AZ up:");
    limits_layout->addWidget(_az_up_info);
    _az_down_info = new QLabel("Limit AZ down:");
    limits_layout->addWidget(_az_down_info);

    _az_current_info = new QLabel("Limit AZ current:");
    limits_layout->addWidget(_az_current_info);
    _lat_current_info = new QLabel("Limit LAT current:");
    limits_layout->addWidget(_lat_current_info);
    _velocity_limit_info = new QLabel("Velocity limit:");
    limits_layout->addWidget(_velocity_limit_info);
    _acceleration_limit_info = new QLabel("Acceleration limit:");
    limits_layout->addWidget(_acceleration_limit_info);

    _velocity_home_info = new QLabel("Velocity home move:");
    limits_layout->addWidget(_velocity_home_info);
    _velocity_traj_info = new QLabel("Velocity traj move:");
    limits_layout->addWidget(_velocity_traj_info);
    _accel_home_info = new QLabel("Acceleration home:");
    limits_layout->addWidget(_accel_home_info);
    _accel_traj_info = new QLabel("Acceleration traj:");
    limits_layout->addWidget(_accel_traj_info);

    _latitude_info = new QLabel("Latitude:");
    limits_layout->addWidget(_latitude_info);
    _longitude_info = new QLabel("Longitude:");
    limits_layout->addWidget(_longitude_info);
    _altitude_info = new QLabel("Altitude:");
    limits_layout->addWidget(_altitude_info);

    _limits_groupbox->setLayout(limits_layout);
    return _limits_groupbox;
}

int View::read_file_config()
{
    string text;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Open the config telescope file";
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_telescope.txt");
    ifstream file(str1);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Telescope file not found";
        return -1;
    }

    int i = 0;
    config_telescope = "config_telescope;15;";
    while(getline(file, text))
    {
        _list_config_value.insert(make_pair(i, text));
        config_telescope = config_telescope + text + ";";
        i++;
    }

    if (i == 0) {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Telescope file is empty";
        return -1;
    }
    config_telescope.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Telescope serialized config file: "
                                      << config_telescope;
    _string_logging = _string_logging + "Serialized config file: \"" +
            QString::fromStdString(config_telescope) + "\"\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close telescope file";
    return i;
}

void View::update_infos_config()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
        "Update infos displayed in telescope config window";

    _alt_up_info->setText(QString::fromStdString(
        "Limit ALT up: " + _list_config_value.find(0)->second + "°"));
    _alt_down_info->setText(QString::fromStdString(
        "Limit ALT down: " + _list_config_value.find(1)->second + "°"));
    _az_up_info->setText(QString::fromStdString(
        "Limit AZ up: " + _list_config_value.find(2)->second + "°"));
    _az_down_info->setText(QString::fromStdString(
        "Limit AZ down: " + _list_config_value.find(3)->second + "°"));
    _az_current_info->setText(QString::fromStdString(
        "Limit AZ current: " + _list_config_value.find(4)->second + "A"));
    _lat_current_info->setText(QString::fromStdString(
        "Limit LAT current: " + _list_config_value.find(5)->second + "A"));
    _velocity_limit_info->setText(QString::fromStdString(
        "Velocity limit: " + _list_config_value.find(6)->second + "m/s"));
    _acceleration_limit_info->setText(QString::fromStdString(
        "Acceleration limit: " + _list_config_value.find(7)->second + "m/s²"));
    _velocity_home_info->setText(QString::fromStdString(
        "Velocity home move: " + _list_config_value.find(8)->second + "m/s"));
    _velocity_traj_info->setText(QString::fromStdString(
        "Velocity traj move: " + _list_config_value.find(9)->second + "m/s"));
    _accel_home_info->setText(QString::fromStdString(
        "Acceleration home: " + _list_config_value.find(10)->second + "m/s²"));
    _accel_traj_info->setText(QString::fromStdString(
        "Acceleration traj: " + _list_config_value.find(11)->second + "m/s²"));
    _latitude_info->setText(QString::fromStdString(
        "Latitude: " + _list_config_value.find(12)->second + "°"));
    _longitude_info->setText(QString::fromStdString(
        "Longitude: " + _list_config_value.find(13)->second + "°"));
    _altitude_info->setText(QString::fromStdString(
        "Altitude: " + _list_config_value.find(14)->second + "m"));
}

void View::config_command_window()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Create new window for telescope configurations. "
            << "Config button clicked";
    _string_logging = _string_logging + "config button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    _new_window = new QWidget();
    _new_window->setWindowTitle("COMMAND CONFIG");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    QGroupBox *_limits_groupbox = new QGroupBox("Limits");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    limits_layout->addWidget(alt_limits());
    limits_layout->addWidget(az_limits());
    limits_layout->addWidget(current_limits());
    limits_layout->addWidget(speed_limits());
    _limits_groupbox->setLayout(limits_layout);

    QGroupBox *_set_speed_groupbox = new QGroupBox("Configurations");
    QVBoxLayout *set_speed_layout = new QVBoxLayout;

    set_speed_layout->addWidget(velocity_config());
    set_speed_layout->addWidget(acceleration_config());
    set_speed_layout->addWidget(position_config());
    _set_speed_groupbox->setLayout(set_speed_layout);

    QGroupBox *_display_groupbox = new QGroupBox("Actual data");
    QVBoxLayout *display_layout = new QVBoxLayout;

    display_layout->addWidget(limits_infos());
    _display_groupbox->setLayout(display_layout);

    _config_layout = new QGridLayout();
    _config_layout->addWidget(_limits_groupbox, 0, 0);
    _config_layout->addWidget(_set_speed_groupbox, 0, 1);
    _config_layout->addWidget(_display_groupbox, 0, 2);

    _new_window->setLayout(_config_layout);
    read_file_config();
    update_infos_config();
    _new_window->show();
}

////////////////////////////////MQTT CONNECTION/////////////////////////

void View::mqtt_state_publish(const QString &payload)
{
    /*if(mqtt_connected == true)
    {
        string str = payload.toStdString();
        _mqttinter->publish_msg(str);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Payload: " << str;
        _string_logging = _string_logging + "\"" +
                QString::fromStdString(str) + "\" sent\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());

        _string_logging_dome = _string_logging_dome + "\"" +
                QString::fromStdString(str) + "\" sent\n";
        _logging_label_dome->setText(_string_logging_dome);
        _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                    _logging_label_dome->height());

        QPalette pal = _blinking_mqtt_pub->palette();
        pal.setColor(QPalette::Button, QColor(Qt::blue));
        _blinking_mqtt_pub->setAutoFillBackground(true);
        _blinking_mqtt_pub->setPalette(pal);
        _blinking_mqtt_pub->update();

        QObject::connect(_timer_blinking_pub, SIGNAL(timeout()),
                this, SLOT(blinking_slot_pub()));
        _timer_blinking_pub->start(100);
    }*/
}

void View::blinking_slot_pub()
{
    _timer_blinking_pub->stop();
    QPalette pal = _blinking_mqtt_pub->palette();
    pal.setColor(QPalette::Button, QColor(Qt::white));
    _blinking_mqtt_pub->setAutoFillBackground(true);
    _blinking_mqtt_pub->setPalette(pal);
    _blinking_mqtt_pub->update();
}

void View::mqtt_subscribe()
{
    /*if(_mqttinter->cb._msg != NULL)
    {
        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Message received: "
                                          << _mqttinter->get_message();
        _string_logging = _string_logging + "\"" +
                QString::fromStdString(_mqttinter->get_message()) +
                "\" received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());

        _string_logging_dome = _string_logging_dome + "\"" +
                QString::fromStdString(_mqttinter->get_message()) +
                "\" received\n";
        _logging_label_dome->setText(_string_logging_dome);
        _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                    _logging_label_dome->height());

        _mqttinter->cb._msg = NULL;
    }*/
}

void View::mqtt_connect_publish()
{
    /*PLOG_INFO_IF(LOGGING_INFO == 1) << "Call connect MQTT publish function";
    _string_logging = _string_logging + "MQTT connect button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    if(mqtt_connected == false)
    {
        _mqttinter = new MQTT_pubsub(server_address, id_client_sub,
                            id_client_pub, topic_gui_pub, topic_gui_sub);
        _mqttinter->start();
        _timer_pub_sub = new QTimer(this);
        QObject::connect(_timer_pub_sub, SIGNAL(timeout()), this,
                         SLOT(mqtt_subscribe()));
        _timer_pub_sub->start(1);

        mqtt_connected  = true;
        QPalette pal = _connect_mqtt->palette();
        pal.setColor(QPalette::Button, QColor(Qt::green));
        _connect_mqtt->setAutoFillBackground(true);
        _connect_mqtt->setPalette(pal);
        _connect_mqtt->update();
    }*/
}

void View::mqtt_disconnect_publish()
{
    /*PLOG_INFO_IF(LOGGING_INFO == 1) << "Call disconnect MQTT publish function";
    _string_logging = _string_logging + "MQTT disconnect button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    if(mqtt_connected == true)
    {
        _mqttinter->disconnect_all();
        mqtt_connected = false;
    }*/
}

void View::mqtt_subs_connect()
{
    /*PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT error subscribe";
    _sub_error = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_error, topic_sub_error);
    _sub_error->start();
    _timer_error = new QTimer(this);
    QObject::connect(_timer_error, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_error()));
    _timer_error->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT power subscribe";
    _sub_power = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_power, topic_sub_power);
    _sub_power->start();
    _timer_power = new QTimer(this);
    QObject::connect(_timer_power, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_power()));
    _timer_power->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT brake subscribe";
    _sub_brake = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_brake, topic_sub_brake);
    _sub_brake->start();
    _timer_brake = new QTimer(this);
    QObject::connect(_timer_brake, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_brake()));
    _timer_brake->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT Ra/Dec subscribe";
    _sub_radec = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_radec, topic_sub_radec);
    _sub_radec->start();
    _timer_radec = new QTimer(this);
    QObject::connect(_timer_radec, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_radec()));
    _timer_radec->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT ALT/AZ subscribe";
    _sub_alt_az = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_alt_az, topic_sub_alt_az);
    _sub_alt_az->start();
    _timer_alt_az = new QTimer(this);
    QObject::connect(_timer_alt_az, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_alt_az()));
    _timer_alt_az->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT error AZ/shutter subscribe";
    _sub_error_az = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_error_az, topic_sub_error_az);
    _sub_error_az->start();
    _timer_error_az = new QTimer(this);
    QObject::connect(_timer_error_az, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_error_az_shut()));
    _timer_error_az->start(1);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Start MQTT value AZ/shutter subscribe";
    _sub_value_az = new MQTT_subscriber<msg_position_telescope>(server_address,
                        client_sub_value_az, topic_sub_value_az);
    _sub_value_az->start();
    _timer_value_az = new QTimer(this);
    QObject::connect(_timer_value_az, SIGNAL(timeout()), this,
                     SLOT(mqtt_subs_value_az_shut()));
    _timer_value_az->start(1);*/
}

void View::update_error_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update ALT/AZ errors displayed";
    float alt = msg._pos.alt;
    float az = msg._pos.az;

    _error_AZ_label->setText("Error AZ: " + QString::number(az) + " step " +
        QString::number(az) + " arc/s");
    _error_ALT_label->setText("Error ALT: " + QString::number(alt) + " step " +
        QString::number(alt) + " arc/s");
}

void View::update_power_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update ALT/AZ power displayed";
    float alt = msg._pos.alt;
    float az = msg._pos.az;

    _power_AZ_label->setText("Power AZ: " + QString::number(az) + " W");
    _power_ALT_label->setText("Power ALT: " + QString::number(alt) + " W");
}

void View::update_brake_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update AZ/Dec brake displayed";
    float alt = msg._pos.alt;
    float az = msg._pos.az;

    QString dec;
    if(alt == 0)
        dec = "False";
    else
        dec = "True";

    QString az_str;
    if(az == 0)
        az_str = "False";
    else
        az_str = "True";

    _az_brake_label->setText("AZ Brake: " + az_str);
    _dec_brake_label->setText("Dec Brake: " + dec);
}

void View::update_ra_dec_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update Ra/Dec values displayed";
    float ra = msg._pos.alt;
    float dec = msg._pos.az;

    _ra_label->setText("RA value: " + QString::number(ra) + "° " +
        QString::number(ra) + "' " + QString::number(ra) + "\"");
    _dec_label->setText("Dec value: " + QString::number(dec) + "° " +
        QString::number(dec) + "' " + QString::number(dec) + "\"");
}

void View::update_alt_az_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update ALT/AZ values displayed";
    float alt = msg._pos.alt;
    float az = msg._pos.az;

    _AZ_display_label->setText("AZ value: " + QString::number(az) + "° " +
        QString::number(az) + "' " + QString::number(az) + "\"");
    _ALT_display_label->setText("ALT value: " + QString::number(alt) +
        "° " + QString::number(alt) + "' " + QString::number(alt) + "\"");
}

void View::update_error_az_shut_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update error AZ/shutter displayed";
    float shut = msg._pos.alt;
    float az = msg._pos.az;

    _error_AZ_dome->setText("Error AZ: " + QString::number(az) + " step " +
        QString::number(az) + " arc/s");
    _error_AZ_dome->setText("Error shutter: " + QString::number(shut) +
        " step " + QString::number(shut) + " arc/s");
}

void View::update_value_az_shut_display(msg_position_telescope msg)
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update value AZ/shutter displayed";
    float shut = msg._pos.alt;
    float az = msg._pos.az;

    _AZ_display_dome->setText("AZ value: " + QString::number(az) + "° " +
        QString::number(az) + "'");
    _shutter_display_dome->setText("Shutter value: " +
        QString::number(shut) + "%");
}

void View::mqtt_subs_error()
{
    /*if(_sub_error->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_error->get_message(msg);
        QString alt = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Error ALT: " << alt
                                          << " Error AZ: " << az;
        _string_logging = _string_logging + "Error alt: " + alt +
                " Error az: " + az + " received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());
        update_error_display(msg);
    }*/
}

void View::mqtt_subs_power()
{
    /*if(_sub_power->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_power->get_message(msg);
        QString alt = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Power ALT: " << alt
                                          << " Power AZ: " << az;
        _string_logging = _string_logging + "Power alt: " + alt +
                " Power az: " + az + " received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());
        update_power_display(msg);
    }*/
}

void View::mqtt_subs_brake()
{
    /*if(_sub_brake->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_brake->get_message(msg);
        QString alt = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Brake ALT: " << alt
                                          << " Brake AZ: " << az;
        _string_logging = _string_logging + "Brake alt: " + alt +
                " Brake az: " + az + " received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());
        update_brake_display(msg);
    }*/
}

void View::mqtt_subs_radec()
{
    /*if(_sub_radec->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_radec->get_message(msg);
        QString ra = QString::number(msg._pos.alt);
        QString dec = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Ra value: " << ra
                                          << " Dec value: " << dec;
        _string_logging = _string_logging + "Ra value: " + ra +
                " Dec value: " + dec + " received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());
        update_ra_dec_display(msg);
    }*/
}

void View::mqtt_subs_alt_az()
{
    /*if(_sub_alt_az->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_alt_az->get_message(msg);
        QString alt = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "ALT value: " << alt
                                          << " AZ value: " << az;
        _string_logging = _string_logging + "Alt value: " + alt +
                " Az value: " + az + " received\n";
        _logging_label->setText(_string_logging);
        _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                    _logging_label->height());
        update_alt_az_display(msg);
    }*/
}

void View::mqtt_subs_error_az_shut()
{
    /*if(_sub_error_az->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_error_az->get_message(msg);
        QString shut = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Shutter error: " << shut
                                          << " Shutter error: " << az;
        _string_logging_dome = _string_logging_dome + "Shutter error: " +
                shut + " Az error: " + az + " received\n";
        _logging_label_dome->setText(_string_logging_dome);
        _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                    _logging_label_dome->height());
        update_error_az_shut_display(msg);
    }*/
}

void View::mqtt_subs_value_az_shut()
{
    /*if(_sub_value_az->cb_sub._msg_arrived == 1)
    {
        msg_position_telescope msg;
        _sub_value_az->get_message(msg);
        QString shut = QString::number(msg._pos.alt);
        QString az = QString::number(msg._pos.az);

        PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Shutter value: " << shut
                                          << " Shutter value: " << az;
        _string_logging_dome = _string_logging_dome + "Shutter value: " +
                shut + " Az value: " + az + " received\n";
        _logging_label_dome->setText(_string_logging_dome);
        _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                    _logging_label_dome->height());
        update_value_az_shut_display(msg);
    }*/
}

//////////////////////GRAPH WINDOW FUNCTIONS//////////////////////////

void View::fill_graph_alt()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create error ALT serie";
    _curves_error_alt = new QLineSeries();
    _curves_error_alt->append(0, 26.1);
    _curves_error_alt->setName("Error ALT");

    QPen pen_error_alt(QColor(255, 0, 0, 255));
    pen_error_alt.setWidth(2);
    pen_error_alt.setStyle(Qt::SolidLine);
    _curves_error_alt->setPen(pen_error_alt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_alt = new QChart();
    _graph_alt->addSeries(_curves_error_alt);
    _graph_alt->setTitle("ALT errors in function of time");
    _graph_alt->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the error ALT serie";
    axeX_error_alt = new QValueAxis;
    axeX_error_alt->setTitleText("Number of the measurement");
    axeX_error_alt->setRange(0, NB_POINTS_GRAPH);
    _graph_alt->addAxis(axeX_error_alt, Qt::AlignBottom);
    _curves_error_alt->attachAxis(axeX_error_alt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the error ALT serie";
    axeY_error_alt = new QValueAxis;
    axeY_error_alt->setTitleText("Error ALT");
    axeY_error_alt->setRange(y_error_min_alt, y_error_max_alt);
    _graph_alt->addAxis(axeY_error_alt, Qt::AlignLeft);
    _curves_error_alt->attachAxis(axeY_error_alt);
}

void View::fill_graph_az()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create error AZ serie";
    _curves_error_az = new QLineSeries();
    _curves_error_az->append(0, 26.1);
    _curves_error_az->setName("Error AZ");

    QPen pen_error_az(QColor(0, 0, 255, 255));
    pen_error_az.setWidth(2);
    pen_error_az.setStyle(Qt::SolidLine);
    _curves_error_az->setPen(pen_error_az);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_az = new QChart();
    _graph_az->addSeries(_curves_error_az);
    _graph_az->setTitle("AZ errors in function of time");
    _graph_az->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the error AZ serie";
    axeX_error_az = new QValueAxis;
    axeX_error_az->setTitleText("Number of the measurement");
    axeX_error_az->setRange(0, NB_POINTS_GRAPH);
    _graph_az->addAxis(axeX_error_az, Qt::AlignBottom);
    _curves_error_az->attachAxis(axeX_error_az);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the error AZ serie";
    axeY_error_az = new QValueAxis;
    axeY_error_az->setTitleText("Error AZ");
    axeY_error_az->setRange(y_error_min_az, y_error_max_az);
    _graph_az->addAxis(axeY_error_az, Qt::AlignLeft);
    _curves_error_az->attachAxis(axeY_error_az);
}

void View::fill_graph_az_power()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create AZ power serie";
    _curves_error_az_power = new QLineSeries();
    _curves_error_az_power->append(0, 26.1);
    _curves_error_az_power->setName("AZ power");

    QPen pen_error_az_power(QColor(0, 200, 0, 255));
    pen_error_az_power.setWidth(2);
    pen_error_az_power.setStyle(Qt::SolidLine);
    _curves_error_az_power->setPen(pen_error_az_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_az_power = new QChart();
    _graph_az_power->addSeries(_curves_error_az_power);
    _graph_az_power->setTitle("AZ power in function of time");
    _graph_az_power->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the power AZ serie";
    axeX_error_az_power = new QValueAxis;
    axeX_error_az_power->setTitleText("Number of the measurement");
    axeX_error_az_power->setRange(0, NB_POINTS_GRAPH);
    _graph_az_power->addAxis(axeX_error_az_power, Qt::AlignBottom);
    _curves_error_az_power->attachAxis(axeX_error_az_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the power AZ serie";
    axeY_error_az_power = new QValueAxis;
    axeY_error_az_power->setTitleText("AZ power");
    axeY_error_az_power->setRange(y_error_min_az_power, y_error_max_az_power);
    _graph_az_power->addAxis(axeY_error_az_power, Qt::AlignLeft);
    _curves_error_az_power->attachAxis(axeY_error_az_power);
}

void View::fill_graph_alt_power()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create ALT power serie";
    _curves_error_alt_power = new QLineSeries();
    _curves_error_alt_power->append(0, 26.1);
    _curves_error_alt_power->setName("ALT power");

    QPen pen_error_alt_power(QColor(255, 150, 0, 255));
    pen_error_alt_power.setWidth(2);
    pen_error_alt_power.setStyle(Qt::SolidLine);
    _curves_error_alt_power->setPen(pen_error_alt_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph_alt_power = new QChart();
    _graph_alt_power->addSeries(_curves_error_alt_power);
    _graph_alt_power->setTitle("ALT power in function of time");
    _graph_alt_power->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the power ALT serie";
    axeX_error_alt_power = new QValueAxis;
    axeX_error_alt_power->setTitleText("Number of the measurement");
    axeX_error_alt_power->setRange(0, NB_POINTS_GRAPH);
    _graph_alt_power->addAxis(axeX_error_alt_power, Qt::AlignBottom);
    _curves_error_alt_power->attachAxis(axeX_error_alt_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the power ALT serie";
    axeY_error_alt_power = new QValueAxis;
    axeY_error_alt_power->setTitleText("ALT power");
    axeY_error_alt_power->setRange(y_error_min_alt_power,
                                   y_error_max_alt_power);
    _graph_alt_power->addAxis(axeY_error_alt_power, Qt::AlignLeft);
    _curves_error_alt_power->attachAxis(axeY_error_alt_power);
}

void View::fill_graph_all_errors()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create error ALT serie";
    _all_error_alt = new QLineSeries();
    _all_error_alt->append(0, 26.1);
    _all_error_alt->setName("Error ALT value");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create error AZ serie";
    _all_error_az = new QLineSeries();
    _all_error_az->append(0, 26.1);
    _all_error_az->setName("Error AZ value");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create ALT power serie";
    _all_error_alt_power = new QLineSeries();
    _all_error_alt_power->append(0, 26.1);
    _all_error_alt_power->setName("ALT power");

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create AZ power serie";
    _all_error_az_power = new QLineSeries();
    _all_error_az_power->append(0, 26.1);
    _all_error_az_power->setName("AZ power");

    QPen pen_error_alt(QColor(255, 0, 0, 255));
    pen_error_alt.setWidth(2);
    pen_error_alt.setStyle(Qt::SolidLine);
    _all_error_alt->setPen(pen_error_alt);

    QPen pen_error_az(QColor(0, 0, 255, 255));
    pen_error_az.setWidth(2);
    pen_error_az.setStyle(Qt::SolidLine);
    _all_error_az->setPen(pen_error_az);

    QPen pen_error_alt_power(QColor(0, 200, 0, 255));
    pen_error_alt_power.setWidth(2);
    pen_error_alt_power.setStyle(Qt::SolidLine);
    _all_error_alt_power->setPen(pen_error_alt_power);

    QPen pen_error_az_power(QColor(255, 150, 0, 255));
    pen_error_az_power.setWidth(2);
    pen_error_az_power.setStyle(Qt::SolidLine);
    _all_error_az_power->setPen(pen_error_az_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create the graph adding the series";
    _graph = new QChart();
    _graph->addSeries(_all_error_alt);
    _graph->addSeries(_all_error_az);
    _graph->addSeries(_all_error_alt_power);
    _graph->addSeries(_all_error_az_power);
    _graph->setTitle("ALT/AZ value error and power in function of time");
    _graph->legend()->setAlignment(Qt::AlignBottom);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the ALT serie";
    QValueAxis *axeX_error_alt = new QValueAxis;
    axeX_error_alt->setRange(0, NB_POINTS_GRAPH);
    _graph->addAxis(axeX_error_alt, Qt::AlignBottom);
    _all_error_alt->attachAxis(axeX_error_alt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the AZ serie";
    QValueAxis *axeX_error_az = new QValueAxis;
    axeX_error_az->setRange(0, NB_POINTS_GRAPH);
    _graph->addAxis(axeX_error_az, Qt::AlignBottom);
    _all_error_az->attachAxis(axeX_error_az);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the ALT power serie";
    QValueAxis *axeX_error_alt_power = new QValueAxis;
    axeX_error_alt_power->setRange(0, NB_POINTS_GRAPH);
    _graph->addAxis(axeX_error_alt_power, Qt::AlignBottom);
    _all_error_alt_power->attachAxis(axeX_error_alt_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis X for the AZ power serie";
    QValueAxis *axeX_error_az_power = new QValueAxis;
    axeX_error_az_power->setRange(0, NB_POINTS_GRAPH);
    _graph->addAxis(axeX_error_az_power, Qt::AlignBottom);
    _all_error_az_power->attachAxis(axeX_error_az_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the ALT serie";
    axeY_all_error_alt = new QValueAxis;
    axeY_all_error_alt->setTitleText("Error ALT value");
    axeY_all_error_alt->setRange(y_all_error_min_alt, y_all_error_max_alt);
    _graph->addAxis(axeY_all_error_alt, Qt::AlignLeft);
    _all_error_alt->attachAxis(axeY_all_error_alt);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the AZ serie";
    axeY_all_error_az = new QValueAxis;
    axeY_all_error_az->setTitleText("Error AZ value");
    axeY_all_error_az->setRange(y_all_error_min_az, y_all_error_max_az);
    _graph->addAxis(axeY_all_error_az, Qt::AlignLeft);
    _all_error_az->attachAxis(axeY_all_error_az);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the ALT power serie";
    axeY_all_error_alt_power = new QValueAxis;
    axeY_all_error_alt_power->setTitleText("ALT power");
    axeY_all_error_alt_power->setRange(y_all_error_min_alt_power,
                                       y_all_error_max_alt_power);
    _graph->addAxis(axeY_all_error_alt_power, Qt::AlignLeft);
    _all_error_alt_power->attachAxis(axeY_all_error_alt_power);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create axis Y for the AZ power serie";
    axeY_all_error_az_power = new QValueAxis;
    axeY_all_error_az_power->setTitleText("AZ power");
    axeY_all_error_az_power->setRange(y_all_error_min_az_power,
                                      y_all_error_max_az_power);
    _graph->addAxis(axeY_all_error_az_power, Qt::AlignLeft);
    _all_error_az_power->attachAxis(axeY_all_error_az_power);
}

void View::set_tabs_graph()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up the graph tabs";
    _graph_tabs = new QTabWidget(_new_window);
    _graph_tabs->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
    _graph_tabs->setIconSize(QSize(20, 20));

    _error_alt_tab = new QWidget();
    _error_az_tab = new QWidget();
    _power_alt_tab = new QWidget();
    _power_az_tab = new QWidget();
    _all_errors_tab = new QWidget();

    _graph_tabs->addTab(_error_alt_tab,"ERROR ALT VALUE");
    _graph_tabs->addTab(_error_az_tab,"ERROR AZ VALUE");
    _graph_tabs->addTab(_power_alt_tab,"ALT POWER");
    _graph_tabs->addTab(_power_az_tab,"AZ POWER");
    _graph_tabs->addTab(_all_errors_tab,"ALL ERRORS SAME GRAPH");
}

void View::show_graph()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Create new window to show graph";
    _string_logging = _string_logging + "show graph button clicked\n";
    _logging_label->setText(_string_logging);
    _logging_scrollArea->verticalScrollBar()->setSliderPosition(
                _logging_label->height());

    _new_window = new QWidget();
    _new_window->setWindowTitle("SHOW GRAPH");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
    set_tabs_graph();
    fill_graph_alt();
    fill_graph_az();
    fill_graph_az_power();
    fill_graph_alt_power();
    fill_graph_all_errors();

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Display the graphs";
    _display_graph_alt = new QChartView(_graph_alt, _error_alt_tab);
    _display_graph_alt->setRenderHint(QPainter::Antialiasing);
    _display_graph_alt->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_az = new QChartView(_graph_az, _error_az_tab);
    _display_graph_az->setRenderHint(QPainter::Antialiasing);
    _display_graph_az->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_az_power = new QChartView(_graph_az_power, _power_az_tab);
    _display_graph_az_power->setRenderHint(QPainter::Antialiasing);
    _display_graph_az_power->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph_alt_power = new QChartView(_graph_alt_power, _power_alt_tab);
    _display_graph_alt_power->setRenderHint(QPainter::Antialiasing);
    _display_graph_alt_power->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    _display_graph = new QChartView(_graph, _all_errors_tab);
    _display_graph->setRenderHint(QPainter::Antialiasing);
    _display_graph->resize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Create a timer to update the graph, y axis autoscale";
    _timer_graph = new QTimer(_new_window);
    QObject::connect(_timer_graph, SIGNAL(timeout()),
                                        this, SLOT(update_graph()));
    _timer_graph->start(DISPLAY_GRAPH_SPEED);
    _new_window->show();
}

void View::update_alt_errors()
{
    int y = QRandomGenerator::global()->bounded(20, 30);
    if(number_points_alt<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_alt) || (y > y_error_max_alt))
        {
            if(y < y_error_min_alt)
            {
                y_all_error_min_alt = y;
                y_error_min_alt = y;
            }

            if(y > y_error_max_alt)
            {
                y_all_error_max_alt = y;
                y_error_max_alt = y;
            }
            axeY_error_alt->setRange(y_error_min_alt-1, y_error_max_alt+1);
            axeY_all_error_alt->setRange(y_all_error_min_alt-1,
                                         y_all_error_max_alt+1);
        }
        _curves_error_alt->append(number_points_alt, y);
        _all_error_alt->append(number_points_alt, y);
        number_points_alt++;
    }
    else{
        number_points_alt = 1;
        y_error_min_alt = y;
        y_error_max_alt = y;
        y_all_error_min_alt = y;
        y_all_error_max_alt = y;
        _curves_error_alt->clear();
        _all_error_alt->clear();
        axeY_error_alt->setRange(y_error_min_alt-1, y_error_max_alt+1);
        axeY_all_error_alt->setRange(y_all_error_min_alt-1,
                                     y_all_error_max_alt+1);
    }
}

void View::update_az_errors()
{
    int y = QRandomGenerator::global()->bounded(15, 20);
    if(number_points_az<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_az) || (y > y_error_max_az))
        {
            if(y < y_error_min_az)
            {
                y_all_error_min_az = y;
                y_error_min_az = y;
            }
            if(y > y_error_max_az)
            {
                y_all_error_max_az = y;
                y_error_max_az = y;
            }
            axeY_error_az->setRange(y_error_min_az-1, y_error_max_az+1);
            axeY_all_error_az->setRange(y_all_error_min_az-1,
                                        y_all_error_max_az+1);
        }
        _curves_error_az->append(number_points_az, y);
        _all_error_az->append(number_points_az, y);
        number_points_az++;
    }
    else{
        number_points_az = 1;
        y_error_min_az = y;
        y_error_max_az = y;
        y_all_error_min_az = y;
        y_all_error_max_az = y;
        _curves_error_az->clear();
        _all_error_az->clear();
        axeY_error_az->setRange(y_error_min_az-1, y_error_max_az+1);
        axeY_all_error_az->setRange(y_all_error_min_az-1,
                                    y_all_error_max_az+1);
    }
}

void View::update_az_power()
{
    int y = QRandomGenerator::global()->bounded(10, 15);
    if(number_points_az_power<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_az_power) || (y > y_error_max_az_power))
        {
            if(y < y_error_min_az_power)
            {
                y_all_error_min_az_power = y;
                y_error_min_az_power = y;
            }
            if(y > y_error_max_az_power)
            {
                y_all_error_max_az_power = y;
                y_error_max_az_power = y;
            }
            axeY_error_az_power->setRange(y_error_min_az_power-1,
                                    y_error_max_az_power+1);
            axeY_all_error_az_power->setRange(y_all_error_min_az_power-1,
                                                y_all_error_max_az_power+1);
        }
        _curves_error_az_power->append(number_points_az_power, y);
        _all_error_az_power->append(number_points_az_power, y);
        number_points_az_power++;
    }
    else{
        number_points_az_power = 1;
        y_error_min_az_power = y;
        y_error_max_az_power = y;
        y_all_error_min_az_power = y;
        y_all_error_max_az_power = y;
        _curves_error_az_power->clear();
        _all_error_az_power->clear();
        axeY_error_az_power->setRange(y_error_min_az_power-1,
                                      y_error_max_az_power+1);
        axeY_all_error_az_power->setRange(y_all_error_min_az_power-1,
                                            y_all_error_max_az_power+1);
    }
}

void View::update_alt_power()
{
    int y = QRandomGenerator::global()->bounded(25, 35);
    if(number_points_alt_power<=NB_POINTS_GRAPH)
    {
        if((y < y_error_min_alt_power) || (y > y_error_max_alt_power))
        {
            if(y < y_error_min_alt_power)
            {
                y_all_error_min_alt_power = y;
                y_error_min_alt_power = y;
            }
            if(y > y_error_max_alt_power)
            {
                y_all_error_max_alt_power = y;
                y_error_max_alt_power = y;
            }
            axeY_error_alt_power->setRange(y_error_min_alt_power-1,
                                    y_error_max_alt_power+1);
            axeY_all_error_alt_power->setRange(y_all_error_min_alt_power-1,
                                                y_all_error_max_alt_power+1);
        }
        _curves_error_alt_power->append(number_points_alt_power, y);
        _all_error_alt_power->append(number_points_alt_power, y);
        number_points_alt_power++;
    }
    else{
        number_points_alt_power = 1;
        y_error_min_alt_power = y;
        y_error_max_alt_power = y;
        y_all_error_min_alt_power = y;
        y_all_error_max_alt_power = y;
        _curves_error_alt_power->clear();
        _all_error_alt_power->clear();
        axeY_error_alt_power->setRange(y_error_min_alt_power-1,
                                      y_error_max_alt_power+1);
        axeY_all_error_alt_power->setRange(y_all_error_min_alt_power-1,
                                            y_all_error_max_alt_power+1);
    }
}

void View::update_graph()
{
    update_alt_errors();
    update_az_errors();
    update_az_power();
    update_alt_power();
}

////////////////////////DOME FUNCTIONS//////////////////////////////

QGroupBox* View::hand_control_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up dome hand control group box";
    QGroupBox *_commands_groupbox = new QGroupBox("Relative AZ command");
    QGridLayout *command_layout = new QGridLayout;

    _west_button_big_dome = new QPushButton("W ++");
    _west_button_big_dome->setToolTip("WEST movement");
    command_layout->addWidget(_west_button_big_dome, 2, 0);
    _west_button_small_dome = new QPushButton("W +");
    _west_button_small_dome->setToolTip("Small WEST movement");
    command_layout->addWidget(_west_button_small_dome, 2, 1);
    _east_button_big_dome = new QPushButton("E ++");
    _east_button_big_dome->setToolTip("EAST movement");
    command_layout->addWidget(_east_button_big_dome, 2, 4);
    _east_button_small_dome = new QPushButton("E +");
    _east_button_small_dome->setToolTip("Small EAST movement");
    command_layout->addWidget(_east_button_small_dome, 2, 3);
    _stop_button_dome = new QPushButton("STOP");
    _stop_button_dome->setToolTip("Emergency STOP");
    command_layout->addWidget(_stop_button_dome, 2, 2);

    _degree_hand_control_dome = new QDoubleSpinBox;
    _degree_hand_control_dome->setRange(0, 360);
    _degree_hand_control_dome->setSingleStep(1);
    _degree_hand_control_dome->setValue(0);
    _degree_hand_control_dome->setToolTip("Between 0 and 360 degrees");
    command_layout->addWidget(_degree_hand_control_dome, 0, 1);

    _minute_hand_control_dome = new QDoubleSpinBox;
    _minute_hand_control_dome->setRange(0, 60);
    _minute_hand_control_dome->setSingleStep(1);
    _minute_hand_control_dome->setValue(0);
    _minute_hand_control_dome->setToolTip("Between 0 and 60 degrees");
    command_layout->addWidget(_minute_hand_control_dome, 0, 3);

    _degree_label_dome = new QLabel("°");
    command_layout->addWidget(_degree_label_dome, 0, 2);
    _minute_label_dome = new QLabel("'");
    command_layout->addWidget(_minute_label_dome, 0, 4);

    _ratio_label_dome = new QLabel("Ratio\nSmall/Big\nmovement");
    command_layout->addWidget(_ratio_label_dome, 1, 1);
    _ratio_combobox_dome = new QComboBox();
    _ratio_combobox_dome->addItem("5");
    _ratio_combobox_dome->addItem("10");
    _ratio_combobox_dome->addItem("15");
    _ratio_combobox_dome->setCurrentText("10");
    command_layout->addWidget(_ratio_combobox_dome, 1, 3);
    _commands_groupbox->setLayout(command_layout);

    QObject::connect(_stop_button_dome, SIGNAL(clicked()),
                     this, SLOT(stop_button_dome_slot()));
    QObject::connect(_west_button_big_dome, SIGNAL(clicked()),
                     this, SLOT(west_big_dome_slot()));
    QObject::connect(_west_button_small_dome, SIGNAL(clicked()),
                     this, SLOT(west_small_dome_slot()));

    QObject::connect(_east_button_big_dome, SIGNAL(clicked()),
                     this, SLOT(east_big_dome_slot()));
    QObject::connect(_east_button_small_dome, SIGNAL(clicked()),
                     this, SLOT(east_small_dome_slot()));

    return _commands_groupbox;
}

void View::west_big_dome_slot()
{
    int ratio = _ratio_combobox_dome->currentText().toInt();
    float degrees = _degree_hand_control_dome->value()*
            static_cast<float>(ratio);
    float minutes = _minute_hand_control_dome->value()*
            static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;

    dome_handcontrol_west = "dome_handcontrol_west," +
            to_string(2) + "," + ss_degree.str() + "," + ss_minute.str();

    _signal_mapper_mqtt->setMapping(_west_button_big_dome,
            dome_handcontrol_west.c_str());
}

void View::west_small_dome_slot()
{
    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control_dome->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control_dome->value();

    dome_handcontrol_west = "dome_handcontrol_west," +
            to_string(2) + "," + ss_degree.str() + "," + ss_minute.str();

    _signal_mapper_mqtt->setMapping(_west_button_small_dome,
            dome_handcontrol_west.c_str());
}

void View::east_big_dome_slot()
{
    int ratio = _ratio_combobox_dome->currentText().toInt();
    float degrees = _degree_hand_control_dome->value()*
            static_cast<float>(ratio);
    float minutes = _minute_hand_control_dome->value()*
            static_cast<float>(ratio);

    std::ostringstream ss_degree;
    ss_degree << degrees;
    std::ostringstream ss_minute;
    ss_minute << minutes;

    dome_handcontrol_east = "dome_handcontrol_east," +
            to_string(2) + "," + ss_degree.str() + "," + ss_minute.str();

    _signal_mapper_mqtt->setMapping(_east_button_big_dome,
            dome_handcontrol_east.c_str());
}

void View::east_small_dome_slot()
{
    std::ostringstream ss_degree;
    ss_degree << _degree_hand_control_dome->value();
    std::ostringstream ss_minute;
    ss_minute << _minute_hand_control_dome->value();

    dome_handcontrol_east = "dome_handcontrol_east," +
            to_string(2) + "," + ss_degree.str() + "," + ss_minute.str();

    _signal_mapper_mqtt->setMapping(_east_button_small_dome,
            dome_handcontrol_east.c_str());
}

void View::stop_button_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Dome hand control STOP button clicked";
    QPalette pal = _stop_button_dome->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_button_dome->setAutoFillBackground(true);
    _stop_button_dome->setPalette(pal);
    _stop_button_dome->update();

    _string_logging_dome = _string_logging_dome +
            "Dome hand control STOP button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::show_loggging_dome()
{
    _string_logging_dome = "Dome logging !!!!\n";
    _logging_label_dome = new QLabel(_string_logging_dome, this);

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up dome logging label scrollbar";
    _logging_scrollArea_dome = new QScrollArea(this);
    _logging_scrollArea_dome->setVerticalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_dome->setHorizontalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_dome->setWidget(_logging_label_dome);
    _logging_scrollArea_dome->setWidgetResizable(true);
    _logging_scrollArea_dome->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scrollArea_dome);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::connection_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up connection dome group box";

    QGroupBox *_dome_groupbox = new QGroupBox("Connection Dome");
    QHBoxLayout *dome_layout = new QHBoxLayout;
    _connect_dome = new QPushButton("Connect");
    dome_layout->addWidget(_connect_dome);
    _disconnect_dome = new QPushButton("Disconnect");
    dome_layout->addWidget(_disconnect_dome);
    _dome_groupbox->setLayout(dome_layout);

    QObject::connect(_connect_dome, SIGNAL(clicked()),
                    this, SLOT(connect_dome_slot()));

    return _dome_groupbox;
}

void View::connect_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Connect dome button clicked";

    QPalette pal = _connect_dome->palette();
    pal.setColor(QPalette::Button, QColor(Qt::green));
    _connect_dome->setAutoFillBackground(true);
    _connect_dome->setPalette(pal);
    _connect_dome->update();

    _string_logging_dome = _string_logging_dome +
            "Connect dome button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::home_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up home dome group box";

    QGroupBox *_home_groupbox = new QGroupBox("Home Dome");
    QGridLayout *home_layout = new QGridLayout;
    _full_home_dome = new QPushButton("FULL HOME");
    home_layout->addWidget(_full_home_dome, 0, 0);
    _AZ_home_dome = new QPushButton("AZ HOME");
    home_layout->addWidget(_AZ_home_dome, 0, 1);
    _shutter_home_dome = new QPushButton("SHUTTER HOME");
    home_layout->addWidget(_shutter_home_dome, 0, 2);
    _stop_home_dome = new QPushButton("STOP");
    _stop_home_dome->setToolTip("Emergency Stop homing");
    home_layout->addWidget(_stop_home_dome, 1, 1);
    _home_groupbox->setLayout(home_layout);

    QObject::connect(_stop_home_dome, SIGNAL(clicked()),
                     this, SLOT(stop_home_dome_slot()));

    return _home_groupbox;
}

void View::stop_home_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Dome stop home button clicked";

    QPalette pal = _stop_home_dome->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_home_dome->setAutoFillBackground(true);
    _stop_home_dome->setPalette(pal);
    _stop_home_dome->update();

    _string_logging_dome = _string_logging_dome +
            "Dome STOP home button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::park_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up park dome group box";

    QGroupBox *_park_groupbox = new QGroupBox("Park Dome");
    QGridLayout *park_layout = new QGridLayout;
    _park_dome = new QPushButton("PARK");
    park_layout->addWidget(_park_dome, 2, 0);
    _unpark_dome = new QPushButton("UNPARK");
    park_layout->addWidget(_unpark_dome, 2, 1);
    _stop_park_dome = new QPushButton("STOP");
    _stop_park_dome->setToolTip("Emergency Stop parking");
    park_layout->addWidget(_stop_park_dome, 2, 2);

    _az_park_spinbox_dome = new QDoubleSpinBox;
    _az_park_spinbox_dome->setRange(0, 360);
    _az_park_spinbox_dome->setSingleStep(1);
    _az_park_spinbox_dome->setValue(180);
    _az_park_spinbox_dome->setToolTip("Between 0 and 360 degrees");
    park_layout->addWidget(_az_park_spinbox_dome, 0, 1);

    _shutter_park_spinbox_dome = new QDoubleSpinBox;
    _shutter_park_spinbox_dome->setRange(0, 100);
    _shutter_park_spinbox_dome->setSingleStep(1);
    _shutter_park_spinbox_dome->setValue(100);
    _shutter_park_spinbox_dome->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_shutter_park_spinbox_dome, 1, 1);

    _AZ_park_label_dome = new QLabel("AZ (°)");
    _AZ_park_label_dome->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_AZ_park_label_dome, 0, 0);
    _shutter_park_label_dome = new QLabel("Shutter (%)");
    _shutter_park_label_dome->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_shutter_park_label_dome, 1, 0);
    _park_groupbox->setLayout(park_layout);

    QObject::connect(_stop_park_dome, SIGNAL(clicked()),
                     this, SLOT(stop_park_dome_slot()));
    QObject::connect(_park_dome, SIGNAL(clicked()),
                     this, SLOT(park_dome_slot()));

    return _park_groupbox;
}

void View::park_dome_slot()
{
    std::ostringstream ss_az;
    ss_az << _az_park_spinbox_dome->value();
    std::ostringstream ss_shutter;
    ss_shutter << _shutter_park_spinbox_dome->value();

    dome_park = "dome_park," + to_string(2) +
            "," + ss_az.str() + "," + ss_shutter.str();

    _signal_mapper_mqtt->setMapping(_park_dome, dome_park.c_str());
}

void View::stop_park_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Park stop button dome clicked";

    QPalette pal = _stop_park_dome->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_park_dome->setAutoFillBackground(true);
    _stop_park_dome->setPalette(pal);
    _stop_park_dome->update();

    _string_logging_dome = _string_logging_dome +
            "Dome STOP park button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::alt_az_commands_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up ALT/AZ command dome group box";

    QGroupBox *_alt_az_groupbox = new QGroupBox("Absolute AZ Command");
    QGridLayout *alt_az_layout = new QGridLayout;

    _degree_az_dome = new QDoubleSpinBox;
    _degree_az_dome->setRange(0, 90);
    _degree_az_dome->setSingleStep(1);
    _degree_az_dome->setValue(0);
    _degree_az_dome->setToolTip("Between 0 and 90 degrees");
    alt_az_layout->addWidget(_degree_az_dome, 0, 0);
    _degree_az_label_dome = new QLabel("°");
    alt_az_layout->addWidget(_degree_az_label_dome, 0, 1);

    _minute_az_dome = new QDoubleSpinBox;
    _minute_az_dome->setRange(0, 60);
    _minute_az_dome->setSingleStep(1);
    _minute_az_dome->setValue(0);
    _minute_az_dome->setToolTip("Between 0 and 60 minutes");
    alt_az_layout->addWidget(_minute_az_dome, 1, 0);
    _minute_az_label_dome = new QLabel("'");
    alt_az_layout->addWidget(_minute_az_label_dome, 1, 1);

    _goto_alt_az_dome = new QPushButton("GOTO");
    alt_az_layout->addWidget(_goto_alt_az_dome, 0, 2);
    _sync_alt_az_dome = new QPushButton("SYNC");
    alt_az_layout->addWidget(_sync_alt_az_dome, 0, 3);
    _speed_alt_az_dome = new QPushButton("SPEED");
    alt_az_layout->addWidget(_speed_alt_az_dome, 1, 2);
    _stop_alt_az_dome = new QPushButton("STOP");
    _stop_alt_az_dome->setToolTip("Emergency Stop ALT/AZ movement");
    alt_az_layout->addWidget(_stop_alt_az_dome, 1, 3);
    _alt_az_groupbox->setLayout(alt_az_layout);

    QObject::connect(_stop_alt_az_dome, SIGNAL(clicked()),
                     this, SLOT(stop_alt_az_dome_slot()));
    QObject::connect(_goto_alt_az_dome, SIGNAL(clicked()),
                     this, SLOT(goto_az_dome_slot()));
    QObject::connect(_sync_alt_az_dome, SIGNAL(clicked()),
                     this, SLOT(sync_az_dome_slot()));
    QObject::connect(_speed_alt_az_dome, SIGNAL(clicked()),
                     this, SLOT(speed_az_dome_slot()));

    return _alt_az_groupbox;
}

void View::goto_az_dome_slot()
{
    std::ostringstream ss_az_degree;
    ss_az_degree << _degree_az_dome->value();
    std::ostringstream ss_az_minute;
    ss_az_minute << _minute_az_dome->value();

    dome_az_goto = "dome_az_goto," + to_string(2) + "," +
            ss_az_degree.str() + "," + ss_az_minute.str();

    _signal_mapper_mqtt->setMapping(_goto_alt_az_dome,
            dome_az_goto.c_str());
}

void View::sync_az_dome_slot()
{
    std::ostringstream ss_az_degree;
    ss_az_degree << _degree_az_dome->value();
    std::ostringstream ss_az_minute;
    ss_az_minute << _minute_az_dome->value();

    dome_az_sync = "dome_az_sync," + to_string(2) + "," +
           ss_az_degree.str() + "," + ss_az_minute.str();

    _signal_mapper_mqtt->setMapping(_sync_alt_az_dome,
            dome_az_sync.c_str());
}

void View::speed_az_dome_slot()
{
    std::ostringstream ss_az_degree;
    ss_az_degree << _degree_az_dome->value();
    std::ostringstream ss_az_minute;
    ss_az_minute << _minute_az_dome->value();

    dome_az_speed = "dome_az_speed," + to_string(2) + "," +
            ss_az_degree.str() + "," + ss_az_minute.str();

    _signal_mapper_mqtt->setMapping(_speed_alt_az_dome,
            dome_az_speed.c_str());
}

void View::stop_alt_az_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "ALT/AZ stop dome button clicked";

    QPalette pal = _stop_alt_az_dome->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_alt_az_dome->setAutoFillBackground(true);
    _stop_alt_az_dome->setPalette(pal);
    _stop_alt_az_dome->update();

    _string_logging_dome = _string_logging_dome +
            "Dome STOP ALT/AZ button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::open_close_shutter()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up open close shutter group box";

    QGroupBox *_shutter_groupbox = new QGroupBox("Open/Close Shutter");
    QGridLayout *shutter_layout = new QGridLayout;

    _open_shutter = new QPushButton("OPEN");
    shutter_layout->addWidget(_open_shutter, 1, 0);
    _close_shutter = new QPushButton("CLOSE");
    shutter_layout->addWidget(_close_shutter, 1, 1);
    _sync_shutter = new QPushButton("SYNC");
    shutter_layout->addWidget(_sync_shutter, 2, 0);
    _stop_move_shutter = new QPushButton("STOP");
    _stop_move_shutter->setToolTip("Emergency Stop shutter");
    shutter_layout->addWidget(_stop_move_shutter, 2, 1);

    _shutter_spinbox = new QSpinBox;
    _shutter_spinbox->setRange(0, 100);
    _shutter_spinbox->setSingleStep(1);
    _shutter_spinbox->setValue(100);
    _shutter_spinbox->setToolTip("Between 0 and 100%");
    shutter_layout->addWidget(_shutter_spinbox, 0, 1);

    _aperture_shutter_label = new QLabel("Aperture (%)");
    _aperture_shutter_label->setAlignment(Qt::AlignCenter);
    shutter_layout->addWidget(_aperture_shutter_label, 0, 0);
    _shutter_groupbox->setLayout(shutter_layout);

    QObject::connect(_stop_move_shutter, SIGNAL(clicked()),
                     this, SLOT(stop_shutter_dome_slot()));
    QObject::connect(_open_shutter, SIGNAL(clicked()),
                     this, SLOT(open_shutter_dome_slot()));
    QObject::connect(_close_shutter, SIGNAL(clicked()),
                     this, SLOT(close_shutter_dome_slot()));
    QObject::connect(_sync_shutter, SIGNAL(clicked()),
                     this, SLOT(sync_shutter_dome_slot()));

    return _shutter_groupbox;
}

void View::open_shutter_dome_slot()
{
    std::ostringstream ss_shutter;
    ss_shutter << _shutter_spinbox->value();

    dome_shutter_open = "dome_shutter_open," + to_string(1) + "," +
            ss_shutter.str();

    _signal_mapper_mqtt->setMapping(_open_shutter,
            dome_shutter_open.c_str());
}

void View::close_shutter_dome_slot()
{
    std::ostringstream ss_shutter;
    ss_shutter << _shutter_spinbox->value();

    dome_shutter_close = "dome_shutter_close," + to_string(1) + "," +
            ss_shutter.str();

    _signal_mapper_mqtt->setMapping(_close_shutter,
            dome_shutter_close.c_str());
}

void View::sync_shutter_dome_slot()
{
    std::ostringstream ss_shutter;
    ss_shutter << _shutter_spinbox->value();

    dome_shutter_sync = "dome_shutter_sync," + to_string(1) + "," +
           ss_shutter.str();

    _signal_mapper_mqtt->setMapping(_sync_shutter,
            dome_shutter_sync.c_str());
}

void View::stop_shutter_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Move shutter stop button clicked";

    QPalette pal = _stop_move_shutter->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_move_shutter->setAutoFillBackground(true);
    _stop_move_shutter->setPalette(pal);
    _stop_move_shutter->update();

    _string_logging_dome = _string_logging_dome +
            "Shutter STOP button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());
}

QGroupBox* View::dome_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up dome infos group box";

    QGroupBox *_dome_groupbox = new QGroupBox("Dome infos");
    QVBoxLayout *dome_layout = new QVBoxLayout;

    _error_AZ_dome = new QLabel("Error AZ: 0 step 0 arc/s");
    dome_layout->addWidget(_error_AZ_dome);
    _error_shutter_dome = new QLabel("Error shutter: 0 step 0 arc/s");
    dome_layout->addWidget(_error_shutter_dome);
    _AZ_display_dome = new QLabel("AZ value: 0° 0'");
    dome_layout->addWidget(_AZ_display_dome);
    _shutter_display_dome = new QLabel("Shutter value: 0%");
    dome_layout->addWidget(_shutter_display_dome);

    _dome_groupbox->setLayout(dome_layout);
    return _dome_groupbox;
}

pair<QGroupBox*,int> View::config_admin_dome()
{
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Set dome config for admin user level group box";

    QGroupBox *_config_groupbox = new QGroupBox("Config");
    QHBoxLayout *config_layout = new QHBoxLayout;

    _config_dome = new QPushButton("Config commands");
    config_layout->addWidget(_config_dome);
    _show_graph_dome = new QPushButton("Show graph");
    config_layout->addWidget(_show_graph_dome);
    _config_groupbox->setLayout(config_layout);

    QObject::connect(_config_dome, SIGNAL(clicked()),
                     this, SLOT(config_dome_slot()));

    return make_pair(_config_groupbox, USER_LEVEL);
}

void View::dome_buttons()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add widgets into the dome layout";
    _dome_layout = new QGridLayout();
    _dome_layout->setRowMinimumHeight(1, 300);
    _dome_layout->setRowMinimumHeight(0, 200);
    _dome_layout->setColumnMinimumWidth(2, 400);
    _dome_layout->addWidget(connection_dome(), 0, 0);
    _dome_layout->addWidget(home_dome(), 0, 1);
    _dome_layout->addWidget(hand_control_dome(), 1, 0);
    _dome_layout->addWidget(show_loggging_dome(), 2, 1);
    _dome_layout->addWidget(alt_az_commands_dome(), 1, 1);
    _dome_layout->addWidget(park_dome(), 2, 0);
    _dome_layout->addWidget(open_close_shutter(), 1, 2);
    _dome_layout->addWidget(dome_infos(), 2, 2);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Verify user level to display config dome groupbox";
    if(config_admin_dome().second <= 0)
        _dome_layout->addWidget(config_admin_dome().first, 0, 2);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Insert the dome layout into the map with the associated level";
    _map_widgets_dome.insert(make_pair(_dome_layout, USER_LEVEL));
}

void View::verify_user_level_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the grid layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it=_map_widgets_dome.begin();
        it != _map_widgets_dome.end(); ++it)
    {
        if(it->second <= 0)
            _dome_tab->setLayout(it->first);
    }
}

////////////////////////////CONFIG DOME FUNCTIONS//////////////////////

QGroupBox* View::az_limits_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up AZ limits dome group box";
    QGroupBox *_az_limits_groupbox = new QGroupBox("AZ Limits");
    QGridLayout *az_layout = new QGridLayout;

    _limit_az_up_dome = new QLabel("Limit AZ up (°)");
    az_layout->addWidget(_limit_az_up_dome, 0, 0);
    _az_up_dome = new QSpinBox;
    _az_up_dome->setRange(0, 360);
    _az_up_dome->setSingleStep(1);
    _az_up_dome->setValue(360);
    _az_up_dome->setToolTip("Between 0 and 360 degrees");
    az_layout->addWidget(_az_up_dome, 0, 1);

    _limit_az_down_dome = new QLabel("Limit AZ down (°)");
    az_layout->addWidget(_limit_az_down_dome, 1, 0);
    _az_down_dome = new QSpinBox;
    _az_down_dome->setRange(0, 360);
    _az_down_dome->setSingleStep(1);
    _az_down_dome->setValue(0);
    _az_down_dome->setToolTip("Between 0 and 360 degrees");
    az_layout->addWidget(_az_down_dome, 1, 1);

    _set_limit_az_dome = new QPushButton("SET");
    az_layout->addWidget(_set_limit_az_dome, 0, 2);
    _reset_limit_az_dome = new QPushButton("RESET");
    az_layout->addWidget(_reset_limit_az_dome, 1, 2);
    _az_limits_groupbox->setLayout(az_layout);

    QObject::connect(_set_limit_az_dome, SIGNAL(clicked()),
        this, SLOT(update_limit_az_dome()));
    QObject::connect(_reset_limit_az_dome, SIGNAL(clicked()),
        this, SLOT(reset_limit_az_dome()));

    return _az_limits_groupbox;
}

void View::update_limit_az_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update AZ limits in config window";
    _list_config_dome.find(0)->second = to_string(_az_up_dome->value());
    _list_config_dome.find(1)->second = to_string(_az_down_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_limit_az_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset AZ limits in config window";
    _list_config_dome.find(0)->second = to_string(360);
    _list_config_dome.find(1)->second = to_string(0);
    _az_up_dome->setValue(360);
    _az_down_dome->setValue(0);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::shutter_limits_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up shutter limits dome group box";
    QGroupBox *_shutter_limits_groupbox = new QGroupBox("Shutter Limits");
    QGridLayout *shutter_layout = new QGridLayout;

    _limit_shutter_up_dome = new QLabel("Limit Shutter up (%)");
    shutter_layout->addWidget(_limit_shutter_up_dome, 0, 0);
    _shutter_up_dome = new QSpinBox;
    _shutter_up_dome->setRange(0, 100);
    _shutter_up_dome->setSingleStep(1);
    _shutter_up_dome->setValue(100);
    _shutter_up_dome->setToolTip("Between 0 and 100%");
    shutter_layout->addWidget(_shutter_up_dome, 0, 1);

    _limit_shutter_down_dome = new QLabel("Limit shutter down (%)");
    shutter_layout->addWidget(_limit_shutter_down_dome, 1, 0);
    _shutter_down_dome = new QSpinBox;
    _shutter_down_dome->setRange(0, 100);
    _shutter_down_dome->setSingleStep(1);
    _shutter_down_dome->setValue(0);
    _shutter_down_dome->setToolTip("Between 0 and 100%");
    shutter_layout->addWidget(_shutter_down_dome, 1, 1);

    _set_limit_shutter_dome = new QPushButton("SET");
    shutter_layout->addWidget(_set_limit_shutter_dome, 0, 2);
    _reset_limit_shutter_dome = new QPushButton("RESET");
    shutter_layout->addWidget(_reset_limit_shutter_dome, 1, 2);
    _shutter_limits_groupbox->setLayout(shutter_layout);

    QObject::connect(_set_limit_shutter_dome, SIGNAL(clicked()),
        this, SLOT(update_limit_shutter()));
    QObject::connect(_reset_limit_shutter_dome, SIGNAL(clicked()),
        this, SLOT(reset_limit_shutter()));

    return _shutter_limits_groupbox;
}

void View::update_limit_shutter()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update shutter limits in config window";
    _list_config_dome.find(2)->second = to_string(_shutter_up_dome->value());
    _list_config_dome.find(3)->second = to_string(_shutter_down_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_limit_shutter()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset shutter limits in config window";
    _list_config_dome.find(2)->second = to_string(100);
    _list_config_dome.find(3)->second = to_string(0);
    _shutter_up_dome->setValue(100);
    _shutter_down_dome->setValue(0);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::speed_limits_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up speed limits group box";
    QGroupBox *_speed_limits_groupbox = new QGroupBox("Speed Limits");
    QGridLayout *speed_layout = new QGridLayout;

    _limit_velocity_dome = new QLabel("Limit MAX Velocity (°/s)");
    speed_layout->addWidget(_limit_velocity_dome, 0, 0);
    _velocity_dome = new QSpinBox;
    _velocity_dome->setRange(0, 10);
    _velocity_dome->setSingleStep(1);
    _velocity_dome->setValue(3);
    _velocity_dome->setToolTip("Between 0 and 10 °/s");
    speed_layout->addWidget(_velocity_dome, 0, 1);

    _limit_acceleration_dome = new QLabel("Limit MAX Acceleration (°/s²)");
    speed_layout->addWidget(_limit_acceleration_dome, 1, 0);
    _acceleration_dome = new QSpinBox;
    _acceleration_dome->setRange(0, 10);
    _acceleration_dome->setSingleStep(1);
    _acceleration_dome->setValue(3);
    _acceleration_dome->setToolTip("Between 0 and 10 °/s²");
    speed_layout->addWidget(_acceleration_dome, 1, 1);

    _set_limit_speed_dome = new QPushButton("SET");
    speed_layout->addWidget(_set_limit_speed_dome, 0, 2);
    _reset_limit_speed_dome = new QPushButton("RESET");
    speed_layout->addWidget(_reset_limit_speed_dome, 1, 2);
    _speed_limits_groupbox->setLayout(speed_layout);

    QObject::connect(_set_limit_speed_dome, SIGNAL(clicked()),
        this, SLOT(update_limit_speed_slot()));
    QObject::connect(_reset_limit_speed_dome, SIGNAL(clicked()),
        this, SLOT(reset_limit_speed_slot()));

    return _speed_limits_groupbox;
}

void View::update_limit_speed_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update speed limits in config window";
    _list_config_dome.find(4)->second = to_string(_velocity_dome->value());
    _list_config_dome.find(5)->second = to_string(_acceleration_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_limit_speed_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset speed limits in config window";
    _list_config_dome.find(4)->second = to_string(3);
    _list_config_dome.find(5)->second = to_string(3);
    _velocity_dome->setValue(3);
    _acceleration_dome->setValue(3);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::velocity_config_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up velocity dome group box";
    QGroupBox *_velocity_groupbox = new QGroupBox("Set Velocity");
    QGridLayout *velocity_layout = new QGridLayout;

    _velocity_homing_dome = new
            QLabel("Velocity for home/park\nmovement (°/s)");
    velocity_layout->addWidget(_velocity_homing_dome, 0, 0);
    _velocity_home_dome = new QSpinBox;
    _velocity_home_dome->setRange(0, 10);
    _velocity_home_dome->setSingleStep(1);
    _velocity_home_dome->setValue(3);
    _velocity_home_dome->setToolTip("Between 0 and 10 °/s");
    velocity_layout->addWidget(_velocity_home_dome, 0, 1);

    _velocity_trajectory_dome = new QLabel("Velocity for\ntrajectory (°/s)");
    velocity_layout->addWidget(_velocity_trajectory_dome, 1, 0);
    _velocity_traj_dome = new QSpinBox;
    _velocity_traj_dome->setRange(0, 10);
    _velocity_traj_dome->setSingleStep(1);
    _velocity_traj_dome->setValue(3);
    _velocity_traj_dome->setToolTip("Between 0 and 10 °/s");
    velocity_layout->addWidget(_velocity_traj_dome, 1, 1);

    _set_velocity_dome = new QPushButton("SET");
    velocity_layout->addWidget(_set_velocity_dome, 0, 2);
    _reset_velocity_dome = new QPushButton("RESET");
    velocity_layout->addWidget(_reset_velocity_dome, 1, 2);
    _velocity_groupbox->setLayout(velocity_layout);

    QObject::connect(_set_velocity_dome, SIGNAL(clicked()),
        this, SLOT(update_velocity_dome()));
    QObject::connect(_reset_velocity_dome, SIGNAL(clicked()),
        this, SLOT(reset_velocity_dome()));

    return _velocity_groupbox;
}

void View::update_velocity_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update velocity in config window";
    _list_config_dome.find(6)->second = to_string(_velocity_home_dome->value());
    _list_config_dome.find(7)->second = to_string(_velocity_traj_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_velocity_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset velocity in config window";
    _list_config_dome.find(6)->second = to_string(3);
    _list_config_dome.find(7)->second = to_string(5);
    _velocity_home_dome->setValue(3);
    _velocity_traj_dome->setValue(5);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::acceleration_config_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up acceleration dome group box";
    QGroupBox *_accel_groupbox = new QGroupBox("Set Acceleration");
    QGridLayout *accel_layout = new QGridLayout;

    _accel_homing_dome = new
            QLabel("Acceleration for\nhome/park movement (°/s²)");
    accel_layout->addWidget(_accel_homing_dome, 0, 0);
    _accel_home_dome = new QSpinBox;
    _accel_home_dome->setRange(0, 10);
    _accel_home_dome->setSingleStep(1);
    _accel_home_dome->setValue(3);
    _accel_home_dome->setToolTip("Between 0 and 10 °/s²");
    accel_layout->addWidget(_accel_home_dome, 0, 1);

    _accel_trajectory_dome = new QLabel("Acceleration for\ntrajectory (°/s²)");
    accel_layout->addWidget(_accel_trajectory_dome, 1, 0);
    _accel_traj_dome = new QSpinBox;
    _accel_traj_dome->setRange(0, 10);
    _accel_traj_dome->setSingleStep(1);
    _accel_traj_dome->setValue(3);
    _accel_traj_dome->setToolTip("Between 0 and 10 °/s²");
    accel_layout->addWidget(_accel_traj_dome, 1, 1);

    _set_accel_dome = new QPushButton("SET");
    accel_layout->addWidget(_set_accel_dome, 0, 2);
    _reset_accel_dome = new QPushButton("RESET");
    accel_layout->addWidget(_reset_accel_dome, 1, 2);
    _accel_groupbox->setLayout(accel_layout);

    QObject::connect(_set_accel_dome, SIGNAL(clicked()),
        this, SLOT(update_acceleration_dome()));
    QObject::connect(_reset_accel_dome, SIGNAL(clicked()),
        this, SLOT(reset_acceleration_dome()));

    return _accel_groupbox;
}

void View::update_acceleration_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update acceleration in config window";
    _list_config_dome.find(8)->second = to_string(_accel_home_dome->value());
    _list_config_dome.find(9)->second = to_string(_accel_traj_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_acceleration_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset acceleration in config window";
    _list_config_dome.find(8)->second = to_string(3);
    _list_config_dome.find(9)->second = to_string(5);
    _accel_home_dome->setValue(3);
    _accel_traj_dome->setValue(5);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::offset_config_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up offset dome group box";
    QGroupBox *_offset_groupbox = new QGroupBox("Set Offset");
    QGridLayout *offset_layout = new QGridLayout;

    _az_offset_label = new QLabel("AZ Offset (°)");
    offset_layout->addWidget(_az_offset_label, 0, 0);
    _az_offset_dome = new QSpinBox;
    _az_offset_dome->setRange(0, 360);
    _az_offset_dome->setSingleStep(1);
    _az_offset_dome->setValue(0);
    _az_offset_dome->setToolTip("Between 0 and 360°");
    offset_layout->addWidget(_az_offset_dome, 0, 1);

    _shutter_offset_label = new QLabel("Shutter Offset (%)");
    offset_layout->addWidget(_shutter_offset_label, 1, 0);
    _shutter_offset_dome = new QSpinBox;
    _shutter_offset_dome->setRange(0, 100);
    _shutter_offset_dome->setSingleStep(1);
    _shutter_offset_dome->setValue(0);
    _shutter_offset_dome->setToolTip("Between 0 and 100%");
    offset_layout->addWidget(_shutter_offset_dome, 1, 1);

    _set_offset_dome = new QPushButton("SET");
    offset_layout->addWidget(_set_offset_dome, 0, 2);
    _reset_offset_dome = new QPushButton("RESET");
    offset_layout->addWidget(_reset_offset_dome, 1, 2);
    _offset_groupbox->setLayout(offset_layout);

    QObject::connect(_set_offset_dome, SIGNAL(clicked()),
        this, SLOT(update_offset_dome()));
    QObject::connect(_reset_offset_dome, SIGNAL(clicked()),
        this, SLOT(reset_offset_dome()));

    return _offset_groupbox;
}

void View::update_offset_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update offset in config window";
    _list_config_dome.find(10)->second = to_string(_az_offset_dome->value());
    _list_config_dome.find(11)->second =
            to_string(_shutter_offset_dome->value());
    write_file_config_dome();
    update_infos_config_dome();
}

void View::reset_offset_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset offset in config window";
    _list_config_dome.find(10)->second = to_string(0);
    _list_config_dome.find(11)->second = to_string(0);
    _az_offset_dome->setValue(0);
    _shutter_offset_dome->setValue(0);
    write_file_config_dome();
    update_infos_config_dome();
}

QGroupBox* View::limits_infos_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up limits infos dome group box";

    QGroupBox *_limits_groupbox = new QGroupBox("");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    _az_up_info_dome = new QLabel("Limit AZ up:");
    limits_layout->addWidget(_az_up_info_dome);
    _az_down_info_dome = new QLabel("Limit AZ down:");
    limits_layout->addWidget(_az_down_info_dome);

    _shutter_up_info = new QLabel("Limit shutter up:");
    limits_layout->addWidget(_shutter_up_info);
    _shutter_down_info = new QLabel("Limit shutter down:");
    limits_layout->addWidget(_shutter_down_info);
    _velocity_limit_info_dome = new QLabel("Velocity limit:");
    limits_layout->addWidget(_velocity_limit_info_dome);
    _accel_limit_info_dome = new QLabel("Acceleration limit:");
    limits_layout->addWidget(_accel_limit_info_dome);

    _velocity_home_info_dome = new QLabel("Velocity home move:");
    limits_layout->addWidget(_velocity_home_info_dome);
    _velocity_traj_info_dome = new QLabel("Velocity traj move:");
    limits_layout->addWidget(_velocity_traj_info_dome);
    _accel_home_info_dome = new QLabel("Acceleration home:");
    limits_layout->addWidget(_accel_home_info_dome);
    _accel_traj_info_dome = new QLabel("Acceleration traj:");
    limits_layout->addWidget(_accel_traj_info_dome);

    _az_offset_info_dome = new QLabel("AZ offset:");
    limits_layout->addWidget(_az_offset_info_dome);
    _shutter_offset_info_dome = new QLabel("Shutter offset:");
    limits_layout->addWidget(_shutter_offset_info_dome);
    _limits_groupbox->setLayout(limits_layout);

    return _limits_groupbox;
}

void View::update_infos_config_dome()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update infos displayed in config dome window";

    _az_up_info_dome->setText(QString::fromStdString(
        "Limit AZ up: " + _list_config_dome.find(0)->second + "°"));
    _az_down_info_dome->setText(QString::fromStdString(
        "Limit AZ down: " + _list_config_dome.find(1)->second + "°"));
    _shutter_up_info->setText(QString::fromStdString(
        "Limit shutter up: " + _list_config_dome.find(2)->second + "%"));
    _shutter_down_info->setText(QString::fromStdString(
        "Limit shutter down: " + _list_config_dome.find(3)->second + "%"));
    _velocity_limit_info_dome->setText(QString::fromStdString(
        "Velocity limit: " + _list_config_dome.find(4)->second + "m/s"));
    _accel_limit_info_dome->setText(QString::fromStdString(
        "Acceleration limit: " + _list_config_dome.find(5)->second + "m/s²"));
    _velocity_home_info_dome->setText(QString::fromStdString(
        "Velocity home move: " + _list_config_dome.find(6)->second + "m/s"));
    _velocity_traj_info_dome->setText(QString::fromStdString(
        "Velocity traj move: " + _list_config_dome.find(7)->second + "m/s"));
    _accel_home_info_dome->setText(QString::fromStdString(
        "Acceleration home: " + _list_config_dome.find(8)->second + "m/s²"));
    _accel_traj_info_dome->setText(QString::fromStdString(
        "Acceleration traj: " + _list_config_dome.find(9)->second + "m/s²"));
    _az_offset_info_dome->setText(QString::fromStdString(
        "AZ offset: " + _list_config_dome.find(10)->second + "°"));
    _shutter_offset_info_dome->setText(QString::fromStdString(
        "Shutter offset: " + _list_config_dome.find(11)->second + "%"));
}

int View::read_file_config_dome()
{
    string text;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Open the config dome file";
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_dome.txt");
    ifstream file(str1);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Dome file not found";
        return -1;
    }

    int i = 0;
    config_dome = "config_dome;12;";
    while(getline(file, text))
    {
        _list_config_dome.insert(make_pair(i, text));
        config_dome = config_dome + text + ";";
        i++;
    }

    if (i == 0) {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Dome file is empty";
        return -1;
    }
    config_dome.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Dome serialized config file: "
                                      << config_dome;
    _string_logging_dome = _string_logging_dome + "Serialized config file: \""
            + QString::fromStdString(config_dome) + "\"\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close dome file";
    return i;
}

int View::write_file_config_dome()
{
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_dome.txt");
    ofstream file;

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Open and delete the file's dome content";
    file.open(str1, ios::out | ios::trunc);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "File dome not found";
        return -1;
    }

    config_dome = "config_dome;12;";
    for(map<int,string>::const_iterator it =_list_config_dome.begin();
        it != _list_config_dome.end(); ++it)
    {
        file << it->second << "\n";
        config_dome = config_dome + it->second + ";";
    }

    config_dome.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Dome serialized config file: "
                                      << config_dome;
    _string_logging_dome = _string_logging_dome + "Serialized config file: \""
            + QString::fromStdString(config_dome) + "\"\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close dome file";
    return 0;
}

void View::config_dome_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Create new window for dome commands configurations";
    _string_logging_dome = _string_logging_dome +
            "config dome button clicked\n";
    _logging_label_dome->setText(_string_logging_dome);
    _logging_scrollArea_dome->verticalScrollBar()->setSliderPosition(
                _logging_label_dome->height());

    _new_window = new QWidget();
    _new_window->setWindowTitle("COMMAND DOME CONFIG");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    QGroupBox *_limits_groupbox = new QGroupBox("Dome Limits");
    QVBoxLayout *limits_layout = new QVBoxLayout;

    limits_layout->addWidget(az_limits_dome());
    limits_layout->addWidget(shutter_limits_dome());
    limits_layout->addWidget(speed_limits_dome());
    _limits_groupbox->setLayout(limits_layout);

    QGroupBox *_set_speed_groupbox = new QGroupBox("Dome Configurations");
    QVBoxLayout *set_speed_layout = new QVBoxLayout;

    set_speed_layout->addWidget(velocity_config_dome());
    set_speed_layout->addWidget(acceleration_config_dome());
    set_speed_layout->addWidget(offset_config_dome());
    _set_speed_groupbox->setLayout(set_speed_layout);

    QGroupBox *_display_groupbox = new QGroupBox("Actual data");
    QVBoxLayout *display_layout = new QVBoxLayout;

    display_layout->addWidget(limits_infos_dome());
    _display_groupbox->setLayout(display_layout);

    _config_dome_layout = new QGridLayout();
    _config_dome_layout->addWidget(_limits_groupbox, 0, 0);
    _config_dome_layout->addWidget(_set_speed_groupbox, 0, 1);
    _config_dome_layout->addWidget(_display_groupbox, 0, 2);

    _new_window->setLayout(_config_dome_layout);
    read_file_config_dome();
    update_infos_config_dome();
    _new_window->show();
}

//////////////////////ACTUATORS M2 M3 FUNCTIONS///////////////////////////

QGroupBox* View::show_logging_actuator()
{
    _string_logging_actuator = "Actuators M2 M3 logging !!!!\n";
    _logging_label_actuator = new QLabel(_string_logging_actuator, this);

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up logging actuator scrollbar";
    _logging_scrollArea_actuator = new QScrollArea(this);
    _logging_scrollArea_actuator->setVerticalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_actuator->setHorizontalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_actuator->setWidget(_logging_label_actuator);
    _logging_scrollArea_actuator->setWidgetResizable(true);
    _logging_scrollArea_actuator->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scrollArea_actuator);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::home_actuator()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up home actuator group box";

    QGroupBox *_home_groupbox = new QGroupBox("Home Actuators");
    QGridLayout *home_layout = new QGridLayout;
    _full_home_actuator_M2 = new QPushButton("FULL HOME M2");
    home_layout->addWidget(_full_home_actuator_M2, 2, 0);
    _home_actuator1_M2 = new QPushButton("HOME ACTUATOR 1 M2");
    home_layout->addWidget(_home_actuator1_M2, 0, 0);
    _home_actuator2_M2 = new QPushButton("HOME ACTUATOR 2 M2");
    home_layout->addWidget(_home_actuator2_M2, 0, 1);
    _home_actuator3_M2 = new QPushButton("HOME ACTUATOR 3 M2");
    home_layout->addWidget(_home_actuator3_M2, 0, 2);

    _full_home_actuator_M3 = new QPushButton("FULL HOME M3");
    home_layout->addWidget(_full_home_actuator_M3, 2, 1);
    _home_actuator1_M3 = new QPushButton("HOME ACTUATOR 1 M3");
    home_layout->addWidget(_home_actuator1_M3, 1, 0);
    _home_actuator2_M3 = new QPushButton("HOME ACTUATOR 2 M3");
    home_layout->addWidget(_home_actuator2_M3, 1, 1);
    _home_actuator3_M3 = new QPushButton("HOME ACTUATOR 3 M3");
    home_layout->addWidget(_home_actuator3_M3, 1, 2);

    _stop_home_actuator = new QPushButton("STOP");
    _stop_home_actuator->setToolTip("Emergency Stop homing");
    home_layout->addWidget(_stop_home_actuator, 2, 2);
    _home_groupbox->setLayout(home_layout);

    QObject::connect(_full_home_actuator_M2, SIGNAL(clicked()),
                     this, SLOT(full_home_M2M3_slot()));
    QObject::connect(_stop_home_actuator, SIGNAL(clicked()),
                     this, SLOT(stop_home_actuator_slot()));

    return _home_groupbox;
}

void View::full_home_M2M3_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Full home M2 M3 actuators";
}

void View::stop_home_actuator_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Actuator stop home button clicked";

    QPalette pal = _stop_home_actuator->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_home_actuator->setAutoFillBackground(true);
    _stop_home_actuator->setPalette(pal);
    _stop_home_actuator->update();

    _string_logging_actuator = _string_logging_actuator +
            "Actuator STOP home button clicked\n";
    _logging_label_actuator->setText(_string_logging_actuator);
    _logging_scrollArea_actuator->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator->height());
}

QGroupBox* View::park_actuator()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up park actuator group box";

    QGroupBox *_park_groupbox = new QGroupBox("Park Actuators");
    QGridLayout *park_layout = new QGridLayout;
    _park_actuator = new QPushButton("PARK");
    park_layout->addWidget(_park_actuator, 1, 2);
    _unpark_actuator = new QPushButton("UNPARK");
    park_layout->addWidget(_unpark_actuator, 2, 2);
    _stop_park_actuator = new QPushButton("STOP");
    _stop_park_actuator->setToolTip("Emergency Stop parking");
    park_layout->addWidget(_stop_park_actuator, 4, 2);

    _park_spinbox_actuator1_M2 = new QDoubleSpinBox;
    _park_spinbox_actuator1_M2->setRange(0, 100);
    _park_spinbox_actuator1_M2->setSingleStep(1);
    _park_spinbox_actuator1_M2->setValue(50);
    _park_spinbox_actuator1_M2->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator1_M2, 0, 1);

    _park_spinbox_actuator2_M2 = new QDoubleSpinBox;
    _park_spinbox_actuator2_M2->setRange(0, 100);
    _park_spinbox_actuator2_M2->setSingleStep(1);
    _park_spinbox_actuator2_M2->setValue(50);
    _park_spinbox_actuator2_M2->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator2_M2, 1, 1);

    _park_spinbox_actuator3_M2 = new QDoubleSpinBox;
    _park_spinbox_actuator3_M2->setRange(0, 100);
    _park_spinbox_actuator3_M2->setSingleStep(1);
    _park_spinbox_actuator3_M2->setValue(50);
    _park_spinbox_actuator3_M2->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator3_M2, 2, 1);

    _park_spinbox_actuator1_M3 = new QDoubleSpinBox;
    _park_spinbox_actuator1_M3->setRange(0, 100);
    _park_spinbox_actuator1_M3->setSingleStep(1);
    _park_spinbox_actuator1_M3->setValue(50);
    _park_spinbox_actuator1_M3->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator1_M3, 3, 1);

    _park_spinbox_actuator2_M3 = new QDoubleSpinBox;
    _park_spinbox_actuator2_M3->setRange(0, 100);
    _park_spinbox_actuator2_M3->setSingleStep(1);
    _park_spinbox_actuator2_M3->setValue(50);
    _park_spinbox_actuator2_M3->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator2_M3, 4, 1);

    _park_spinbox_actuator3_M3 = new QDoubleSpinBox;
    _park_spinbox_actuator3_M3->setRange(0, 100);
    _park_spinbox_actuator3_M3->setSingleStep(1);
    _park_spinbox_actuator3_M3->setValue(50);
    _park_spinbox_actuator3_M3->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator3_M3, 5, 1);

    _park_label_actuator1_M2 = new QLabel("ACTUATOR 1 M2 (%)");
    _park_label_actuator1_M2->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator1_M2, 0, 0);
    _park_label_actuator2_M2 = new QLabel("ACTUATOR 2 M2 (%)");
    _park_label_actuator2_M2->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator2_M2, 1, 0);
    _park_label_actuator3_M2 = new QLabel("ACTUATOR 3 M2 (%)");
    _park_label_actuator3_M2->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator3_M2, 2, 0);

    _park_label_actuator1_M3 = new QLabel("ACTUATOR 1 M3 (%)");
    _park_label_actuator1_M3->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator1_M3, 3, 0);
    _park_label_actuator2_M3 = new QLabel("ACTUATOR 2 M3 (%)");
    _park_label_actuator2_M3->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator2_M3, 4, 0);
    _park_label_actuator3_M3 = new QLabel("ACTUATOR 3 M3 (%)");
    _park_label_actuator3_M3->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator3_M3, 5, 0);
    _park_groupbox->setLayout(park_layout);

    QObject::connect(_stop_park_actuator, SIGNAL(clicked()),
                     this, SLOT(stop_park_actuator_slot()));
    QObject::connect(_park_actuator, SIGNAL(clicked()),
                     this, SLOT(park_actuator_slot()));

    return _park_groupbox;
}

void View::park_actuator_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Park button actuator clicked";
}

void View::stop_park_actuator_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Park stop button actuator clicked";

    QPalette pal = _stop_park_actuator->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_park_actuator->setAutoFillBackground(true);
    _stop_park_actuator->setPalette(pal);
    _stop_park_actuator->update();

    _string_logging_actuator = _string_logging_actuator +
            "Actuator STOP park button clicked\n";
    _logging_label_actuator->setText(_string_logging_actuator);
    _logging_scrollArea_actuator->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator->height());
}

QGroupBox* View::hand_control_actuator()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up actuator hand control groupbox";
    QGroupBox *_actuators_groupbox = new QGroupBox("Commands");
    QGridLayout *actuators_layout = new QGridLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up 3 actuators for M2";
    QGroupBox *_actuator1_groupbox = new QGroupBox("Mirror 2");
    QGridLayout *actuator1_layout = new QGridLayout;
    QGroupBox *_actuator1_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuator1_relat_layout = new QHBoxLayout;
    _forward_actuator1_relat = new QPushButton("FORWARD");
    actuator1_relat_layout->addWidget(_forward_actuator1_relat);
    _backward_actuator1_relat = new QPushButton("BACKWARD");
    actuator1_relat_layout->addWidget(_backward_actuator1_relat);
    _actuator1_relat_groupbox->setLayout(actuator1_relat_layout);

    QGroupBox *_actuator1_abs_groupbox = new QGroupBox("Absolute commands");
    QHBoxLayout *actuator1_abs_layout = new QHBoxLayout;
    _forward_actuator1_abs = new QPushButton("FORWARD");
    actuator1_abs_layout->addWidget(_forward_actuator1_abs);
    _backward_actuator1_abs = new QPushButton("BACKWARD");
    actuator1_abs_layout->addWidget(_backward_actuator1_abs);
    _actuator1_abs_groupbox->setLayout(actuator1_abs_layout);

    QGroupBox *_mirror1_groupbox = new QGroupBox("Choice of actuator");
    QHBoxLayout *_mirror1_layout = new QHBoxLayout;
    _mirror1_actuator1 = new QRadioButton("Actuator 1");
    _mirror1_actuator1->setChecked(true);
    _mirror1_layout->addWidget(_mirror1_actuator1);
    _mirror1_actuator2 = new QRadioButton("Actuator 2");
    _mirror1_layout->addWidget(_mirror1_actuator2);
    _mirror1_actuator3 = new QRadioButton("Actuator 3");
    _mirror1_layout->addWidget(_mirror1_actuator3);
    _mirror1_groupbox->setLayout(_mirror1_layout);

    actuator1_layout->addWidget(_actuator1_relat_groupbox, 0, 0);
    actuator1_layout->addWidget(_actuator1_abs_groupbox, 1, 0);
    actuator1_layout->addWidget(_mirror1_groupbox, 2, 0);
    _actuator1_groupbox->setLayout(actuator1_layout);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up 3 actuators for M3";
    QGroupBox *_actuator2_groupbox = new QGroupBox("Mirror 3");
    QGridLayout *actuator2_layout = new QGridLayout;
    QGroupBox *_actuator2_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuator2_relat_layout = new QHBoxLayout;
    _forward_actuator2_relat = new QPushButton("FORWARD");
    actuator2_relat_layout->addWidget(_forward_actuator2_relat);
    _backward_actuator2_relat = new QPushButton("BACKWARD");
    actuator2_relat_layout->addWidget(_backward_actuator2_relat);
    _actuator2_relat_groupbox->setLayout(actuator2_relat_layout);

    QGroupBox *_actuator2_abs_groupbox = new QGroupBox("Absolute commands");
    QHBoxLayout *actuator2_abs_layout = new QHBoxLayout;
    _forward_actuator2_abs = new QPushButton("FORWARD");
    actuator2_abs_layout->addWidget(_forward_actuator2_abs);
    _backward_actuator2_abs = new QPushButton("BACKWARD");
    actuator2_abs_layout->addWidget(_backward_actuator2_abs);
    _actuator2_abs_groupbox->setLayout(actuator2_abs_layout);

    QGroupBox *_mirror2_groupbox = new QGroupBox("Choice of actuator");
    QHBoxLayout *_mirror2_layout = new QHBoxLayout;
    _mirror2_actuator1 = new QRadioButton("Actuator 1");
    _mirror2_actuator1->setChecked(true);
    _mirror2_layout->addWidget(_mirror2_actuator1);
    _mirror2_actuator2 = new QRadioButton("Actuator 2");
    _mirror2_layout->addWidget(_mirror2_actuator2);
    _mirror2_actuator3 = new QRadioButton("Actuator 3");
    _mirror2_layout->addWidget(_mirror2_actuator3);
    _mirror2_groupbox->setLayout(_mirror2_layout);

    actuator2_layout->addWidget(_actuator2_relat_groupbox, 0, 0);
    actuator2_layout->addWidget(_actuator2_abs_groupbox, 1, 0);
    actuator2_layout->addWidget(_mirror2_groupbox, 2, 0);
    _actuator2_groupbox->setLayout(actuator2_layout);

    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Set up movements for all actuators at the same time";
    QGroupBox *_actuator_all_groupbox = new QGroupBox("All actuator M2");
    QHBoxLayout *actuator_all_layout = new QHBoxLayout;

    QGroupBox *_actuators_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuators_relat_layout = new QHBoxLayout;
    _forward_actuators_relat = new QPushButton("FORWARD");
    actuators_relat_layout->addWidget(_forward_actuators_relat);
    _backward_actuators_relat = new QPushButton("BACKWARD");
    actuators_relat_layout->addWidget(_backward_actuators_relat);
    _stop_actuators_relat = new QPushButton("STOP");
    _stop_actuators_relat->setToolTip("Emergency Stop parking");
    actuators_relat_layout->addWidget(_stop_actuators_relat);
    _actuators_relat_groupbox->setLayout(actuators_relat_layout);

    actuator_all_layout->addWidget(_actuators_relat_groupbox);
    _actuator_all_groupbox->setLayout(actuator_all_layout);

    QGroupBox *_actuator_all_move_groupbox = new QGroupBox("Control JOG");
    QHBoxLayout *actuator_all_move_layout = new QHBoxLayout;

    QGroupBox *_actuator_type_groupbox = new QGroupBox("Commands Type");
    QHBoxLayout *actuator_type_layout = new QHBoxLayout;
    _JOG_actuator = new QRadioButton("JOG");
    _JOG_actuator->setChecked(true);
    actuator_type_layout->addWidget(_JOG_actuator);
    _velocity_actuator = new QRadioButton("speed");
    actuator_type_layout->addWidget(_velocity_actuator);
    _actuator_type_groupbox->setLayout(actuator_type_layout);

    QGroupBox *_actuator_value_groupbox = new QGroupBox("Commands Value");
    QHBoxLayout *actuator_value_layout = new QHBoxLayout;
    _milli_hand_control = new QDoubleSpinBox;
    _milli_hand_control->setRange(0, 1000);
    _milli_hand_control->setSingleStep(1);
    _milli_hand_control->setValue(0);
    _milli_hand_control->setToolTip("Between 0 and 1000mm");
    actuator_value_layout->addWidget(_milli_hand_control);
    _milli_label = new QLabel("mm");
    actuator_value_layout->addWidget(_milli_label);

    _micro_hand_control = new QDoubleSpinBox;
    _micro_hand_control->setRange(0, 999);
    _micro_hand_control->setSingleStep(1);
    _micro_hand_control->setValue(0);
    _micro_hand_control->setToolTip("Between 0 and 1000mm");
    actuator_value_layout->addWidget(_micro_hand_control);
    _micro_label = new QLabel("µm");
    actuator_value_layout->addWidget(_micro_label);
    _actuator_value_groupbox->setLayout(actuator_value_layout);

    actuator_all_move_layout->addWidget(_actuator_type_groupbox);
    actuator_all_move_layout->addWidget(_actuator_value_groupbox);
    _actuator_all_move_groupbox->setLayout(actuator_all_move_layout);

    actuators_layout->addWidget(_actuator1_groupbox, 0, 0);
    actuators_layout->addWidget(_actuator2_groupbox, 0, 1);
    actuators_layout->addWidget(_actuator_all_groupbox, 1, 0);
    actuators_layout->addWidget(_actuator_all_move_groupbox, 1, 1);
    _actuators_groupbox->setLayout(actuators_layout);

    QObject::connect(_stop_actuators_relat, SIGNAL(clicked()),
                     this, SLOT(stop_button_actuator_slot()));
    QObject::connect(_forward_actuators_relat, SIGNAL(clicked()),
                     this, SLOT(forward_relat_all_slot()));
    QObject::connect(_forward_actuator1_abs, SIGNAL(clicked()),
                     this, SLOT(forward_abs_act1_slot()));

    return _actuators_groupbox;
}

void View::forward_relat_all_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Move relative forward all actuators";
}

void View::forward_abs_act1_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Move asbolute forward actuator 1";
}

void View::stop_button_actuator_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Stop button actuator clicked";
    QPalette pal = _stop_actuators_relat->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_actuators_relat->setAutoFillBackground(true);
    _stop_actuators_relat->setPalette(pal);
    _stop_actuators_relat->update();

    _string_logging_actuator = _string_logging_actuator +
            "Actuator hand control STOP button clicked\n";
    _logging_label_actuator->setText(_string_logging_actuator);
    _logging_scrollArea_actuator->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator->height());
}

QGroupBox* View::actuator_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up actuator infos group box";

    QGroupBox *_actuator_groupbox = new QGroupBox("Actuators infos");
    QVBoxLayout *actuator_layout = new QVBoxLayout;

    _travel_actuator1_M2 = new QLabel("Travel actuator 1 M2: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator1_M2);
    _travel_actuator2_M2 = new QLabel("Travel actuator 2 M2: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator2_M2);
    _travel_actuator3_M2 = new QLabel("Travel actuator 3 M2: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator3_M2);
    _travel_actuator1_M3 = new QLabel("Travel actuator 1 M3: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator1_M3);
    _travel_actuator2_M3 = new QLabel("Travel actuator 2 M3: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator2_M3);
    _travel_actuator3_M3 = new QLabel("Travel actuator 3 M3: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator3_M3);

    _velocity_label = new QLabel("Velocity: 0 mm/s");
    actuator_layout->addWidget(_velocity_label);
    _jog_actuator = new QLabel("Jog step: 0mm 0µm");
    actuator_layout->addWidget(_jog_actuator);
    _actuator_groupbox->setLayout(actuator_layout);

    return _actuator_groupbox;
}

QGroupBox* View::connection_actuator()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up connection actuator group box";

    QGroupBox *_actuator_groupbox = new QGroupBox("Connection Actuators");
    QGridLayout *actuator_layout = new QGridLayout;
    _connect_actuator = new QPushButton("Connect");
    actuator_layout->addWidget(_connect_actuator, 0, 0);
    _disconnect_actuator = new QPushButton("Disconnect");
    actuator_layout->addWidget(_disconnect_actuator, 0, 1);
    _backlash = new QPushButton("Backlash");
    actuator_layout->addWidget(_backlash, 2, 0);
    _previous_pos = new QPushButton("Previous position");
    actuator_layout->addWidget(_previous_pos, 1, 0);
    _store_pos = new QPushButton("Store position");
    actuator_layout->addWidget(_store_pos, 1, 1);
    _actuator_groupbox->setLayout(actuator_layout);

    QObject::connect(_connect_actuator, SIGNAL(clicked()),
                     this, SLOT(connect_actuator_slot()));

    return _actuator_groupbox;
}

void View::connect_actuator_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Connect actuator button clicked";

    QPalette pal = _connect_actuator->palette();
    pal.setColor(QPalette::Button, QColor(Qt::green));
    _connect_actuator->setAutoFillBackground(true);
    _connect_actuator->setPalette(pal);
    _connect_actuator->update();

    _string_logging_actuator = _string_logging_actuator +
            "Connect actuator button clicked\n";
    _logging_label_actuator->setText(_string_logging_actuator);
    _logging_scrollArea_actuator->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator->height());
}

void View::actuator_buttons()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add widgets into the actuators layout";
    _actuator_layout = new QGridLayout();

    _actuator_layout->addWidget(connection_actuator(), 0, 0);
    _actuator_layout->addWidget(home_actuator(), 1, 0);
    _actuator_layout->addWidget(show_logging_actuator(), 2, 1);
    _actuator_layout->addWidget(park_actuator(), 0, 1);
    _actuator_layout->addWidget(hand_control_actuator(), 2, 0);
    _actuator_layout->addWidget(actuator_infos(), 1, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
        << "Insert the actuator layout into the map with the associated level";
    _map_widgets_actuator.insert(make_pair(_actuator_layout, USER_LEVEL));

    read_file_backlash();
}

void View::verify_user_level_actuator()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the actuator layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it=_map_widgets_actuator.begin();
        it != _map_widgets_actuator.end(); ++it)
    {
        if(it->second <= 0)
            _actuator_tab->setLayout(it->first);
    }
}

int View::read_file_backlash()
{
    string text;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Open the backlash actuator file";
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../backlash.txt");
    ifstream file(str1);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Backlash file not found";
        return -1;
    }

    int i = 0;
    backlash_serialized = "backlash;6;";
    while(getline(file, text))
    {
        _list_backlash.insert(make_pair(i, text));
        backlash_serialized = backlash_serialized + text + ";";
        i++;
    }

    if (i == 0) {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Backlash file is empty";
        return -1;
    }

    backlash_serialized.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Backlash serialized config file: "
                                      << backlash_serialized;
    _string_logging_actuator = _string_logging_actuator +
            "Serialized backlash file: \""
            + QString::fromStdString(backlash_serialized) + "\"\n";
    _logging_label_actuator->setText(_string_logging_actuator);
    _logging_scrollArea_actuator->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close backlash file";
    return i;
}

//////////////////////ACTUATORS M4 M5 FUNCTIONS///////////////////////////

QGroupBox* View::show_logging_actuator_M4_M5()
{
    _string_logging_actuator_M4_M5 = "Actuators M4 M5 logging !!!!\n";
    _logging_label_actuator_M4_M5 = new QLabel(_string_logging_actuator_M4_M5,
                                               this);

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up M4 M5 logging label scrollbar";
    _logging_scrollArea_actuator_M4_M5 = new QScrollArea(this);
    _logging_scrollArea_actuator_M4_M5->setVerticalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_actuator_M4_M5->setHorizontalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_actuator_M4_M5->setWidget(
                _logging_label_actuator_M4_M5);
    _logging_scrollArea_actuator_M4_M5->setWidgetResizable(true);
    _logging_scrollArea_actuator_M4_M5->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scrollArea_actuator_M4_M5);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::home_actuator_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up home M4 M5 group box";

    QGroupBox *_home_groupbox = new QGroupBox("Home Actuators");
    QGridLayout *home_layout = new QGridLayout;
    _full_home_actuator_M4 = new QPushButton("FULL HOME M4");
    home_layout->addWidget(_full_home_actuator_M4, 2, 0);
    _home_actuator1_M4 = new QPushButton("HOME ACTUATOR 1 M4");
    home_layout->addWidget(_home_actuator1_M4, 0, 0);
    _home_actuator2_M4 = new QPushButton("HOME ACTUATOR 2 M4");
    home_layout->addWidget(_home_actuator2_M4, 0, 1);
    _home_actuator3_M4 = new QPushButton("HOME ACTUATOR 3 M4");
    home_layout->addWidget(_home_actuator3_M4, 0, 2);

    _full_home_actuator_M5 = new QPushButton("FULL HOME M5");
    home_layout->addWidget(_full_home_actuator_M5, 2, 1);
    _home_actuator2_M5 = new QPushButton("HOME ACTUATOR 1 M5");
    home_layout->addWidget(_home_actuator2_M5, 1, 0);
    _home_actuator3_M5 = new QPushButton("HOME ACTUATOR 2 M5");
    home_layout->addWidget(_home_actuator3_M5, 1, 1);
    _home_actuator3_M5 = new QPushButton("HOME ACTUATOR 3 M5");
    home_layout->addWidget(_home_actuator3_M5, 1, 2);

    _stop_home_actuator_M4_M5 = new QPushButton("STOP");
    _stop_home_actuator_M4_M5->setToolTip("Emergency Stop homing");
    home_layout->addWidget(_stop_home_actuator_M4_M5, 2, 2);
    _home_groupbox->setLayout(home_layout);

    QObject::connect(_stop_home_actuator_M4_M5, SIGNAL(clicked()),
                     this, SLOT(stop_home_actuator_slot_M4_M5()));

    return _home_groupbox;
}

void View::stop_home_actuator_slot_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "M4 M5 stop home button clicked";

    QPalette pal = _stop_home_actuator_M4_M5->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_home_actuator_M4_M5->setAutoFillBackground(true);
    _stop_home_actuator_M4_M5->setPalette(pal);
    _stop_home_actuator_M4_M5->update();

    _string_logging_actuator_M4_M5 = _string_logging_actuator_M4_M5 +
            "M4 M5 STOP home button clicked\n";
    _logging_label_actuator_M4_M5->setText(_string_logging_actuator_M4_M5);
    _logging_scrollArea_actuator_M4_M5->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator_M4_M5->height());
}

QGroupBox* View::park_actuator_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up park M4 M5 group box";

    QGroupBox *_park_groupbox = new QGroupBox("Park Actuators");
    QGridLayout *park_layout = new QGridLayout;
    _park_actuator_M4_M5 = new QPushButton("PARK");
    park_layout->addWidget(_park_actuator_M4_M5, 1, 2);
    _unpark_actuator_M4_M5 = new QPushButton("UNPARK");
    park_layout->addWidget(_unpark_actuator_M4_M5, 2, 2);
    _stop_park_actuator_M4_M5 = new QPushButton("STOP");
    _stop_park_actuator_M4_M5->setToolTip("Emergency Stop parking");
    park_layout->addWidget(_stop_park_actuator_M4_M5, 4, 2);

    _park_spinbox_actuator1_M4 = new QDoubleSpinBox;
    _park_spinbox_actuator1_M4->setRange(0, 100);
    _park_spinbox_actuator1_M4->setSingleStep(1);
    _park_spinbox_actuator1_M4->setValue(50);
    _park_spinbox_actuator1_M4->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator1_M4, 0, 1);

    _park_spinbox_actuator2_M4 = new QDoubleSpinBox;
    _park_spinbox_actuator2_M4->setRange(0, 100);
    _park_spinbox_actuator2_M4->setSingleStep(1);
    _park_spinbox_actuator2_M4->setValue(50);
    _park_spinbox_actuator2_M4->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator2_M4, 1, 1);

    _park_spinbox_actuator3_M4 = new QDoubleSpinBox;
    _park_spinbox_actuator3_M4->setRange(0, 100);
    _park_spinbox_actuator3_M4->setSingleStep(1);
    _park_spinbox_actuator3_M4->setValue(50);
    _park_spinbox_actuator3_M4->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator3_M4, 2, 1);

    _park_spinbox_actuator1_M5 = new QDoubleSpinBox;
    _park_spinbox_actuator1_M5->setRange(0, 100);
    _park_spinbox_actuator1_M5->setSingleStep(1);
    _park_spinbox_actuator1_M5->setValue(50);
    _park_spinbox_actuator1_M5->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator1_M5, 3, 1);

    _park_spinbox_actuator2_M5 = new QDoubleSpinBox;
    _park_spinbox_actuator2_M5->setRange(0, 100);
    _park_spinbox_actuator2_M5->setSingleStep(1);
    _park_spinbox_actuator2_M5->setValue(50);
    _park_spinbox_actuator2_M5->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator2_M5, 4, 1);

    _park_spinbox_actuator3_M5 = new QDoubleSpinBox;
    _park_spinbox_actuator3_M5->setRange(0, 100);
    _park_spinbox_actuator3_M5->setSingleStep(1);
    _park_spinbox_actuator3_M5->setValue(50);
    _park_spinbox_actuator3_M5->setToolTip("Between 0 and 100%");
    park_layout->addWidget(_park_spinbox_actuator3_M5, 5, 1);

    _park_label_actuator1_M4 = new QLabel("ACTUATOR 1 M4 (%)");
    _park_label_actuator1_M4->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator1_M4, 0, 0);
    _park_label_actuator2_M4 = new QLabel("ACTUATOR 2 M4 (%)");
    _park_label_actuator2_M4->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator2_M4, 1, 0);
    _park_label_actuator3_M4 = new QLabel("ACTUATOR 3 M4 (%)");
    _park_label_actuator3_M4->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator3_M4, 2, 0);

    _park_label_actuator1_M5 = new QLabel("ACTUATOR 1 M5 (%)");
    _park_label_actuator1_M5->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator1_M5, 3, 0);
    _park_label_actuator2_M5 = new QLabel("ACTUATOR 2 M5 (%)");
    _park_label_actuator2_M5->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator2_M5, 4, 0);
    _park_label_actuator3_M5 = new QLabel("ACTUATOR 3 M5 (%)");
    _park_label_actuator3_M5->setAlignment(Qt::AlignCenter);
    park_layout->addWidget(_park_label_actuator3_M5, 5, 0);
    _park_groupbox->setLayout(park_layout);

    QObject::connect(_stop_park_actuator_M4_M5, SIGNAL(clicked()),
                     this, SLOT(stop_park_actuator_slot_M4_M5()));

    return _park_groupbox;
}

void View::stop_park_actuator_slot_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Park stop button M4 M5 clicked";

    QPalette pal = _stop_park_actuator_M4_M5->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_park_actuator_M4_M5->setAutoFillBackground(true);
    _stop_park_actuator_M4_M5->setPalette(pal);
    _stop_park_actuator_M4_M5->update();

    _string_logging_actuator_M4_M5 = _string_logging_actuator_M4_M5 +
            "M4 M5 STOP park button clicked\n";
    _logging_label_actuator_M4_M5->setText(_string_logging_actuator_M4_M5);
    _logging_scrollArea_actuator_M4_M5->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator_M4_M5->height());
}

QGroupBox* View::actuator_infos_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up M4 M5 infos group box";

    QGroupBox *_actuator_groupbox = new QGroupBox("Actuators infos");
    QVBoxLayout *actuator_layout = new QVBoxLayout;

    _travel_actuator1_M4 = new QLabel("Travel actuator 1 M4: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator1_M4);
    _travel_actuator2_M4 = new QLabel("Travel actuator 2 M4: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator2_M4);
    _travel_actuator3_M4 = new QLabel("Travel actuator 3 M4: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator3_M4);
    _travel_actuator1_M5 = new QLabel("Travel actuator 1 M5: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator1_M5);
    _travel_actuator2_M5 = new QLabel("Travel actuator 2 M5: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator2_M5);
    _travel_actuator3_M5 = new QLabel("Travel actuator 3 M5: 0mm 0µm");
    actuator_layout->addWidget(_travel_actuator3_M5);

    _velocity_label_M4_M5 = new QLabel("Velocity: 0 mm/s");
    actuator_layout->addWidget(_velocity_label_M4_M5);
    _jog_actuator_M4_M5 = new QLabel("Jog step: 0mm 0µm");
    actuator_layout->addWidget(_jog_actuator_M4_M5);
    _actuator_groupbox->setLayout(actuator_layout);

    return _actuator_groupbox;
}

QGroupBox* View::hand_control_actuator_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up M4 M5 hand control group box";
    QGroupBox *_actuators_groupbox = new QGroupBox("Commands");
    QGridLayout *actuators_layout = new QGridLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up 3 actuators for M4";
    QGroupBox *_actuator1_groupbox = new QGroupBox("Mirror 4");
    QGridLayout *actuator1_layout = new QGridLayout;
    QGroupBox *_actuator1_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuator1_relat_layout = new QHBoxLayout;
    _forward_relat_M4 = new QPushButton("FORWARD");
    actuator1_relat_layout->addWidget(_forward_relat_M4);
    _backward_relat_M4 = new QPushButton("BACKWARD");
    actuator1_relat_layout->addWidget(_backward_relat_M4);
    _actuator1_relat_groupbox->setLayout(actuator1_relat_layout);

    QGroupBox *_actuator1_abs_groupbox = new QGroupBox("Absolute commands");
    QHBoxLayout *actuator1_abs_layout = new QHBoxLayout;
    _forward_abs_M4 = new QPushButton("FORWARD");
    actuator1_abs_layout->addWidget(_forward_abs_M4);
    _backward_abs_M4 = new QPushButton("BACKWARD");
    actuator1_abs_layout->addWidget(_backward_abs_M4);
    _actuator1_abs_groupbox->setLayout(actuator1_abs_layout);

    QGroupBox *_mirror1_groupbox = new QGroupBox("Choice of actuator");
    QHBoxLayout *_mirror1_layout = new QHBoxLayout;
    _mirror4_actuator1 = new QRadioButton("Actuator 1");
    _mirror4_actuator1->setChecked(true);
    _mirror1_layout->addWidget(_mirror4_actuator1);
    _mirror4_actuator2 = new QRadioButton("Actuator 2");
    _mirror1_layout->addWidget(_mirror4_actuator2);
    _mirror4_actuator3 = new QRadioButton("Actuator 3");
    _mirror1_layout->addWidget(_mirror4_actuator3);
    _mirror1_groupbox->setLayout(_mirror1_layout);

    actuator1_layout->addWidget(_actuator1_relat_groupbox, 0, 0);
    actuator1_layout->addWidget(_actuator1_abs_groupbox, 1, 0);
    actuator1_layout->addWidget(_mirror1_groupbox, 2, 0);
    _actuator1_groupbox->setLayout(actuator1_layout);

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up 3 actuators for M5";
    QGroupBox *_actuator2_groupbox = new QGroupBox("Mirror 5");
    QGridLayout *actuator2_layout = new QGridLayout;
    QGroupBox *_actuator2_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuator2_relat_layout = new QHBoxLayout;
    _forward_relat_M5 = new QPushButton("FORWARD");
    actuator2_relat_layout->addWidget(_forward_relat_M5);
    _backward_relat_M5 = new QPushButton("BACKWARD");
    actuator2_relat_layout->addWidget(_backward_relat_M5);
    _actuator2_relat_groupbox->setLayout(actuator2_relat_layout);

    QGroupBox *_actuator2_abs_groupbox = new QGroupBox("Absolute commands");
    QHBoxLayout *actuator2_abs_layout = new QHBoxLayout;
    _forward_abs_M5 = new QPushButton("FORWARD");
    actuator2_abs_layout->addWidget(_forward_abs_M5);
    _backward_abs_M5 = new QPushButton("BACKWARD");
    actuator2_abs_layout->addWidget(_backward_abs_M5);
    _actuator2_abs_groupbox->setLayout(actuator2_abs_layout);

    QGroupBox *_mirror2_groupbox = new QGroupBox("Choice of actuator");
    QHBoxLayout *_mirror2_layout = new QHBoxLayout;
    _mirror5_actuator1 = new QRadioButton("Actuator 1");
    _mirror5_actuator1->setChecked(true);
    _mirror2_layout->addWidget(_mirror5_actuator1);
    _mirror5_actuator2 = new QRadioButton("Actuator 2");
    _mirror2_layout->addWidget(_mirror5_actuator2);
    _mirror5_actuator3 = new QRadioButton("Actuator 3");
    _mirror2_layout->addWidget(_mirror5_actuator3);
    _mirror2_groupbox->setLayout(_mirror2_layout);

    actuator2_layout->addWidget(_actuator2_relat_groupbox, 0, 0);
    actuator2_layout->addWidget(_actuator2_abs_groupbox, 1, 0);
    actuator2_layout->addWidget(_mirror2_groupbox, 2, 0);
    _actuator2_groupbox->setLayout(actuator2_layout);

    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Set up movements for all actuators at the same time";
    QGroupBox *_actuator_all_groupbox = new QGroupBox("All actuator M4");
    QHBoxLayout *actuator_all_layout = new QHBoxLayout;

    QGroupBox *_actuators_relat_groupbox = new QGroupBox("Relative commands");
    QHBoxLayout *actuators_relat_layout = new QHBoxLayout;
    _forward_actuators_relat_M4 = new QPushButton("FORWARD");
    actuators_relat_layout->addWidget(_forward_actuators_relat_M4);
    _backward_actuators_relat_M4 = new QPushButton("BACKWARD");
    actuators_relat_layout->addWidget(_backward_actuators_relat_M4);
    _stop_actuators_relat_M4 = new QPushButton("STOP");
    _stop_actuators_relat_M4->setToolTip("Emergency Stop parking");
    actuators_relat_layout->addWidget(_stop_actuators_relat_M4);
    _actuators_relat_groupbox->setLayout(actuators_relat_layout);

    actuator_all_layout->addWidget(_actuators_relat_groupbox);
    _actuator_all_groupbox->setLayout(actuator_all_layout);

    QGroupBox *_actuator_all_move_groupbox = new QGroupBox("Control JOG");
    QHBoxLayout *actuator_all_move_layout = new QHBoxLayout;

    QGroupBox *_actuator_type_groupbox = new QGroupBox("Commands Type");
    QHBoxLayout *actuator_type_layout = new QHBoxLayout;
    _JOG_actuator_M4_M5 = new QRadioButton("JOG");
    _JOG_actuator_M4_M5->setChecked(true);
    actuator_type_layout->addWidget(_JOG_actuator_M4_M5);
    _velocity_actuator_M4_M5 = new QRadioButton("speed");
    actuator_type_layout->addWidget(_velocity_actuator_M4_M5);
    _actuator_type_groupbox->setLayout(actuator_type_layout);

    QGroupBox *_actuator_value_groupbox = new QGroupBox("Commands Value");
    QHBoxLayout *actuator_value_layout = new QHBoxLayout;
    _milli_hand_control_M4_M5 = new QDoubleSpinBox;
    _milli_hand_control_M4_M5->setRange(0, 1000);
    _milli_hand_control_M4_M5->setSingleStep(1);
    _milli_hand_control_M4_M5->setValue(0);
    _milli_hand_control_M4_M5->setToolTip("Between 0 and 1000mm");
    actuator_value_layout->addWidget(_milli_hand_control_M4_M5);
    _milli_label_M4_M5 = new QLabel("mm");
    actuator_value_layout->addWidget(_milli_label_M4_M5);

    _micro_hand_control_M4_M5 = new QDoubleSpinBox;
    _micro_hand_control_M4_M5->setRange(0, 999);
    _micro_hand_control_M4_M5->setSingleStep(1);
    _micro_hand_control_M4_M5->setValue(0);
    _micro_hand_control_M4_M5->setToolTip("Between 0 and 1000mm");
    actuator_value_layout->addWidget(_micro_hand_control_M4_M5);
    _micro_label_M4_M5 = new QLabel("µm");
    actuator_value_layout->addWidget(_micro_label_M4_M5);
    _actuator_value_groupbox->setLayout(actuator_value_layout);

    actuator_all_move_layout->addWidget(_actuator_type_groupbox);
    actuator_all_move_layout->addWidget(_actuator_value_groupbox);
    _actuator_all_move_groupbox->setLayout(actuator_all_move_layout);

    actuators_layout->addWidget(_actuator1_groupbox, 0, 0);
    actuators_layout->addWidget(_actuator2_groupbox, 0, 1);
    actuators_layout->addWidget(_actuator_all_groupbox, 1, 0);
    actuators_layout->addWidget(_actuator_all_move_groupbox, 1, 1);
    _actuators_groupbox->setLayout(actuators_layout);

    QObject::connect(_stop_actuators_relat_M4, SIGNAL(clicked()),
                     this, SLOT(stop_button_actuator_slot_M4_M5()));

    return _actuators_groupbox;
}

void View::stop_button_actuator_slot_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Stop button M4 M5 clicked";
    QPalette pal = _stop_actuators_relat_M4->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_actuators_relat_M4->setAutoFillBackground(true);
    _stop_actuators_relat_M4->setPalette(pal);
    _stop_actuators_relat_M4->update();

    _string_logging_actuator_M4_M5 = _string_logging_actuator_M4_M5 +
            "M4 M5 hand control STOP button clicked\n";
    _logging_label_actuator_M4_M5->setText(_string_logging_actuator_M4_M5);
    _logging_scrollArea_actuator_M4_M5->verticalScrollBar()->setSliderPosition(
                _logging_label_actuator_M4_M5->height());
}

void View::actuator_buttons_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add widgets into the M4 M5 layout";
    _actuator_layout_M4_M5 = new QGridLayout();

    _actuator_layout_M4_M5->addWidget(show_logging_actuator_M4_M5(), 2, 1);
    _actuator_layout_M4_M5->addWidget(home_actuator_M4_M5(), 1, 0);
    _actuator_layout_M4_M5->addWidget(park_actuator_M4_M5(), 0, 1);
    _actuator_layout_M4_M5->addWidget(hand_control_actuator_M4_M5(), 2, 0);
    _actuator_layout_M4_M5->addWidget(actuator_infos_M4_M5(), 1, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Insert the M4 M5 layout into the map with associated level";
    _map_widgets_actuator_M4_M5.insert(make_pair(_actuator_layout_M4_M5,
                                                 USER_LEVEL));
}

void View::verify_user_level_actuator_M4_M5()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the M4 M5 layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it =
        _map_widgets_actuator_M4_M5.begin();
        it != _map_widgets_actuator_M4_M5.end(); ++it)
    {
        if(it->second <= 0)
            _actuators_M4_M5_tab->setLayout(it->first);
    }
}

////////////////////////CAMERA FUNCTIONS//////////////////////////////

QGroupBox* View::show_logging_camera()
{
    _string_logging_camera = "Camera logging !!!!\n";
    _logging_label_camera = new QLabel(_string_logging_camera, this);

    QGroupBox *_logging_groupbox = new QGroupBox("Logging");
    QVBoxLayout *logging_layout = new QVBoxLayout;

    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up logging camera label scrollbar";
    _logging_scrollArea_camera = new QScrollArea(this);
    _logging_scrollArea_camera->setVerticalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_camera->setHorizontalScrollBarPolicy(
                Qt::ScrollBarAlwaysOn);
    _logging_scrollArea_camera->setWidget(_logging_label_camera);
    _logging_scrollArea_camera->setWidgetResizable(true);
    _logging_scrollArea_camera->setBackgroundRole(QPalette::Light);
    logging_layout->addWidget(_logging_scrollArea_camera);
    _logging_groupbox->setLayout(logging_layout);

    return _logging_groupbox;
}

QGroupBox* View::connection_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up connection camera group box";

    QGroupBox *_camera_groupbox = new QGroupBox("Connection Camera");
    QHBoxLayout *camera_layout = new QHBoxLayout;
    _connect_camera = new QPushButton("Connect");
    camera_layout->addWidget(_connect_camera);
    _disconnect_camera = new QPushButton("Disconnect");
    camera_layout->addWidget(_disconnect_camera);
    _camera_groupbox->setLayout(camera_layout);

    QObject::connect(_connect_camera, SIGNAL(clicked()),
                this, SLOT(connect_camera_slot()));

    return _camera_groupbox;
}

void View::connect_camera_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Connect camera button clicked";

    QPalette pal = _connect_camera->palette();
    pal.setColor(QPalette::Button, QColor(Qt::green));
    _connect_camera->setAutoFillBackground(true);
    _connect_camera->setPalette(pal);
    _connect_camera->update();

    _string_logging_camera = _string_logging_camera +
            "Connect camera button clicked\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());
}

QGroupBox* View::exposure_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up exposure camera group box";

    QGroupBox *_camera_groupbox = new QGroupBox("Camera Exposure");
    QGridLayout *camera_layout = new QGridLayout;

    _s_exposure = new QLabel("Exposure (s)");
    camera_layout->addWidget(_s_exposure, 0, 0);
    _s_exposure_box = new QSpinBox;
    _s_exposure_box->setRange(0, 100);
    _s_exposure_box->setSingleStep(1);
    _s_exposure_box->setValue(0);
    _s_exposure_box->setToolTip("Between 0 and 100s");
    camera_layout->addWidget(_s_exposure_box, 0, 1);

    _ms_exposure = new QLabel("Exposure (ms)");
    camera_layout->addWidget(_ms_exposure, 1, 0);
    _ms_exposure_box = new QSpinBox;
    _ms_exposure_box->setRange(0, 1000);
    _ms_exposure_box->setSingleStep(1);
    _ms_exposure_box->setValue(0);
    _ms_exposure_box->setToolTip("Between 0 and 1000ms");
    camera_layout->addWidget(_ms_exposure_box, 1, 1);

    _set_exposure_camera = new QPushButton("SET");
    camera_layout->addWidget(_set_exposure_camera, 2, 0);
    _stop_exposure_camera = new QPushButton("STOP");
    _stop_exposure_camera->setToolTip("Emergency Stop Exposure");
    camera_layout->addWidget(_stop_exposure_camera, 2, 1);
    _camera_groupbox->setLayout(camera_layout);

    QObject::connect(_stop_exposure_camera, SIGNAL(clicked()),
                     this, SLOT(stop_exposure_camera()));

    return _camera_groupbox;
}

void View::stop_exposure_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Stop button exposure camera clicked";
    QPalette pal = _stop_exposure_camera->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_exposure_camera->setAutoFillBackground(true);
    _stop_exposure_camera->setPalette(pal);
    _stop_exposure_camera->update();

    _string_logging_camera = _string_logging_camera +
            "Exposure camera STOP button clicked\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());
}

QGroupBox* View::video_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up video camera group box";

    QGroupBox *_camera_groupbox = new QGroupBox("Camera Video");
    QGridLayout *camera_layout = new QGridLayout;

    _fps_video = new QLabel("FPS");
    camera_layout->addWidget(_fps_video, 0, 0);
    _fps_video_box = new QSpinBox;
    _fps_video_box->setRange(0, 100);
    _fps_video_box->setSingleStep(5);
    _fps_video_box->setValue(30);
    _fps_video_box->setToolTip("Between 0 and 100 FPS");
    camera_layout->addWidget(_fps_video_box, 0, 1);

    _on_video = new QPushButton("ON");
    camera_layout->addWidget(_on_video, 1, 0);
    _off_video = new QPushButton("OFF");
    camera_layout->addWidget(_off_video, 1, 1);
    _stop_video = new QPushButton("STOP");
    _stop_video->setToolTip("Emergency Stop Video");
    camera_layout->addWidget(_stop_video, 1, 2);

    _on_record_video = new QPushButton("Recording ON");
    camera_layout->addWidget(_on_record_video, 2, 0);
    _off_record_video = new QPushButton("Recording OFF");
    camera_layout->addWidget(_off_record_video, 2, 1);
    _file_record_button = new QPushButton("File recording");
    camera_layout->addWidget(_file_record_button, 2, 2);
    _camera_groupbox->setLayout(camera_layout);

    QObject::connect(_file_record_button, SIGNAL(clicked()),
                     this, SLOT(file_recording()));
    QObject::connect(_stop_video, SIGNAL(clicked()),
                     this, SLOT(stop_video_camera()));
    QObject::connect(_on_video, SIGNAL(clicked()),
                this, SLOT(on_video_slot()));

    return _camera_groupbox;
}

void View::file_recording()
{
    file_record_video = QFileDialog::getOpenFileName(this, "File recording",
                                    "../", "File (*.txt)");
    PLOG_INFO_IF(LOGGING_INFO == 1) << "File recording selected: "
                                    << file_record_video;

    _string_logging_camera = _string_logging_camera +
            "File recording selected: " + file_record_video;
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());
}

void View::on_video_slot()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "ON video camera button clicked";

    QPalette pal = _on_video->palette();
    pal.setColor(QPalette::Button, QColor(Qt::green));
    _on_video->setAutoFillBackground(true);
    _on_video->setPalette(pal);
    _on_video->update();

    _string_logging_camera = _string_logging_camera +
            "ON video camera button clicked\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());
}

void View::stop_video_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Stop button video camera clicked";
    QPalette pal = _stop_video->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    _stop_video->setAutoFillBackground(true);
    _stop_video->setPalette(pal);
    _stop_video->update();

    _string_logging_camera = _string_logging_camera +
            "Video camera STOP button clicked\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());
}

QGroupBox* View::camera_infos()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up camera infos group box";

    QGroupBox *_camera_groupbox = new QGroupBox("Camera infos");
    QVBoxLayout *camera_layout = new QVBoxLayout;

    _camera_name = new QLabel("Camera name: ZWO ASI");
    camera_layout->addWidget(_camera_name);
    _camera_version = new QLabel("Camera version: 0");
    camera_layout->addWidget(_camera_version);
    _camera_size_x = new QLabel("Camera size X: 0mm");
    camera_layout->addWidget(_camera_size_x);
    _camera_size_y = new QLabel("Camera size Y: 0mm");
    camera_layout->addWidget(_camera_size_y);
    _camera_min_exposure = new QLabel("Min exposure time: 0s");
    camera_layout->addWidget(_camera_min_exposure);
    _camera_max_exposure = new QLabel("Max exposure time: 0s");
    camera_layout->addWidget(_camera_max_exposure);

    _camera_pixel_x = new QLabel("Pixel size X: 0mm");
    camera_layout->addWidget(_camera_pixel_x);
    _camera_pixel_y = new QLabel("Pixel size Y: 0mm");
    camera_layout->addWidget(_camera_pixel_y);
    _camera_binning_x = new QLabel("Binning X: 0");
    camera_layout->addWidget(_camera_binning_x);
    _camera_binning_y = new QLabel("Binning Y: 0");
    camera_layout->addWidget(_camera_binning_y);
    _camera_temperature = new QLabel("Temperature: 0°C");
    camera_layout->addWidget(_camera_temperature);
    _camera_groupbox->setLayout(camera_layout);

    return _camera_groupbox;
}

pair<QGroupBox*,int> View::config_admin_camera()
{
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Set camera config for admin user level group box";

    QGroupBox *_config_groupbox = new QGroupBox("Config");
    QHBoxLayout *config_layout = new QHBoxLayout;

    _config_camera = new QPushButton("Config commands");
    config_layout->addWidget(_config_camera);
    _show_graph_camera = new QPushButton("Show graph");
    config_layout->addWidget(_show_graph_camera);
    _config_groupbox->setLayout(config_layout);

    QObject::connect(_config_camera, SIGNAL(clicked()),
                     this, SLOT(config_command_camera()));

    return make_pair(_config_groupbox, USER_LEVEL);
}

QGroupBox* View::filterwheel_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up camera filterwheel group box";

    QGroupBox *_filterwheel_groupbox = new QGroupBox("Camera Filterwheel");
    QGridLayout *filterwheel_layout = new QGridLayout;

    _filterwheel_label = new QLabel("Filterwheel position");
    filterwheel_layout->addWidget(_filterwheel_label, 0, 0);
    _filterwheel_box = new QSpinBox;
    _filterwheel_box->setRange(1, 7);
    _filterwheel_box->setSingleStep(1);
    _filterwheel_box->setValue(1);
    _filterwheel_box->setToolTip("Between 0 and 7");
    filterwheel_layout->addWidget(_filterwheel_box, 0, 1);

    _set_filterwheel_camera = new QPushButton("SET");
    filterwheel_layout->addWidget(_set_filterwheel_camera, 1, 0);
    _reset_filterwheel_camera = new QPushButton("RESET");
    filterwheel_layout->addWidget(_reset_filterwheel_camera, 1, 1);
    _filterwheel_groupbox->setLayout(filterwheel_layout);

    QObject::connect(_set_filterwheel_camera, SIGNAL(clicked()),
        this, SLOT(update_filterwheel_camera()));
    QObject::connect(_reset_filterwheel_camera, SIGNAL(clicked()),
        this, SLOT(reset_filterwheel_camera()));

    return _filterwheel_groupbox;
}

void View::update_filterwheel_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update filterwheel position";
}

void View::reset_filterwheel_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset filterwheel position";
    _filterwheel_box->setValue(1);
}

QGroupBox* View::target_temp_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Set up camera target temperature group box";

    QGroupBox *_target_temp_groupbox = new QGroupBox("Target temperature");
    QGridLayout *target_temp_layout = new QGridLayout;

    _target_temp_label = new QLabel("Target temperature");
    target_temp_layout->addWidget(_target_temp_label, 0, 0);
    _target_temp_box = new QSpinBox;
    _target_temp_box->setRange(5, 30);
    _target_temp_box->setSingleStep(1);
    _target_temp_box->setValue(20);
    _target_temp_box->setToolTip("Between 5 and 30");
    target_temp_layout->addWidget(_target_temp_box, 0, 1);

    _set_target_temp_camera = new QPushButton("SET");
    target_temp_layout->addWidget(_set_target_temp_camera, 1, 0);
    _reset_target_temp_camera = new QPushButton("RESET");
    target_temp_layout->addWidget(_reset_target_temp_camera, 1, 1);
    _target_temp_groupbox->setLayout(target_temp_layout);

    QObject::connect(_set_target_temp_camera, SIGNAL(clicked()),
        this, SLOT(update_target_temp_camera()));
    QObject::connect(_reset_target_temp_camera, SIGNAL(clicked()),
        this, SLOT(reset_target_temp_camera()));

    return _target_temp_groupbox;
}

void View::update_target_temp_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update target temperature position";
}

void View::reset_target_temp_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset target temperature position";
    _target_temp_box->setValue(20);
}

void View::camera_buttons()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Add widgets into the camera layout";
    _camera_layout = new QGridLayout();
    _camera_layout->setColumnMinimumWidth(1, 500);

    _camera_layout->addWidget(show_logging_camera(), 3, 0);
    _camera_layout->addWidget(camera_infos(), 3, 1);
    _camera_layout->addWidget(connection_camera(), 0, 0);
    _camera_layout->addWidget(exposure_camera(), 1, 0);
    _camera_layout->addWidget(filterwheel_camera(), 2, 1);
    _camera_layout->addWidget(target_temp_camera(), 2, 0);
    _camera_layout->addWidget(video_camera(), 1, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Verify user level to display config camera groupbox";
    if(config_admin_camera().second <= 0)
        _camera_layout->addWidget(config_admin_camera().first, 0, 1);

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1)
            << "Insert the camera layout into the map with associated level";
    _map_widgets_camera.insert(make_pair(_camera_layout, USER_LEVEL));
}

void View::verify_user_level_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Display the camera layout only if user level allows it";
    for(map<QGridLayout*,int>::const_iterator it=_map_widgets_camera.begin();
        it != _map_widgets_camera.end(); ++it)
    {
        if(it->second <= 0)
            _camera_tab->setLayout(it->first);
    }
}

/////////////////////////CONFIG CAMERA FUNCTIONS////////////////////

QGroupBox* View::controls_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up camera controls group box";

    QGroupBox *_control_groupbox = new QGroupBox("Camera Controls");
    QGridLayout *control_layout = new QGridLayout;

    _gain_camera = new QLabel("Gain Camera");
    control_layout->addWidget(_gain_camera, 0, 0);
    _gain_camera_spinbox = new QSpinBox;
    _gain_camera_spinbox->setRange(0, 100);
    _gain_camera_spinbox->setSingleStep(1);
    _gain_camera_spinbox->setValue(50);
    _gain_camera_spinbox->setToolTip("Between 0 and 100");
    control_layout->addWidget(_gain_camera_spinbox, 0, 1);

    _bandwidth_camera = new QLabel("Bandwidth Camera");
    control_layout->addWidget(_bandwidth_camera, 1, 0);
    _bandwidth_camera_spinbox = new QSpinBox;
    _bandwidth_camera_spinbox->setRange(-100, 100);
    _bandwidth_camera_spinbox->setSingleStep(1);
    _bandwidth_camera_spinbox->setValue(50);
    _bandwidth_camera_spinbox->setToolTip("Between -100 and 100");
    control_layout->addWidget(_bandwidth_camera_spinbox, 1, 1);

    _format_camera = new QLabel("Format Camera");
    control_layout->addWidget(_format_camera, 2, 0);
    _format_camera_box = new QComboBox();
    _format_camera_box->addItem("8");
    _format_camera_box->addItem("16");
    _format_camera_box->setToolTip("Format 8 or 16 bits");
    control_layout->addWidget(_format_camera_box, 2, 1);

    _cooler_camera = new QLabel("Cooler Camera");
    control_layout->addWidget(_cooler_camera, 3, 0);
    _cooler_camera_box = new QComboBox();
    _cooler_camera_box->addItem("ON");
    _cooler_camera_box->addItem("OFF");
    _cooler_camera_box->setToolTip("Cooler ON/OFF");
    control_layout->addWidget(_cooler_camera_box, 3, 1);

    _set_control_camera = new QPushButton("SET");
    control_layout->addWidget(_set_control_camera, 4, 0);
    _reset_control_camera = new QPushButton("RESET");
    control_layout->addWidget(_reset_control_camera, 4, 1);
    _control_groupbox->setLayout(control_layout);

    QObject::connect(_set_control_camera, SIGNAL(clicked()),
        this, SLOT(update_control_camera()));
    QObject::connect(_reset_control_camera, SIGNAL(clicked()),
        this, SLOT(reset_control_camera()));

    return _control_groupbox;
}

void View::update_control_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update control in config window";
    _list_config_camera.find(0)->second =
            to_string(_gain_camera_spinbox->value());
    _list_config_camera.find(1)->second =
            to_string(_bandwidth_camera_spinbox->value());
    _list_config_camera.find(2)->second =
            _format_camera_box->currentText().toStdString();
    _list_config_camera.find(3)->second =
            _cooler_camera_box->currentText().toStdString();
    write_file_config_camera();
    update_infos_config_camera();
}

void View::reset_control_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset control in config window";
    _list_config_camera.find(0)->second = to_string(0);
    _list_config_camera.find(1)->second = to_string(0);
    _list_config_camera.find(2)->second = to_string(8);
    _list_config_camera.find(3)->second = "ON";
    _gain_camera_spinbox->setValue(0);
    _bandwidth_camera_spinbox->setValue(0);
    _format_camera_box->setCurrentText("8");
    _cooler_camera_box->setCurrentText("ON");
    write_file_config_camera();
    update_infos_config_camera();
}

QGroupBox* View::frame_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up camera frame group box";

    QGroupBox *_frame_groupbox = new QGroupBox("Camera Frames");
    QGridLayout *frame_layout = new QGridLayout;

    _top_frame = new QLabel("Top Frame");
    frame_layout->addWidget(_top_frame, 0, 0);
    _top_frame_spinbox = new QSpinBox;
    _top_frame_spinbox->setRange(0, 100);
    _top_frame_spinbox->setSingleStep(1);
    _top_frame_spinbox->setValue(50);
    _top_frame_spinbox->setToolTip("Between 0 and 100");
    frame_layout->addWidget(_top_frame_spinbox, 0, 1);

    _left_frame = new QLabel("Left Frame");
    frame_layout->addWidget(_left_frame, 1, 0);
    _left_frame_spinbox = new QSpinBox;
    _left_frame_spinbox->setRange(0, 100);
    _left_frame_spinbox->setSingleStep(1);
    _left_frame_spinbox->setValue(50);
    _left_frame_spinbox->setToolTip("Between 0 and 100");
    frame_layout->addWidget(_left_frame_spinbox, 1, 1);

    _height_frame = new QLabel("Height Frame");
    frame_layout->addWidget(_height_frame, 2, 0);
    _height_frame_spinbox = new QSpinBox;
    _height_frame_spinbox->setRange(0, 100);
    _height_frame_spinbox->setSingleStep(1);
    _height_frame_spinbox->setValue(50);
    _height_frame_spinbox->setToolTip("Between 0 and 100");
    frame_layout->addWidget(_height_frame_spinbox, 2, 1);

    _width_frame = new QLabel("Width Frame");
    frame_layout->addWidget(_width_frame, 3, 0);
    _width_frame_spinbox = new QSpinBox;
    _width_frame_spinbox->setRange(0, 100);
    _width_frame_spinbox->setSingleStep(1);
    _width_frame_spinbox->setValue(50);
    _width_frame_spinbox->setToolTip("Between 0 and 100");
    frame_layout->addWidget(_width_frame_spinbox, 3, 1);

    _type_frame = new QLabel("Type Frame");
    frame_layout->addWidget(_type_frame, 4, 0);
    _type_frame_box = new QComboBox();
    _type_frame_box->addItem("Light");
    _type_frame_box->addItem("Dark");
    _type_frame_box->addItem("Bias");
    _type_frame_box->addItem("Flat");
    frame_layout->addWidget(_type_frame_box, 4, 1);

    _set_frame_camera = new QPushButton("SET");
    frame_layout->addWidget(_set_frame_camera, 5, 0);
    _reset_frame_camera = new QPushButton("RESET");
    frame_layout->addWidget(_reset_frame_camera, 5, 1);
    _frame_groupbox->setLayout(frame_layout);

    QObject::connect(_set_frame_camera, SIGNAL(clicked()),
        this, SLOT(update_frame_camera()));
    QObject::connect(_reset_frame_camera, SIGNAL(clicked()),
        this, SLOT(reset_frame_camera()));

    return _frame_groupbox;
}

void View::update_frame_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update frame in config window";
    _list_config_camera.find(4)->second =
            to_string(_top_frame_spinbox->value());
    _list_config_camera.find(5)->second =
            to_string(_left_frame_spinbox->value());
    _list_config_camera.find(6)->second =
            to_string(_height_frame_spinbox->value());
    _list_config_camera.find(7)->second =
            to_string(_width_frame_spinbox->value());
    _list_config_camera.find(8)->second =
            _type_frame_box->currentText().toStdString();
    write_file_config_camera();
    update_infos_config_camera();
}

void View::reset_frame_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset frame in config window";
    _list_config_camera.find(4)->second = to_string(0);
    _list_config_camera.find(5)->second = to_string(0);
    _list_config_camera.find(6)->second = to_string(100);
    _list_config_camera.find(7)->second = to_string(100);
    _list_config_camera.find(8)->second = "Light";
    _top_frame_spinbox->setValue(0);
    _left_frame_spinbox->setValue(0);
    _height_frame_spinbox->setValue(100);
    _width_frame_spinbox->setValue(100);
    _type_frame_box->setCurrentText("Light");
    write_file_config_camera();
    update_infos_config_camera();
}

QGroupBox* View::binning_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up camera binning group box";

    QGroupBox *_binning_groupbox = new QGroupBox("Camera Binning");
    QGridLayout *binning_layout = new QGridLayout;

    _x_binning = new QLabel("Binning X");
    binning_layout->addWidget(_x_binning, 0, 0);
    _x_binning_box = new QSpinBox;
    _x_binning_box->setRange(1, 10);
    _x_binning_box->setSingleStep(1);
    _x_binning_box->setValue(1);
    _x_binning_box->setToolTip("Between 0 and 10");
    binning_layout->addWidget(_x_binning_box, 0, 1);

    _y_binning = new QLabel("Binning Y");
    binning_layout->addWidget(_y_binning, 1, 0);
    _y_binning_box = new QSpinBox;
    _y_binning_box->setRange(1, 10);
    _y_binning_box->setSingleStep(1);
    _y_binning_box->setValue(1);
    _y_binning_box->setToolTip("Between 0 and 10");
    binning_layout->addWidget(_y_binning_box, 1, 1);

    _set_binning_camera = new QPushButton("SET");
    binning_layout->addWidget(_set_binning_camera, 2, 0);
    _reset_binning_camera = new QPushButton("RESET");
    binning_layout->addWidget(_reset_binning_camera, 2, 1);
    _binning_groupbox->setLayout(binning_layout);

    QObject::connect(_set_binning_camera, SIGNAL(clicked()),
        this, SLOT(update_binning_camera()));
    QObject::connect(_reset_binning_camera, SIGNAL(clicked()),
        this, SLOT(reset_binning_camera()));

    return _binning_groupbox;
}

void View::update_binning_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Update binning in config window";
    _list_config_camera.find(9)->second = to_string(_x_binning_box->value());
    _list_config_camera.find(10)->second = to_string(_y_binning_box->value());
    write_file_config_camera();
    update_infos_config_camera();
}

void View::reset_binning_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Reset binning in config window";
    _list_config_camera.find(9)->second = to_string(1);
    _list_config_camera.find(10)->second = to_string(1);
    _x_binning_box->setValue(1);
    _y_binning_box->setValue(1);
    write_file_config_camera();
    update_infos_config_camera();
}

QGroupBox* View::infos_config_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) << "Set up config camera infos group box";

    QGroupBox *_camera_groupbox = new QGroupBox("");
    QVBoxLayout *camera_layout = new QVBoxLayout;

    _gain_label = new QLabel("Gain:");
    camera_layout->addWidget(_gain_label);
    _bandwidth_label = new QLabel("Bandwidth:");
    camera_layout->addWidget(_bandwidth_label);
    _format_label = new QLabel("Format:");
    camera_layout->addWidget(_format_label);
    _cooler_label = new QLabel("Cooler:");
    camera_layout->addWidget(_cooler_label);

    _top_frame_label = new QLabel("Top Frame:");
    camera_layout->addWidget(_top_frame_label);
    _left_frame_label = new QLabel("Left Frame:");
    camera_layout->addWidget(_left_frame_label);
    _height_frame_label = new QLabel("Height Frame:");
    camera_layout->addWidget(_height_frame_label);
    _width_frame_label = new QLabel("Width Frame:");
    camera_layout->addWidget(_width_frame_label);
    _type_frame_label = new QLabel("Type Frame:");
    camera_layout->addWidget(_type_frame_label);

    _x_binning_label = new QLabel("Binning X:");
    camera_layout->addWidget(_x_binning_label);
    _y_binning_label = new QLabel("Binning Y:");
    camera_layout->addWidget(_y_binning_label);
    _camera_groupbox->setLayout(camera_layout);

    return _camera_groupbox;
}

void View::update_infos_config_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1) <<
            "Update infos displayed in config camera window";

    _gain_label->setText(QString::fromStdString(
        "Gain: " + _list_config_camera.find(0)->second));
    _bandwidth_label->setText(QString::fromStdString(
        "Bandwidth: " + _list_config_camera.find(1)->second));
    _format_label->setText(QString::fromStdString(
        "Format: " + _list_config_camera.find(2)->second + "bits"));
    _cooler_label->setText(QString::fromStdString(
        "Cooler: " + _list_config_camera.find(3)->second));
    _top_frame_label->setText(QString::fromStdString(
        "Top Frame: " + _list_config_camera.find(4)->second));
    _left_frame_label->setText(QString::fromStdString(
        "Left Frame: " + _list_config_camera.find(5)->second));
    _height_frame_label->setText(QString::fromStdString(
        "Height Frame: " + _list_config_camera.find(6)->second));
    _width_frame_label->setText(QString::fromStdString(
        "Width Frame: " + _list_config_camera.find(7)->second));
    _type_frame_label->setText(QString::fromStdString(
        "Type Frame: " + _list_config_camera.find(8)->second));
    _x_binning_label->setText(QString::fromStdString(
        "Binning X: " + _list_config_camera.find(9)->second));
    _y_binning_label->setText(QString::fromStdString(
        "Binning Y: " + _list_config_camera.find(10)->second));
}

int View::read_file_config_camera()
{
    string text;
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Open the config camera file";
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_camera.txt");
    ifstream file(str1);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Camera file not found";
        return -1;
    }

    int i = 0;
    config_camera = "config_camera;11;";
    while(getline(file, text))
    {
        _list_config_camera.insert(make_pair(i, text));
        config_camera = config_camera + text + ";";
        i++;
    }

    if (i == 0) {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "Camera file is empty";
        return -1;
    }

    config_camera.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Camera serialized config file: "
                                      << config_camera;
    _string_logging_camera = _string_logging_camera +
            "Serialized config file: \""
            + QString::fromStdString(config_camera) + "\"\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close camera file";
    return i;
}

int View::write_file_config_camera()
{
    QByteArray ba = QCoreApplication::applicationDirPath().toLocal8Bit();
    char* str1 = ba.data();
    strcat(str1, "/../config_values_camera.txt");
    ofstream file;

    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) <<
            "Open and delete the file's camera content";
    file.open(str1, ios::out | ios::trunc);
    if(!file.is_open())
    {
        PLOG_ERROR_IF(LOGGING_ERROR == 1) << "File camera not found";
        return -1;
    }

    config_camera = "config_camera;11;";
    for(map<int,string>::const_iterator it =_list_config_camera.begin();
        it != _list_config_camera.end(); ++it)
    {
        file << it->second << "\n";
        config_camera = config_camera + it->second + ";";
    }

    config_camera.pop_back();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Camera serialized config file: "
                                      << config_camera;
    _string_logging_camera = _string_logging_camera +
            "Serialized config file: \""
            + QString::fromStdString(config_camera) + "\"\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());

    file.close();
    PLOG_DEBUG_IF(LOGGING_DEBUG == 1) << "Close camera file";
    return 0;
}

void View::config_command_camera()
{
    PLOG_INFO_IF(LOGGING_INFO == 1)
            << "Create new window for camera configurations";
    _string_logging_camera = _string_logging_camera +
            "config button clicked\n";
    _logging_label_camera->setText(_string_logging_camera);
    _logging_scrollArea_camera->verticalScrollBar()->setSliderPosition(
                _logging_label_camera->height());

    _new_window = new QWidget();
    _new_window->setWindowTitle("CAMERA CONFIG");
    _new_window->setFixedSize(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);

    QGroupBox *_config_groupbox = new QGroupBox("Config");
    QVBoxLayout *config_layout = new QVBoxLayout;

    config_layout->addWidget(controls_camera());
    config_layout->addWidget(frame_camera());
    config_layout->addWidget(binning_camera());
    _config_groupbox->setLayout(config_layout);

    QGroupBox *_display_groupbox = new QGroupBox("Actual data");
    QVBoxLayout *display_layout = new QVBoxLayout;

    display_layout->addWidget(infos_config_camera());
    _display_groupbox->setLayout(display_layout);

    _config_camera_layout = new QGridLayout();
    _config_camera_layout->addWidget(_config_groupbox, 0, 0);
    _config_camera_layout->addWidget(_display_groupbox, 0, 1);

    _new_window->setLayout(_config_camera_layout);
    read_file_config_camera();
    update_infos_config_camera();
    _new_window->show();
}
