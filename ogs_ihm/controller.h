#ifndef CONTROLLER_H
#define CONTROLLER_H

/*!
* \file controller.h
* \brief Controller of the MVC design pattern
* \author Maeva Manuel
* \version 0.1
*/
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "abstractController.h"

using namespace std;

/*! \class Controller
 *  \brief Class inheriting from AbstractController
 */
class Controller : public AbstractController
{
public:
    /*!
     *  \brief Constructor
     *  \param model : Model
     */
    Controller(Model model) : AbstractController(model)
    {
    };

private:
};

#endif // CONTROLLER_H
