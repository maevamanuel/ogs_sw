#ifndef MODEL_H
#define MODEL_H

/*!
 * \file model.h
 * \brief Model of the MVC design pattern
 * \author Maeva Manuel
 * \version 0.1
 */
#include "abstractModel.h"

/*! \class Model
 *  \brief Class inheriting from AbstractModel
 */
class Model : public AbstractModel
{
public:
    /*!
     *  \brief Constructor
     *  Constructor of the Model class
     */
    Model(){};
};

#endif // MODEL_H
