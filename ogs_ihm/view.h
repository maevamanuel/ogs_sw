﻿#ifndef VIEW_H
#define VIEW_H

/*!
 * \file view.h
 * \brief View of the OGS GUI
 * \author Maeva Manuel
 * \version 0.1
 */
#include <QWidget>
#include <QtWidgets>
#include <QtCharts>
#include <QMoveEvent>
#include <QPaintEvent>
#include <QTextToSpeech>

#include <map>
#include <math.h>
#include <string>
#include <stdio.h>
#include <cstring>
#include <sstream>
#include <utility>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <locale>
#include <codecvt>
#include <fcntl.h>
#include <termios.h>

#include <plog/Log.h>
#include <../third_parties/MQTTInterface/src/mqtt_interface.hpp>
#include "plog/Initializers/RollingFileInitializer.h"
#include "controller.h"

#define LOGGING_VERBOSE  0
#define LOGGING_DEBUG  1
#define LOGGING_INFO  1
#define LOGGING_WARNING  1
#define LOGGING_ERROR  1
#define LOGGING_FATAL  1

#define USER_LEVEL 0
#define STRING_LOG_LENGTH  4096
#define DISPLAY_GRAPH_SPEED 100
#define DISPLAY_GRAPH_WEATHER 5000
#define NB_POINTS_GRAPH 500

#define NEW_WINDOW_WIDTH 1600
#define NEW_WINDOW_HEIGHT 900
#define MAIN_WINDOW_WIDTH 1920
#define MAIN_WINDOW_HEIGHT 1080

#define POSITION_PIXEL_X0 500
#define POSITION_PIXEL_Y0 440
#define POSITION_PIXEL_XW 905
#define IMAGING_PIXEL_TOP_X 25
#define IMAGING_PIXEL_TOP_Y 127
#define CURSOR_PIXEL_TOP_X 95
#define CURSOR_PIXEL_TOP_Y 190

#define PI 3.141592653

using namespace std;

/*! \class View
 *  \brief Class representing the view of the MVC
 */
class View : public QWidget
{
    Q_OBJECT

public:
    /*!
     *  \brief Constructor of View class
     *  \param controller : Controller
     *  \param parent: QWidget * by default to nullptr
     */
    View(Controller controller, QWidget *parent = nullptr);
    /*!
     *  \brief Destructor of View class
     */
    ~View(){};

    //////////////////////GROUPBOX TELESCOPE///////////////////////////

    /*!
     *  \brief Set up logging widget in telescope tab
     *  \return QGroupBox *
     */
    QGroupBox* show_loggging();
    /*!
     *  \brief Set up date and time group box
     *  \return QGroupBox *
     */
    QGroupBox* date_time();
    /*!
     *  \brief Set up connection telescope/MQTT group box
     *  \return QGroupBox *
     */
    QGroupBox* connection_telescope();
    /*!
     *  \brief Set up telescope hand control group box
     *  \return QGroupBox *
     */
    QGroupBox* hand_control();
    /*!
     *  \brief Set up home telescope group box
     *  \return QGroupBox *
     */
    QGroupBox* home_telescope();
    /*!
     *  \brief Set up park telescope group box
     *  \return QGroupBox *
     */
    QGroupBox* park_telescope();
    /*!
     *  \brief Set up tracking group box
     *  \return QGroupBox *
     */
    QGroupBox* tracking();
    /*!
     *  \brief Set up controller infos group box
     *  \return QGroupBox *
     */
    QGroupBox* controller_infos();
    /*!
     *  \brief Set up telescope ALT/AZ command group box
     *  \return QGroupBox *
     */
    QGroupBox* alt_az_commands();
    /*!
     *  \brief Set up telescope RA/Dec command group box
     *  \return QGroupBox *
     */
    QGroupBox* ra_dec_commands();
    /*!
     *  \brief Set up telescope ALT limits group box
     *  \return QGroupBox *
     */
    QGroupBox* alt_limits();
    /*!
     *  \brief Set up telescope AZ limits group box
     *  \return QGroupBox *
     */
    QGroupBox* az_limits();
    /*!
     *  \brief Set up telescope current limits group box
     *  \return QGroupBox *
     */
    QGroupBox* current_limits();
    /*!
     *  \brief Set up telescope speed limits group box
     *  \return QGroupBox *
     */
    QGroupBox* speed_limits();
    /*!
     *  \brief Set up telescope velocity group box
     *  \return QGroupBox *
     */
    QGroupBox* velocity_config();
    /*!
     *  \brief Set up telescope acceleration group box
     *  \return QGroupBox *
     */
    QGroupBox* acceleration_config();
    /*!
     *  \brief Set up telescope geographical position group box
     *  \return QGroupBox *
     */
    QGroupBox* position_config();
    /*!
     *  \brief Set up telescope limits infos group box
     *  \return QGroupBox *
     */
    QGroupBox* limits_infos();
    /*!
     *  \brief Set up satellite trajectory group box
     *  \return QGroupBox *
     */
    QGroupBox* satellite_traj();
    /*!
     *  \brief Set up config/graph group box in function of user level access
     *  \return pair<QGroupBox*,int>
     */
    pair<QGroupBox*,int> config_admin();

    /////////////////////GROUPBOX DOME/////////////////////////

    /*!
     *  \brief Set up dome hand control group box
     *  \return QGroupBox *
     */
    QGroupBox* hand_control_dome();
    /*!
     *  \brief Set up connection dome group box
     *  \return QGroupBox *
     */
    QGroupBox* connection_dome();
    /*!
     *  \brief Set up home dome group box
     *  \return QGroupBox *
     */
    QGroupBox* home_dome();
    /*!
     *  \brief Set up logging widget in dome tab
     *  \return QGroupBox *
     */
    QGroupBox* show_loggging_dome();
    /*!
     *  \brief Set up park dome group box
     *  \return QGroupBox *
     */
    QGroupBox* park_dome();
    /*!
     *  \brief Set up ALT/AZ command dome group box
     *  \return QGroupBox *
     */
    QGroupBox* alt_az_commands_dome();
    /*!
     *  \brief Set up open close shutter group box
     *  \return QGroupBox *
     */
    QGroupBox* open_close_shutter();
    /*!
     *  \brief Set up dome infos group box
     *  \return QGroupBox *
     */
    QGroupBox* dome_infos();
    /*!
     *  \brief Set up AZ limits dome group box
     *  \return QGroupBox *
     */
    QGroupBox* az_limits_dome();
    /*!
     *  \brief Set up shutter limits dome group box
     *  \return QGroupBox *
     */
    QGroupBox* shutter_limits_dome();
    /*!
     *  \brief Set up speed limits group box
     *  \return QGroupBox *
     */
    QGroupBox* speed_limits_dome();
    /*!
     *  \brief Set up velocity dome group box
     *  \return QGroupBox *
     */
    QGroupBox* velocity_config_dome();
    /*!
     *  \brief Set up acceleration dome group box
     *  \return QGroupBox *
     */
    QGroupBox* acceleration_config_dome();
    /*!
     *  \brief Set up offset dome group box
     *  \return QGroupBox *
     */
    QGroupBox* offset_config_dome();
    /*!
     *  \brief Set up limits infos dome group box
     *  \return QGroupBox *
     */
    QGroupBox* limits_infos_dome();
    /*!
     *  \brief Set dome config for admin user level group box
     *  \return pair<QGroupBox*,int>
     */
    pair<QGroupBox*,int> config_admin_dome();

    /////////////////////GROUPBOX ACTUATOR/////////////////////////

    /*!
     *  \brief Set up home actuator group box
     *  \return QGroupBox *
     */
    QGroupBox* home_actuator();
    /*!
     *  \brief Set up logging widget in actuator M2 M3 tab
     *  \return QGroupBox *
     */
    QGroupBox* show_logging_actuator();
    /*!
     *  \brief Set up park actuator group box
     *  \return QGroupBox *
     */
    QGroupBox* park_actuator();
    /*!
     *  \brief Set up actuator infos group box
     *  \return QGroupBox *
     */
    QGroupBox* actuator_infos();
    /*!
     *  \brief Set up connection actuator group box
     *  \return QGroupBox *
     */
    QGroupBox* connection_actuator();
    /*!
     *  \brief Set up actuator hand control groupbox.
     *  Set up movements for all actuators at the same time
     *  \return QGroupBox *
     */
    QGroupBox* hand_control_actuator();
    /*!
     *  \brief Set up home M4 M5 group box
     *  \return QGroupBox *
     */
    QGroupBox* home_actuator_M4_M5();
    /*!
     *  \brief Set up logging widget in actuator M4 M5 tab
     *  \return QGroupBox *
     */
    QGroupBox* show_logging_actuator_M4_M5();
    /*!
     *  \brief Set up park M4 M5 group box
     *  \return QGroupBox *
     */
    QGroupBox* park_actuator_M4_M5();
    /*!
     *  \brief Set up M4 M5 infos group box
     *  \return QGroupBox *
     */
    QGroupBox* actuator_infos_M4_M5();
    /*!
     *  \brief Set up M4 M5 hand control group box.
     *  Set up movements for all actuators at the same time
     *  \return QGroupBox *
     */
    QGroupBox* hand_control_actuator_M4_M5();

    ///////////////////////GROUPBOX CAMERA/////////////////////////

    /*!
     *  \brief Set up logging widget in camera tab
     *  \return QGroupBox *
     */
    QGroupBox* show_logging_camera();
    /*!
     *  \brief Set up connection camera group box
     *  \return QGroupBox *
     */
    QGroupBox* connection_camera();
    /*!
     *  \brief Set up camera infos group box
     *  \return QGroupBox *
     */
    QGroupBox* camera_infos();
    /*!
     *  \brief Set up exposure camera group box
     *  \return QGroupBox *
     */
    QGroupBox* exposure_camera();
    /*!
     *  \brief Set up camera filterwheel group box
     *  \return QGroupBox *
     */
    QGroupBox* filterwheel_camera();
    /*!
     *  \brief Set up camera target temperature group box
     *  \return QGroupBox *
     */
    QGroupBox* target_temp_camera();
    /*!
     *  \brief Set up video camera group box
     *  \return QGroupBox *
     */
    QGroupBox* video_camera();
    /*!
     *  \brief Set up camera controls group box
     *  \return QGroupBox *
     */
    QGroupBox* controls_camera();
    /*!
     *  \brief Set up camera frame group box
     *  \return QGroupBox *
     */
    QGroupBox* frame_camera();
    /*!
     *  \brief Set up camera binning group box
     *  \return QGroupBox *
     */
    QGroupBox* binning_camera();
    /*!
     *  \brief Set up config camera infos group box
     *  \return QGroupBox *
     */
    QGroupBox* infos_config_camera();
    /*!
     *  \brief Set camera config for admin user level group box
     *  \return pair<QGroupBox*,int>
     */
    pair<QGroupBox*,int> config_admin_camera();

    /////////////////////GROUPBOX WEATHER/////////////////////////

    /*!
     *  \brief Set weather config for admin user level group box
     *  \return pair<QGroupBox*,int>
     */
    pair<QGroupBox*,int> buttons_weather();
    /*!
     *  \brief Set up weather infos group box
     *  \return QGroupBox *
     */
    QGroupBox* display_infos_weather();
    /*!
     *  \brief Set up logging widget in weather tab
     *  \return QGroupBox *
     */
    QGroupBox* show_log_weather();
    /*!
     *  \brief Set up connection weather group box
     *  \return QGroupBox *
     */
    QGroupBox* connection_weather();
    /*!
     *  \brief Set up wind thresholds group box
     *  \return QGroupBox *
     */
    QGroupBox* wind_limits();
    /*!
     *  \brief Set up rain thresholds group box
     *  \return QGroupBox *
     */
    QGroupBox* rain_limits();
    /*!
     *  \brief Set up hail thresholds group box
     *  \return QGroupBox *
     */
    QGroupBox* hail_limits();
    /*!
     *  \brief Set up voltage thresholds group box
     *  \return QGroupBox *
     */
    QGroupBox* volt_limits();
    /*!
     *  \brief Set up thresholds weather infos group box
     *  \return QGroupBox *
     */
    QGroupBox* thresholds_infos();

    /////////////////////GROUPBOX POSITION/////////////////////////

    /*!
     *  \brief Set up position telescope groupbox
     *  \return QGroupBox *
     */
    QGroupBox* display_position();

    /////////////////////GROUPBOX IMAGING/////////////////////////

    /*!
     *  \brief Set up imaging groupbox
     *  \return QGroupBox *
     */
    QGroupBox* display_imaging();

    ///////////////////////TELESCOPE FUNCTIONS//////////////////////

    /*!
     *  \brief Set up the main/equipment window tabs.
     *  Add icons in the main/equipment window tabs
     */
    void set_tabWidgets();
    /*!
     *  \brief Add widgets into the telescope layout.
     *  Verify user level to display config telescope groupbox
     */
    void telescope_buttons();
    /*!
     *  \brief Display the telescope layout only if user level allows it
     */
    void verify_user_level();
    /*!
     *  \brief Set up ALT error telescope graph
     */
    void fill_graph_alt();
    /*!
     *  \brief Set up AZ error telescope graph
     */
    void fill_graph_az();
    /*!
     *  \brief Set up AZ power telescope graph
     */
    void fill_graph_az_power();
    /*!
     *  \brief Set up ALT power telescope graph
     */
    void fill_graph_alt_power();
    /*!
     *  \brief Set up all telescope errors in a same graph
     */
    void fill_graph_all_errors();
    /*!
     *  \brief Set up the telescope graph tabs
     */
    void set_tabs_graph();
    /*!
     *  \brief Update ALT error telescope graph
     */
    void update_alt_errors();
    /*!
     *  \brief Update AZ error telescope graph
     */
    void update_az_errors();
    /*!
     *  \brief Update AZ power telescope graph
     */
    void update_az_power();
    /*!
     *  \brief Update ALT power telescope graph
     */
    void update_alt_power();
    /*!
     *  \brief Update infos displayed in telescope config window
     */
    void update_infos_config();
    /*!
     *  \brief Connection with telescope MQTT publish slots
     */
    void map_command_payload_telescope();
    /*!
     *  \brief Start MQTT subscribers
     */
    void mqtt_subs_connect();
    /*!
     *  \brief Update telescope Ra/Dec values displayed
     *  \param msg : msg_position_telescope
     */
    void update_ra_dec_display(msg_position_telescope msg);
    /*!
     *  \brief Update telescope ALT/AZ values displayed
     *  \param msg : msg_position_telescope
     */
    void update_alt_az_display(msg_position_telescope msg);
    /*!
     *  \brief Update telescope AZ/Dec brake displayed
     *  \param msg : msg_position_telescope
     */
    void update_brake_display(msg_position_telescope msg);
    /*!
     *  \brief Update telescope ALT/AZ power displayed
     *  \param msg : msg_position_telescope
     */
    void update_power_display(msg_position_telescope msg);
    /*!
     *  \brief Update telescope ALT/AZ errors displayed
     *  \param msg : msg_position_telescope
     */
    void update_error_display(msg_position_telescope msg);
    /*!
     *  \brief Read config telescope file and fill a config values list
     *  \return int: -1 error, number of lines read
     */
    int read_file_config();
    /*!
     *  \brief Write in config telescope file values contained in config list
     *  Firstly, delete the file's content, then fill it.
     *  \return int: -1 error, 0 success
     */
    int write_file_config();

    /////////////////////////DOME FUNCTIONS///////////////////////////

    /*!
     *  \brief Add widgets into the dome layout.
     *  Verify user level to display config dome groupbox
     */
    void dome_buttons();
    /*!
     *  \brief Display the dome grid layout only if user level allows it
     */
    void verify_user_level_dome();
    /*!
     *  \brief Update infos displayed in config dome window
     */
    void update_infos_config_dome();
    /*!
     *  \brief Connection with dome MQTT publish slots
     */
    void map_command_payload_dome();
    /*!
     *  \brief Read config dome file and fill a config values list
     *  \return int: -1 error, number of lines read
     */
    int read_file_config_dome();
    /*!
     *  \brief Write in config dome file values contained in config list
     *  Firstly, delete the file's content, then fill it.
     *  \return int: -1 error, 0 success
     */
    int write_file_config_dome();
    /*!
     *  \brief Update dome error AZ/shutter displayed
     *  \param msg : msg_position_telescope
     */
    void update_error_az_shut_display(msg_position_telescope msg);
    /*!
     *  \brief Update dome value AZ/shutter displayed
     *  \param msg : msg_position_telescope
     */
    void update_value_az_shut_display(msg_position_telescope msg);

    /////////////////////////ACTUATOR FUNCTIONS//////////////////////

    /*!
     *  \brief Add widgets into the M2 M3 actuators layout
     */
    void actuator_buttons();
    /*!
     *  \brief Display the M2 M3 layout only if user level allows it
     */
    void verify_user_level_actuator();
    /*!
     *  \brief Read backlash actuator file and fill a list
     *  \return int: -1 error, number of lines read
     */
    int read_file_backlash();
    /*!
     *  \brief Add widgets into the M4 M5 actuators layout
     */
    void actuator_buttons_M4_M5();
    /*!
     *  \brief Display the M4 M5 layout only if user level allows it
     */
    void verify_user_level_actuator_M4_M5();

    ///////////////////////////CAMERA FUNCTIONS//////////////////////

    /*!
     *  \brief Add widgets into the camera layout.
     *  Verify user level to display config camera groupbox
     */
    void camera_buttons();
    /*!
     *  \brief Display the camera layout only if user level allows it
     */
    void verify_user_level_camera();
    /*!
     *  \brief Read config camera file and fill a config values list
     *  \return int: -1 error, number of lines read
     */
    int read_file_config_camera();
    /*!
     *  \brief Write in config camera file values contained in config list
     *  Firstly, delete the file's content, then fill it.
     *  \return int: -1 error, 0 success
     */
    int write_file_config_camera();
    /*!
     *  \brief Update infos displayed in config camera window
     */
    void update_infos_config_camera();

    ////////////////////////WEATHER FUNCTIONS////////////////////////

    /*!
     *  \brief Store the data in variables and display them in IHM
     *  \param v : vector<string>
     *  \return int: -1 error, 0 success
     */
    int get_param_weather(vector<string> v);
    /*!
     *  \brief Write in config weather file values contained in config list
     *  Firstly, delete the file's content, then fill it.
     *  \return int: -1 error, 0 success
     */
    int write_file_config_weather();
    /*!
     *  \brief Read config weather file and fill a config values list
     *  \return int: -1 error, number of lines read
     */
    int read_file_config_weather();
    /*!
     *  \brief Update weather thresholds displayed in config window
     *  \return int: -1 error, 0 success
     */
    void update_thresholds_config();
    /*!
     *  \brief Add widgets into the weather layout.
     *  Verify user level to display config weather groupbox
     */
    void weather_buttons();
    /*!
     *  \brief Display the weather layout only if user level allows it
     */
    void verify_user_level_weather();
    /*!
     *  \brief Dew point calculation
     *  \param temp: float
     *  \param hum: float
     *  \return float
     */
    float dew_point_calculation(float temp, float hum);
    /*!
     *  \brief Set up wind direction weather graph
     */
    void fill_graph_wind_dir();
    /*!
     *  \brief Set up wind speed weather graph
     */
    void fill_graph_wind_speed();
    /*!
     *  \brief Set up temperature weather graph
     */
    void fill_graph_temp();
    /*!
     *  \brief Set up dew point weather graph
     */
    void fill_graph_dew_point();
    /*!
     *  \brief Set up humidity weather graph
     */
    void fill_graph_hum();
    /*!
     *  \brief Set up pressure weather graph
     */
    void fill_graph_pressure();
    /*!
     *  \brief Set up voltage weather graph
     */
    void fill_graph_voltage();
    /*!
     *  \brief Set up the graph tab weather
     */
    void set_tabs_weather_graph();
    /*!
     *  \brief Update wind direction info in graph weather
     */
    void update_wind_dir();
    /*!
     *  \brief Update wind speed info in graph weather
     */
    void update_wind_speed();
    /*!
     *  \brief Update temperature info in graph weather
     */
    void update_temp();
    /*!
     *  \brief Update dew point info in graph weather
     */
    void update_dew_point();
    /*!
     *  \brief Update humidity info in graph weather
     */
    void update_hum();
    /*!
     *  \brief Update pressure info in graph weather
     */
    void update_pressure();
    /*!
     *  \brief Update voltage info in graph weather
     */
    void update_voltage();

    ////////////////////////POSITION FUNCTIONS////////////////////////

    /*!
     *  \brief Draw circle in telescope position image
     *  \param xc: int
     *  \param yc: int
     *  \param x: int
     *  \param y: int
     */
    void draw_circle(int xc, int yc, int x, int y);
    /*!
     *  \brief Implement Bresenham algorithm to draw a circle
     *  \param xc: int
     *  \param yc: int
     *  \param r: int
     */
    void algo_bresenham(int xc, int yc, int r);
    /*!
     *  \brief Update position telescope in fonction of (xc,yc) coordinates
     *  \param xc: int
     *  \param yc: int
     */
    void update_position_telescope(int xc, int yc);
    /*!
     *  \brief Set up position telescope groupbox
     */
    void position_buttons();

    //////////////////////////IMAGING FUNCTIONS//////////////////////

    /*!
     *  \brief Draw rectangle in imaging tab
     */
    void draw_rect();
    /*!
     *  \brief Set up imaging groupbox
     */
    void imaging_buttons();
    /*!
     *  \brief Crop image with the rectangle drawn and save the new image
     *  \param fileName: const QString &
     *  \param fileFormat: const char *
     */
    void save_crop_image(const QString &fileName, const char *fileFormat);

public slots:

    /////////////////////////TELESCOPE SLOTS////////////////////////

    /*!
     *  \brief Create new window using 'new' button
     */
    void create_new_window();
    /*!
     *  \brief Update and show time in IHM when timer ends
     */
    void show_time();
    /*!
     *  \brief Get the cursor position in imaging tab
     */
    void pos_cursor();
    /*!
     *  \brief Create new window to show telescope graphes.
     *  Create a timer to update the graphes, y axis autoscale
     */
    void show_graph();
    /*!
     *  \brief Update all the graphes at the same time
     */
    void update_graph();
    /*!
     *  \brief Create new window for telescope configurations.
     *  Config button has been clicked.
     */
    void config_command_window();
    /*!
     *  \brief Call disconnect MQTT publish function
     */
    void mqtt_disconnect_publish();
    /*!
     *  \brief Call connect MQTT publish function
     */
    void mqtt_connect_publish();
    /*!
     *  \brief Publish payload and blink a button to indicate the delivery
     *  \param payload: const QString &
     */
    void mqtt_state_publish(const QString &payload);
    /*!
     *  \brief Update ALT limits in config telescope window
     */
    void update_limit_alt();
    /*!
     *  \brief Reset ALT limits in config telescope window
     */
    void reset_limit_alt();
    /*!
     *  \brief Update AZ limits in config telescope window
     */
    void update_limit_az();
    /*!
     *  \brief Reset AZ limits in config telescope window
     */
    void reset_limit_az();
    /*!
     *  \brief Update current limits in config telescope window
     */
    void update_current();
    /*!
     *  \brief Reset current limits in config telescope window
     */
    void reset_current();
    /*!
     *  \brief Update speed limits in config telescope window
     */
    void update_speed_limit();
    /*!
     *  \brief Reset speed limits in config telescope window
     */
    void reset_speed_limit();
    /*!
     *  \brief Update velocity limits in config telescope window
     */
    void update_velocity();
    /*!
     *  \brief Reset velocity limits in config telescope window
     */
    void reset_velocity();
    /*!
     *  \brief Update acceleration limits in config telescope window
     */
    void update_acceleration();
    /*!
     *  \brief Reset acceleration limits in config telescope window
     */
    void reset_acceleration();
    /*!
     *  \brief Update position limits in config telescope window
     */
    void update_position();
    /*!
     *  \brief Reset position limits in config telescope window
     */
    void reset_position();
    /*!
     *  \brief Hand control telescope STOP slot
     */
    void stop_button_slot();
    /*!
     *  \brief Tracking telescope STOP slot
     */
    void stop_tracking_slot();
    /*!
     *  \brief Connect telescope slot
     */
    void connect_telescope_slot();
    /*!
     *  \brief Homing telescope STOP slot
     */
    void stop_home_slot();
    /*!
     *  \brief Parking telescope STOP slot
     */
    void stop_park_slot();
    /*!
     *  \brief Ra/Dec telescope STOP slot
     */
    void stop_ra_dec_slot();
    /*!
     *  \brief ALT/AZ telescope STOP slot
     */
    void stop_alt_az_slot();
    /*!
     *  \brief Satellite telescope STOP slot
     */
    void stop_satellite_slot();
    /*!
     *  \brief Blinking publish slot
     */
    void blinking_slot_pub();
    /*!
     *  \brief Update goto RA/DEC payload
     */
    void goto_ra_dec_slot();
    /*!
     *  \brief Update sync RA/DEC payload
     */
    void sync_ra_dec_slot();
    /*!
     *  \brief Update goto ALT/AZ payload
     */
    void goto_alt_az_slot();
    /*!
     *  \brief Update sync ALT/AZ payload
     */
    void sync_alt_az_slot();
    /*!
     *  \brief Update park telescope payload
     */
    void park_telescope_slot();
    /*!
     *  \brief Update west big movement telescope payload
     */
    void west_big_slot();
    /*!
     *  \brief Update west small movement telescope payload
     */
    void west_small_slot();
    /*!
     *  \brief Update east big movement telescope payload
     */
    void east_big_slot();
    /*!
     *  \brief Update east small movement telescope payload
     */
    void east_small_slot();
    /*!
     *  \brief Update north big movement telescope payload
     */
    void north_big_slot();
    /*!
     *  \brief Update north small movement telescope payload
     */
    void north_small_slot();
    /*!
     *  \brief Update south big movement telescope payload
     */
    void south_big_slot();
    /*!
     *  \brief Update south small movement telescope payload
     */
    void south_small_slot();
    /*!
     *  \brief Display payload received
     */
    void mqtt_subscribe();
    /*!
     *  \brief Display payload received by error subscriber
     */
    void mqtt_subs_error();
    /*!
     *  \brief Display payload received by power subscriber
     */
    void mqtt_subs_power();
    /*!
     *  \brief Display payload received by brake subscriber
     */
    void mqtt_subs_brake();
    /*!
     *  \brief Display payload received by ALT/AZ subscriber
     */
    void mqtt_subs_alt_az();
    /*!
     *  \brief Display payload received by RA/Dec subscriber
     */
    void mqtt_subs_radec();

    ///////////////////////////////DOME SLOTS///////////////////////////

    /*!
     *  \brief Dome STOP slot
     */
    void stop_button_dome_slot();
    /*!
     *  \brief Connect dome slot
     */
    void connect_dome_slot();
    /*!
     *  \brief Homing dome STOP slot
     */
    void stop_home_dome_slot();
    /*!
     *  \brief Parking dome STOP slot
     */
    void stop_park_dome_slot();
    /*!
     *  \brief ALT/AZ dome STOP slot
     */
    void stop_alt_az_dome_slot();
    /*!
     *  \brief Shutter dome STOP slot
     */
    void stop_shutter_dome_slot();
    /*!
     *  \brief Create new window for dome commands configurations
     */
    void config_dome_slot();
    /*!
     * \brief Update AZ limits in config dome window
     */
    void update_limit_az_dome();
    /*!
     * \brief Reset AZ limits in config dome window
     */
    void reset_limit_az_dome();
    /*!
     * \brief Update shutter limits in config dome window
     */
    void update_limit_shutter();
    /*!
     * \brief Reset shutter limits in config dome window
     */
    void reset_limit_shutter();
    /*!
     * \brief Update speed limits in config dome window
     */
    void update_limit_speed_slot();
    /*!
     * \brief Reset speed limits in config dome window
     */
    void reset_limit_speed_slot();
    /*!
     * \brief Update velocity limits in config dome window
     */
    void update_velocity_dome();
    /*!
     * \brief Reset velocity limits in config dome window
     */
    void reset_velocity_dome();
    /*!
     * \brief Update acceleration limits in config dome window
     */
    void update_acceleration_dome();
    /*!
     * \brief Reset acceleration limits in config dome window
     */
    void reset_acceleration_dome();
    /*!
     * \brief Update offset limits in config dome window
     */
    void update_offset_dome();
    /*!
     * \brief Reset offset limits in config dome window
     */
    void reset_offset_dome();
    /*!
     *  \brief Update west big movement dome payload
     */
    void west_big_dome_slot();
    /*!
     *  \brief Update west small movement dome payload
     */
    void west_small_dome_slot();
    /*!
     *  \brief Update east big movement dome payload
     */
    void east_big_dome_slot();
    /*!
     *  \brief Update east small movement dome payload
     */
    void east_small_dome_slot();
    /*!
     *  \brief Update park dome payload
     */
    void park_dome_slot();
    /*!
     *  \brief Update goto AZ dome payload
     */
    void goto_az_dome_slot();
    /*!
     *  \brief Update sync AZ dome payload
     */
    void sync_az_dome_slot();
    /*!
     *  \brief Update speed AZ dome payload
     */
    void speed_az_dome_slot();
    /*!
     *  \brief Update open shutter dome payload
     */
    void open_shutter_dome_slot();
    /*!
     *  \brief Update close shutter dome payload
     */
    void close_shutter_dome_slot();
    /*!
     *  \brief Update sync shutter dome payload
     */
    void sync_shutter_dome_slot();
    /*!
     *  \brief Display dome payload received by error AZ shutter subscriber
     */
    void mqtt_subs_error_az_shut();
    /*!
     *  \brief Display dome payload received by value AZ shutter subscriber
     */
    void mqtt_subs_value_az_shut();

    ///////////////////////ACTUATOR SLOTS/////////////////////////

    /*!
     *  \brief Full home M2 M3 actuators slot
     */
    void full_home_M2M3_slot();
    /*!
     *  \brief Move relative forward actuators slot
     */
    void forward_relat_all_slot();
    /*!
     *  \brief Move absolute forward actuator 1 slot
     */
    void forward_abs_act1_slot();
    /*!
     *  \brief Actuator M2 M3 slot
     */
    void stop_button_actuator_slot();
    /*!
     *  \brief Connect actuator M2 M3 slot
     */
    void connect_actuator_slot();
    /*!
     *  \brief Homing actuator M2 M3 STOP slot
     */
    void stop_home_actuator_slot();
    /*!
     *  \brief Parking actuator M2 M3 slot
     */
    void park_actuator_slot();
    /*!
     *  \brief Parking actuator M2 M3 STOP slot
     */
    void stop_park_actuator_slot();
    /*!
     *  \brief Actuator M4 M5 slot
     */
    void stop_button_actuator_slot_M4_M5();
    /*!
     *  \brief Homing actuator M4 M5 STOP slot
     */
    void stop_home_actuator_slot_M4_M5();
    /*!
     *  \brief Parking actuator M4 M5 STOP slot
     */
    void stop_park_actuator_slot_M4_M5();

    /////////////////////////CAMERA SLOTS/////////////////////////

    /*!
     *  \brief Connect camera slot
     */
    void connect_camera_slot();
    /*!
     *  \brief Create new window for camera configurations
     */
    void config_command_camera();
    /*!
     *  \brief Update frame in config camera window
     */
    void update_frame_camera();
    /*!
     *  \brief Reset frame in config camera window
     */
    void reset_frame_camera();
    /*!
     *  \brief Update control in config camera window
     */
    void update_control_camera();
    /*!
     *  \brief Reset control in config camera window
     */
    void reset_control_camera();
    /*!
     *  \brief Update binning in config camera window
     */
    void update_binning_camera();
    /*!
     *  \brief Reset binning in config camera window
     */
    void reset_binning_camera();
    /*!
     *  \brief Update filterwheel position
     */
    void update_filterwheel_camera();
    /*!
     *  \brief Reset filterwheel position
     */
    void reset_filterwheel_camera();
    /*!
     *  \brief Update target temperature camera
     */
    void update_target_temp_camera();
    /*!
     *  \brief Reset target temperature camera
     */
    void reset_target_temp_camera();
    /*!
     *  \brief Exposure camera STOP slot
     */
    void stop_exposure_camera();
    /*!
     *  \brief Video camera STOP slot
     */
    void stop_video_camera();
    /*!
     *  \brief Video file recording slot
     */
    void file_recording();
    /*!
     *  \brief ON video camera clicked
     */
    void on_video_slot();

    /////////////////////////WEATHER SLOTS////////////////////////

    /*!
     *  \brief Create new window to show graph weather.
     *  Set up timer weather graph, Y axis autoscale.
     */
    void show_graph_weather();
    /*!
     *  \brief Update all the weather graphes at the same time
     */
    void update_graph_weather();
    /*!
     *  \brief Create new window for weather configurations.
     *  Config button has been clicked
     */
    void config_command_weather();
    /*!
     *  \brief Update wind threshold in config weather window
     */
    void update_threshold_wind();
    /*!
     *  \brief Reset wind threshold in config weather window
     */
    void reset_threshold_wind();
    /*!
     *  \brief Update rain threshold in config weather window
     */
    void update_threshold_rain();
    /*!
     *  \brief Reset rain threshold in config weather window
     */
    void reset_threshold_rain();
    /*!
     *  \brief Update hail threshold in config weather window
     */
    void update_threshold_hail();
    /*!
     *  \brief Reset hail threshold in config weather window
     */
    void reset_threshold_hail();
    /*!
     *  \brief Update voltage threshold in config weather window
     */
    void update_threshold_volt();
    /*!
     *  \brief Reset voltage threshold in config weather window
     */
    void reset_threshold_volt();
    /*!
     *  \brief Check weather data conditions by comparing with thresholds
     */
    void check_weather_conditions();
    /*!
     *  \brief Open Caussols meteoblue hyperlink
     */
    void open_meteoblue();

private:

    ////////////////////////TELESCOPE PAYLOADS SENT/////////////////////

    string telescope_home_full; /*!< Full home payload */
    string telescope_home_AZ; /*!< AZ home payload */
    string telescope_home_ALT; /*!< ALT home payload */
    string telescope_stop; /*!< Stop telescope payload */

    string telescope_handcontrol_west; /*!< West payload */
    string telescope_handcontrol_east; /*!< East payload */
    string telescope_handcontrol_north; /*!< North payload */
    string telescope_handcontrol_south; /*!< South payload */

    string telescope_tracking_sideral_velocity; /*!< Sideral velocity */
    string telescope_tracking_sun_velocity; /*!< Sun velocity payload */
    string telescope_tracking_moon_velocity; /*!< Moon velocity payload */

    string telescope_connect; /*!< Connect telescope payload */
    string telescope_disconnect; /*!< Disconnect telescope payload */
    string telescope_park; /*!< Park telescope payload */
    string telescope_unpark; /*!< Unpark telescope payload */

    string telescope_radec_goto; /*!< GOTO RA/Dec payload */
    string telescope_radec_sync; /*!< SYNC RA/Dec payload */
    string telescope_altaz_goto; /*!< GOTO ALT/AZ payload */
    string telescope_altaz_sync; /*!< SYNC ALT/AZ payload */

    string telescope_satellite_load_traj; /*!< Load trajectory payload */
    string telescope_satellite_follow_traj; /*!< Follow trajectory payload */

    string config_telescope; /*!< Serialized config telescope file */

    ///////////////////////////TELESCOPE WIDGETS///////////////////////////

    Controller _controller; /*!< Controller member */
    map<int,string> _list_config_value; /*!< Map (int, string) config values */
    QSignalMapper *_signal_mapper_mqtt; /*!< Signal mapper MQTT */
    QGridLayout *_telescope_layout; /*!< Buttons grid telescope layout */
    QGridLayout *_config_layout; /*!< Config grid layout */
    map<QGridLayout*,int> _map_widgets; /*!< Map of widgets (widget, level) */

    QTabWidget *_main_tabs; /*!< Tabs of the main window */
    QWidget *_equipement_tab; /*!< Equipment tab */
    QWidget *_sequence_tab; /*!< Sequence tab */
    QWidget *_imaging_tab; /*!< Imaging tab */
    QWidget *_options_tab; /*!< Options tab */
    QWidget *_help_tab; /*!< Help tab */

    QTabWidget *_equipment_tabs; /*!< Equipment tabs */
    QWidget *_camera_tab; /*!< Camera tab */
    QWidget *_actuator_tab; /*!< Actuators M2 M3 tab */
    QWidget *_actuators_M4_M5_tab; /*!< Actuators M4 M5 tab */
    QWidget *_telescope_tab; /*!< Telescope tab */
    QWidget *_dome_tab; /*!< Dome tab */
    QWidget *_weather_tab; /*!< Weather tab */
    QWidget *_position_tab; /*!< Position tab */

    QToolBar *_toolbar; /*!< Imaging toolbar */
    QAction *_quit; /*!< Quit action */
    QAction *_new; /*!< New action */

    QWidget *_camera_window; /*!< Camera window */
    QWidget *_new_window; /*!< New window after click button */
    QTimer *_timer; /*!< Timer to get the date and time updated */
    QLabel *_dateTimeUTC; /*!< Date and time UTC label */
    QLabel *_dateTime_local; /*!< Local date and time label */
    QLabel *_date; /*!< Date label member */

    QLabel *_logging_label; /*!< Logging label */
    QString _string_logging; /*!< String Logging */
    QScrollArea *_logging_scrollArea; /*!< Logging Scroll Area */

    QPushButton *_west_button_big; /*!< Big West button for telescope */
    QPushButton *_west_button_small; /*!< Small West button for telescope */
    QPushButton *_east_button_big; /*!< Big East button for telescope */
    QPushButton *_east_button_small; /*!< Small East button for telescope */

    QPushButton *_north_button_big; /*!< Big North button for telescope */
    QPushButton *_north_button_small; /*!< Small North button for telescope */
    QPushButton *_south_button_big; /*!< Big South button for telescope */
    QPushButton *_south_button_small; /*!< Small South button for telescope */
    QPushButton *_stop_button; /*!< Stop button for telescope */

    QDoubleSpinBox *_degree_hand_control; /*!< Degree spinbox member */
    QLabel *_degree_label; /*!< Degree label member */
    QDoubleSpinBox *_minute_hand_control; /*!< Minute spinbox member */
    QLabel *_minute_label; /*!< Minute label member */
    QDoubleSpinBox *_second_hand_control; /*!< Second spinbox member */
    QLabel *_second_label; /*!< Second label member */

    QRadioButton *_JOG_movement; /*!< JOG movement */
    QRadioButton *_velocity_movement; /*!< Velocity movement */
    QLabel *_ratio_label; /*!< Small big ratio movement member */
    QComboBox *_ratio_combobox; /*!< Ratio combobox member */

    QPushButton *_stop_tracking; /*!< Stop tracking */
    QPushButton *_sideral_velocity; /*!< Sideral velocity */
    QPushButton *_sun_velocity; /*!< Sun velocity */
    QPushButton *_moon_velocity; /*!< Moon velocity */
    QCheckBox *_sync_dome; /*!< Sync dome tracking checkboox */

    QPushButton *_connect_telescope; /*!< Connect telescope button */
    QPushButton *_disconnect_telescope; /*!< Disconnect telescope button */
    QPushButton *_connect_mqtt; /*!< Connect to MQTT server button */
    QPushButton *_disconnect_mqtt; /*!< Disconnect to MQTT button */
    QPushButton *_blinking_mqtt_pub; /*!< Blinking MQTT publish button */
    QTimer *_timer_blinking_pub; /*!< Timer publish blinking */

    bool mqtt_connected = false; /*!< MQTT connected boolean */
    string server_address = "tcp://localhost:1883"; /*!< Server address */
    string id_client_sub = "gui_sub"; /*!< ID client subscribe */
    string id_client_pub = "gui_pub"; /*!< ID client publish */
    string topic_gui_sub = "archi_pub"; /*!< Topic GUI subscribe */
    string topic_gui_pub = "gui_pub"; /*!< Topic GUI publish */
    QTimer *_timer_pub_sub; /*!< MQTT timer pubsub */
    MQTT_pubsub *_mqttinter; /*!< MQTT interface */

    MQTT_subscriber<msg_position_telescope> *_sub_radec; /*!< Ra/Dec subs */
    string client_sub_radec = "gui_sub_radec"; /*!< ID client subscribe */
    string topic_sub_radec = "topic_sub_radec"; /*!< Topic GUI subscribe */
    QTimer *_timer_radec; /*!< MQTT timer Ra/Dec */

    MQTT_subscriber<msg_position_telescope> *_sub_alt_az; /*!< ALT/AZ subs */
    string client_sub_alt_az = "gui_sub_alt_az"; /*!< ID client subscribe */
    string topic_sub_alt_az = "telescope_topic"; /*!< Topic GUI subscribe */
    QTimer *_timer_alt_az; /*!< MQTT timer ALT/AZ */

    MQTT_subscriber<msg_position_telescope> *_sub_brake; /*!< Brake subs */
    string client_sub_brake = "gui_sub_brake"; /*!< ID client subscribe */
    string topic_sub_brake = "topic_sub_brake"; /*!< Topic GUI subscribe */
    QTimer *_timer_brake; /*!< MQTT timer brake */

    MQTT_subscriber<msg_position_telescope> *_sub_power; /*!< Power subs */
    string client_sub_power = "gui_sub_power"; /*!< ID client subscribe */
    string topic_sub_power = "topic_sub_power"; /*!< Topic GUI subscribe */
    QTimer *_timer_power; /*!< MQTT timer power */

    MQTT_subscriber<msg_position_telescope> *_sub_error; /*!< Error subs */
    string client_sub_error = "gui_sub_error"; /*!< ID client subscribe */
    string topic_sub_error = "topic_sub_error"; /*!< Topic GUI subscribe */
    QTimer *_timer_error; /*!< MQTT timer error */

    QLabel *_error_AZ_label; /*!< Error AZ label member */
    QLabel *_error_ALT_label; /*!< Error ALT label member */
    QLabel *_power_AZ_label; /*!< Power AZ label member */
    QLabel *_power_ALT_label; /*!< Power ALT label member */
    QLabel *_AZ_display_label; /*!< AZ label member */
    QLabel *_ALT_display_label; /*!< ALT label member */

    QLabel *_ra_label; /*!< RA label member */
    QLabel *_dec_label; /*!< Dec label member */
    QLabel *_az_brake_label; /*!< RA label member */
    QLabel *_dec_brake_label; /*!< Dec label member */

    QPushButton *_full_home_telescope; /*!< Full home telescope button */
    QPushButton *_AZ_home_telescope; /*!< AZ home telescope button */
    QPushButton *_ALT_home_telescope; /*!< ALT home telescope button */
    QPushButton *_stop_home; /*!< Stop home button */

    QPushButton *_park_telescope; /*!< Park telescope button */
    QPushButton *_unpark_telescope; /*!< Unpark telescope button */
    QDoubleSpinBox *_az_park_spinbox; /*!< AZ park spin box */
    QDoubleSpinBox *_alt_park_spinbox; /*!< ALT park spin box */
    QLabel *_AZ_park_label; /*!< AZ park label member */
    QLabel *_ALT_park_label; /*!< ALT park label member */
    QPushButton *_stop_park; /*!< Stop park button */

    QDoubleSpinBox *_degree_ra; /*!< Degree RA member */
    QLabel *_degree_ra_label; /*!< Degree RA label member */
    QDoubleSpinBox *_minute_ra; /*!< Minute RA member */
    QLabel *_minute_ra_label; /*!< Minute RA label member */
    QDoubleSpinBox *_second_ra; /*!< Second RA member */
    QLabel *_second_ra_label; /*!< Second RA label member */

    QDoubleSpinBox *_degree_dec; /*!< Degree Dec member */
    QLabel *_degree_dec_label; /*!< Degree Dec label member */
    QDoubleSpinBox *_minute_dec; /*!< Minute Dec member */
    QLabel *_minute_dec_label; /*!< Minute Dec label member */
    QDoubleSpinBox *_second_dec; /*!< Second Dec member */
    QLabel *_second_dec_label; /*!< Second Dec label member */

    QComboBox *_equinox_combobox; /*!< Equinox combobox member */
    QPushButton *_goto_rad_dec; /*!< GOTO RA/Dec button */
    QPushButton *_sync_ra_dec; /*!< SYNC RA/Dec button */
    QPushButton *_stop_ra_dec; /*!< STOP RA/Dec button */

    QDoubleSpinBox *_degree_alt; /*!< Degree ALT member */
    QLabel *_degree_alt_label; /*!< Degree ALT label member */
    QDoubleSpinBox *_minute_alt; /*!< Minute ALT member */
    QLabel *_minute_alt_label; /*!< Minute ALT label member */
    QDoubleSpinBox *_second_alt; /*!< Second ALT member */
    QLabel *_second_alt_label; /*!< Second ALT label member */

    QDoubleSpinBox *_degree_az; /*!< Degree AZ member */
    QLabel *_degree_az_label; /*!< Degree AZ label member */
    QDoubleSpinBox *_minute_az; /*!< Minute AZ member */
    QLabel *_minute_az_label; /*!< Minute AZ label member */
    QDoubleSpinBox *_second_az; /*!< Second AZ member */
    QLabel *_second_az_label; /*!< Second AZ label member */

    QPushButton *_goto_alt_az; /*!< GOTO ALT/AZ button */
    QPushButton *_sync_alt_az; /*!< SYNC ALT/AZ button */
    QPushButton *_stop_alt_az; /*!< STOP ALT/AZ button */

    QPushButton *_config_velocity; /*!< Config velocity button */
    QPushButton *_config_mqtt; /*!< Config MQTT button */
    QPushButton *_show_graph; /*!< Show graph button */

    QPushButton *_choose_file; /*!< Choose file button */
    QPushButton *_load_traj; /*!< Load trajectory button */
    QPushButton *_follow_traj; /*!< Follow trajectory button */
    QPushButton *_config_sat; /*!< Config satellite button */
    QPushButton *_stop_sat; /*!< Stop satellite button */

    //////////////////////CONFIG TELESCOPE WIDGETS////////////////////////

    QSpinBox *_alt_up_spinbox; /*!< Limit ALT up spin box */
    QLabel *_limit_alt_up; /*!< Limit ALT up label member */
    QSpinBox *_alt_down_spinbox; /*!< Limit ALT down spin box */
    QLabel *_limit_alt_down; /*!< Limit ALT down label member */
    QPushButton *_set_limit_alt; /*!< Set limit ALT button */
    QPushButton *_reset_limit_alt; /*!< Reset limit ALT button */

    QSpinBox *_az_up_spinbox; /*!< Limit AZ up spin box */
    QLabel *_limit_az_up; /*!< Limit AZ up label member */
    QSpinBox *_az_down_spinbox; /*!< Limit AZ down spin box */
    QLabel *_limit_az_down; /*!< Limit AZ down label member */
    QPushButton *_set_limit_az; /*!< Set limit AZ button */
    QPushButton *_reset_limit_az; /*!< Reset limit AZ button */

    QSpinBox *_az_current_spinbox; /*!< Limit AZ current spin box */
    QLabel *_limit_az_current; /*!< Limit AZ current label member */
    QSpinBox *_lat_currrent_spinbox; /*!< Limit LAT current spin box */
    QLabel *_limit_lat_current; /*!< Limit LAT current label member */
    QPushButton *_set_limit_current; /*!< Set limit current button */
    QPushButton *_reset_limit_current; /*!< Reset limit current button */

    QSpinBox *_velocity_spinbox; /*!< Limit velocity spin box */
    QLabel *_limit_velocity; /*!< Limit velocity label member */
    QSpinBox *_acceleration_spinbox; /*!< Limit acceleration spin box */
    QLabel *_limit_acceleration; /*!< Limit acceleration label member */
    QPushButton *_set_limit_speed; /*!< Set limit speed button */
    QPushButton *_reset_limit_speed; /*!< Reset limit speed button */

    QSpinBox *_velocity_home_spinbox; /*!< Specify velocity homing spinbox */
    QLabel *_velocity_homing; /*!< Velocity homing label member */
    QSpinBox *_velocity_traj_spinbox; /*!< Specify velocity trajectory */
    QLabel *_velocity_trajectory; /*!< Velocity trajectory label member */
    QPushButton *_set_velocity; /*!< Set velocity button */
    QPushButton *_reset_velocity; /*!< Reset velocity button */

    QSpinBox *_accel_home_spinbox; /*!< Specify acceleration homing spinbox */
    QLabel *_accel_homing; /*!< Acceleration homing label member */
    QSpinBox *_accel_traj_spinbox; /*!< Specify acceleration trajectory */
    QLabel *_accel_trajectory; /*!< Acceleration trajectory label member */
    QPushButton *_set_accel; /*!< Set acceleration button */
    QPushButton *_reset_accel; /*!< Reset acceleration button */

    QDoubleSpinBox *_latitude_spinbox; /*!< Specify latitude position */
    QLabel *_latitude; /*!< Latitude label member */
    QDoubleSpinBox *_longitude_spinbox; /*!< Specify longitude position */
    QLabel *_longitude; /*!< Longitude label member */
    QSpinBox *_altitude_spinbox; /*!< Specify altitude position */
    QLabel *_altitude; /*!< Altitude label member */
    QPushButton *_set_position; /*!< Set position button */
    QPushButton *_reset_position; /*!< Reset position button */

    QLabel *_alt_up_info; /*!< ALT up info label */
    QLabel *_alt_down_info; /*!< ALT down info label */
    QLabel *_az_up_info; /*!< AZ up info label */
    QLabel *_az_down_info; /*!< AZ down info label */
    QLabel *_az_current_info; /*!< AZ current info label */
    QLabel *_lat_current_info; /*!< LAT current info label */
    QLabel *_velocity_limit_info; /*!< Velocity limit label */
    QLabel *_acceleration_limit_info; /*!< Acceleration limit label */
    QLabel *_velocity_home_info; /*!< Velocity home info label */
    QLabel *_velocity_traj_info; /*!< Velocity traj info label */
    QLabel *_accel_home_info; /*!< Acceleration home info label */
    QLabel *_accel_traj_info; /*!< Acceleration traj info label */
    QLabel *_latitude_info; /*!< Latitude info label */
    QLabel *_longitude_info; /*!< Longitude info label */
    QLabel *_altitude_info; /*!< Altitude info label */

    ///////////////////////GRAPH WINDOW WIDGETS///////////////////////

    QTabWidget *_graph_tabs; /*!< Tabs of the Graph window */
    QWidget *_error_alt_tab; /*!< Error ALT tab */
    QWidget *_error_az_tab; /*!< Error AZ tab */
    QWidget *_power_alt_tab; /*!< Power ALT tab */
    QWidget *_power_az_tab; /*!< Power AZ tab */
    QWidget *_all_errors_tab; /*!< All errors tab */
    QTimer *_timer_graph; /*!< Timer graph */

    QChartView *_display_graph_alt; /*!< Display ALT graph */
    QChart *_graph_alt; /*!< ALT Graph representation */
    QLineSeries *_curves_error_alt; /*!< Controller ALT errors data */
    QValueAxis *axeX_error_alt; /*!< X axis for ALT errors */
    QValueAxis *axeY_error_alt; /*!< Y axis for ALT errors */
    int y_error_min_alt = 20; /*!< Y min value for ALT errors */
    int y_error_max_alt = 40; /*!< Y max value for ALT errors */
    int number_points_alt = 1; /*!< Number of the ALT measurement */

    QChartView *_display_graph_az; /*!< Display AZ graph */
    QChart *_graph_az; /*!< AZ Graph representation */
    QLineSeries *_curves_error_az; /*!< Controller AZ errors data */
    QValueAxis *axeX_error_az; /*!< X axis for AZ errors */
    QValueAxis *axeY_error_az; /*!< Y axis for AZ errors */
    int y_error_min_az = 20; /*!< Y min value for AZ errors */
    int y_error_max_az = 40; /*!< Y max value for AZ errors */
    int number_points_az = 1; /*!< Number of the AZ measurement */

    QChartView *_display_graph_az_power; /*!< Display AZ power graph */
    QChart *_graph_az_power; /*!< AZ power Graph representation */
    QLineSeries *_curves_error_az_power; /*!< Controller AZ power data */
    QValueAxis *axeX_error_az_power; /*!< X axis for AZ power errors */
    QValueAxis *axeY_error_az_power; /*!< Y axis for AZ power errors */
    int y_error_min_az_power = 20; /*!< Y min value for AZ power errors */
    int y_error_max_az_power = 40; /*!< Y max value for AZ power errors */
    int number_points_az_power = 1; /*!< Number of the AZ power measurement */

    QChartView *_display_graph_alt_power; /*!< Display ALT power graph */
    QChart *_graph_alt_power; /*!< ALT power Graph representation */
    QLineSeries *_curves_error_alt_power; /*!< Controller ALT power data */
    QValueAxis *axeX_error_alt_power; /*!< X axis for ALT power errors */
    QValueAxis *axeY_error_alt_power; /*!< Y axis for ALT power errors */
    int y_error_min_alt_power = 20; /*!< Y min value for ALT power errors */
    int y_error_max_alt_power = 40; /*!< Y max value for ALT power errors */
    int number_points_alt_power = 1; /*!< Number of ALT power measurement */

    QChartView *_display_graph; /*!< Display ALT power graph */
    QChart *_graph; /*!< ALT power Graph representation */

    QLineSeries *_all_error_alt; /*!< Controller ALT value error */
    QLineSeries *_all_error_az; /*!< Controller AZ value error */
    QLineSeries *_all_error_alt_power; /*!< Controller ALT power error */
    QLineSeries *_all_error_az_power; /*!< Controller AZ power error */
    QValueAxis *axeY_all_error_alt; /*!< Y axis for ALT value errors */
    QValueAxis *axeY_all_error_az; /*!< Y axis for AZ value errors */
    QValueAxis *axeY_all_error_alt_power; /*!< Y axis for ALT power errors */
    QValueAxis *axeY_all_error_az_power; /*!< Y axis for AZ power errors */

    int y_all_error_min_alt = 20; /*!< Y min value for ALT error */
    int y_all_error_max_alt = 40; /*!< Y max value for ALT error */
    int y_all_error_min_az = 20; /*!< Y min value for AZ error */
    int y_all_error_max_az = 40; /*!< Y max value for AZ error */
    int y_all_error_min_alt_power = 20; /*!< Y min value for ALT power error */
    int y_all_error_max_alt_power = 40; /*!< Y max value for ALT power error */
    int y_all_error_min_az_power = 20; /*!< Y min value for AZ power error */
    int y_all_error_max_az_power = 40; /*!< Y max value for AZ power error */

    ////////////////////////DOME PAYLOADS SENT////////////////////////

    string dome_home_full; /*!< Dome full home payload */
    string dome_home_AZ; /*!< Dome AZ home payload */
    string dome_home_shutter; /*!< Dome shutter home payload */
    string dome_stop; /*!< Stop dome payload */

    string dome_handcontrol_west; /*!< Dome West payload */
    string dome_handcontrol_east; /*!< Dome East payload */

    string dome_connect; /*!< Connect dome payload */
    string dome_disconnect; /*!< Disconnect dome payload */
    string dome_park; /*!< Park dome payload */
    string dome_unpark; /*!< Unpark dome payload */

    string dome_az_goto; /*!< GOTO AZ dome payload */
    string dome_az_sync; /*!< SYNC AZ dome payload */
    string dome_az_speed; /*!< SPEED AZ dome payload */

    string dome_shutter_open; /*!< OPEN shutter dome payload */
    string dome_shutter_close; /*!< CLOSE shutter dome payload */
    string dome_shutter_sync; /*!< SYNC shutter dome payload */

    string config_dome; /*!< Serialized config dome file */

    ////////////////////////////DOME WIDGETS//////////////////////////

    QGridLayout *_dome_layout; /*!< Buttons grid dome layout */
    QGridLayout *_config_dome_layout; /*!< Config dome grid layout */
    map<int,string> _list_config_dome; /*!< Map (int, string) config dome */
    map<QGridLayout*,int> _map_widgets_dome; /*!< Widgets map (widget,level) */

    MQTT_subscriber<msg_position_telescope> *_sub_error_az; /*!< Error AZ */
    string client_sub_error_az = "gui_sub_error_az"; /*!< ID client sub */
    string topic_sub_error_az = "topic_sub_error_az"; /*!< Topic GUI sub */
    QTimer *_timer_error_az; /*!< MQTT timer error az */

    MQTT_subscriber<msg_position_telescope> *_sub_value_az; /*!< Value AZ */
    string client_sub_value_az = "gui_sub_value_az"; /*!< ID client sub */
    string topic_sub_value_az = "topic_sub_value_az"; /*!< Topic GUI sub */
    QTimer *_timer_value_az; /*!< MQTT timer value az */

    QLabel *_logging_label_dome; /*!< Logging label dome */
    QString _string_logging_dome; /*!< String Logging dome */
    QScrollArea *_logging_scrollArea_dome; /*!< Logging Scroll Area dome */

    QPushButton *_west_button_big_dome; /*!< Big West button for dome */
    QPushButton *_west_button_small_dome; /*!< Small West button for dome */
    QPushButton *_east_button_big_dome; /*!< Big East button for dome */
    QPushButton *_east_button_small_dome; /*!< Small East button for dome */
    QPushButton *_stop_button_dome; /*!< Stop button for dome */

    QDoubleSpinBox *_degree_hand_control_dome; /*!< Degree spinbox dome */
    QLabel *_degree_label_dome; /*!< Degree label dome */
    QDoubleSpinBox *_minute_hand_control_dome; /*!< Minute spinbox dome */
    QLabel *_minute_label_dome; /*!< Minute label dome */
    QLabel *_ratio_label_dome; /*!< Small big ratio movement dome */
    QComboBox *_ratio_combobox_dome; /*!< Ratio combobox dome */

    QPushButton *_connect_dome; /*!< Connect dome button */
    QPushButton *_disconnect_dome; /*!< Disconnect dome button */

    QPushButton *_full_home_dome; /*!< Full home dome button */
    QPushButton *_AZ_home_dome; /*!< AZ home dome button */
    QPushButton *_shutter_home_dome; /*!< Shutter home dome button */
    QPushButton *_stop_home_dome; /*!< Stop home dome button */

    QPushButton *_park_dome; /*!< Park dome button */
    QPushButton *_unpark_dome; /*!< Unpark dome button */
    QDoubleSpinBox *_az_park_spinbox_dome; /*!< AZ park spin box dome */
    QDoubleSpinBox *_shutter_park_spinbox_dome; /*!< Shutter park dome */
    QLabel *_AZ_park_label_dome; /*!< AZ park label dome */
    QLabel *_shutter_park_label_dome; /*!< Shutter park label dome */
    QPushButton *_stop_park_dome; /*!< Stop park dome button */

    QDoubleSpinBox *_degree_az_dome; /*!< Degree AZ dome */
    QLabel *_degree_az_label_dome; /*!< Degree AZ label dome */
    QDoubleSpinBox *_minute_az_dome; /*!< Minute AZ dome */
    QLabel *_minute_az_label_dome; /*!< Minute AZ label dome */

    QPushButton *_goto_alt_az_dome; /*!< GOTO AZ dome button */
    QPushButton *_sync_alt_az_dome; /*!< SYNC AZ dome button */
    QPushButton *_speed_alt_az_dome; /*!< SPEED AZ dome button */
    QPushButton *_stop_alt_az_dome; /*!< STOP AZ dome button */

    QPushButton *_open_shutter; /*!< Open shutter dome button */
    QPushButton *_close_shutter; /*!< Close shutter dome button */
    QPushButton *_sync_shutter; /*!< Sync shutter dome button */
    QSpinBox *_shutter_spinbox; /*!< Shutter dome spin box */
    QLabel *_aperture_shutter_label; /*!< Aperture shutter dome label */
    QPushButton *_stop_move_shutter; /*!< Stop shutter dome button */

    QLabel *_error_AZ_dome; /*!< Error AZ label dome */
    QLabel *_error_shutter_dome; /*!< Error Shutter label dome */
    QLabel *_AZ_display_dome; /*!< AZ label dome */
    QLabel *_shutter_display_dome; /*!< Shutter label dome */

    QPushButton *_config_dome; /*!< Config dome button */
    QPushButton *_show_graph_dome; /*!< Show graph dome button */

    //////////////////////CONFIG DOME WIDGETS////////////////////////

    QSpinBox *_az_up_dome; /*!< Limit AZ up dome spin box */
    QLabel *_limit_az_up_dome; /*!< Limit AZ up label dome */
    QSpinBox *_az_down_dome; /*!< Limit AZ down dome spin box */
    QLabel *_limit_az_down_dome; /*!< Limit AZ down label dome */
    QPushButton *_set_limit_az_dome; /*!< Set limit AZ dome button */
    QPushButton *_reset_limit_az_dome; /*!< Reset limit AZ dome button */

    QSpinBox *_shutter_up_dome; /*!< Limit shutter up dome spin box */
    QLabel *_limit_shutter_up_dome; /*!< Limit shutter up label dome */
    QSpinBox *_shutter_down_dome; /*!< Limit shutter down dome spin box */
    QLabel *_limit_shutter_down_dome; /*!< Limit shutter down label dome */
    QPushButton *_set_limit_shutter_dome; /*!< Set limit shutter dome button */
    QPushButton *_reset_limit_shutter_dome; /*!< Reset limit shutter button */

    QSpinBox *_velocity_dome; /*!< Limit velocity dome spin box */
    QLabel *_limit_velocity_dome; /*!< Limit velocity label dome */
    QSpinBox *_acceleration_dome; /*!< Limit acceleration dome spin box */
    QLabel *_limit_acceleration_dome; /*!< Limit acceleration label dome */
    QPushButton *_set_limit_speed_dome; /*!< Set limit speed dome button */
    QPushButton *_reset_limit_speed_dome; /*!< Reset limit speed dome button */

    QSpinBox *_velocity_home_dome; /*!< Specify velocity homing dome */
    QLabel *_velocity_homing_dome; /*!< Velocity homing label dome */
    QSpinBox *_velocity_traj_dome; /*!< Specify velocity trajectory dome */
    QLabel *_velocity_trajectory_dome; /*!< Velocity trajectory label dome */
    QPushButton *_set_velocity_dome; /*!< Set velocity dome button */
    QPushButton *_reset_velocity_dome; /*!< Reset velocity dome button */

    QSpinBox *_accel_home_dome; /*!< Specify acceleration homing dome */
    QLabel *_accel_homing_dome; /*!< Acceleration homing label dome */
    QSpinBox *_accel_traj_dome; /*!< Specify acceleration trajectory dome */
    QLabel *_accel_trajectory_dome; /*!< Acceleration trajectory label dome */
    QPushButton *_set_accel_dome; /*!< Set acceleration dome button */
    QPushButton *_reset_accel_dome; /*!< Reset acceleration dome button */

    QSpinBox *_az_offset_dome; /*!< Specify AZ offset dome */
    QLabel *_az_offset_label; /*!< AZ offset label dome */
    QSpinBox *_shutter_offset_dome; /*!< Specify shutter offset dome */
    QLabel *_shutter_offset_label; /*!< Shutter offset label dome */
    QPushButton *_set_offset_dome; /*!< Set offset dome button */
    QPushButton *_reset_offset_dome; /*!< Reset offset dome button */

    QLabel *_az_up_info_dome; /*!< AZ up info dome label */
    QLabel *_az_down_info_dome; /*!< AZ down info dome label */
    QLabel *_shutter_up_info; /*!< Shutter up info dome label */
    QLabel *_shutter_down_info; /*!< Shutter down info dome label */
    QLabel *_velocity_limit_info_dome; /*!< Velocity limit dome label */
    QLabel *_accel_limit_info_dome; /*!< Acceleration limit dome label */
    QLabel *_velocity_home_info_dome; /*!< Velocity home info dome label */
    QLabel *_velocity_traj_info_dome; /*!< Velocity traj info dome label */
    QLabel *_accel_home_info_dome; /*!< Acceleration home info dome label */
    QLabel *_accel_traj_info_dome; /*!< Acceleration traj info dome label */
    QLabel *_az_offset_info_dome; /*!< AZ offset info dome label */
    QLabel *_shutter_offset_info_dome; /*!< Shutter offset info dome label */

    /////////////////////////ACTUATORS M2 M3 WIDGETS/////////////////////

    QGridLayout *_actuator_layout; /*!< Buttons grid actuator layout */
    map<QGridLayout*,int> _map_widgets_actuator; /*!< Widgets map actuator */
    map<int,string> _list_backlash; /*!< Map (int, string) backlash */
    string backlash_serialized; /*!< Serialized backlash file */

    QPushButton *_full_home_actuator_M2; /*!< Full home actuators M2 button */
    QPushButton *_home_actuator1_M2; /*!< Home actuator 1 M2 button */
    QPushButton *_home_actuator2_M2; /*!< Home actuator 2 M2 button */
    QPushButton *_home_actuator3_M2; /*!< Home actuator 3 M2 button */
    QPushButton *_full_home_actuator_M3; /*!< Full home actuators M3 button */
    QPushButton *_home_actuator1_M3; /*!< Home actuator 1 M3 button */
    QPushButton *_home_actuator2_M3; /*!< Home actuator 2 M3 button */
    QPushButton *_home_actuator3_M3; /*!< Home actuator 3 M3 button */
    QPushButton *_stop_home_actuator; /*!< Stop home actuator button */

    QLabel *_logging_label_actuator; /*!< Logging label actuator */
    QString _string_logging_actuator; /*!< String Logging actuator */
    QScrollArea *_logging_scrollArea_actuator; /*!< Logging Scroll actuator */

    QPushButton *_connect_actuator; /*!< Connect actuator button */
    QPushButton *_disconnect_actuator; /*!< Disconnect actuator button */
    QPushButton *_backlash; /*!< Backlash actuators button */
    QPushButton *_previous_pos; /*!< Previous position button */
    QPushButton *_store_pos; /*!< Store position button */

    QPushButton *_park_actuator; /*!< Park actuator button */
    QPushButton *_unpark_actuator; /*!< Unpark actuator button */
    QDoubleSpinBox *_park_spinbox_actuator1_M2; /*!< Park spinbox actuator 1 */
    QDoubleSpinBox *_park_spinbox_actuator2_M2; /*!< Park spinbox actuator 2 */
    QDoubleSpinBox *_park_spinbox_actuator3_M2; /*!< Park spinbox actuator 3 */
    QLabel *_park_label_actuator1_M2; /*!< Park label actuator 1 M2 */
    QLabel *_park_label_actuator2_M2; /*!< Park label actuator 2 M2 */
    QLabel *_park_label_actuator3_M2; /*!< Park label actuator 3 M2 */
    QDoubleSpinBox *_park_spinbox_actuator1_M3; /*!< Park spinbox actuator 1 */
    QDoubleSpinBox *_park_spinbox_actuator2_M3; /*!< Park spinbox actuator 2 */
    QDoubleSpinBox *_park_spinbox_actuator3_M3; /*!< Park spinbox actuator 3 */
    QLabel *_park_label_actuator1_M3; /*!< Park label actuator 1 M3 */
    QLabel *_park_label_actuator2_M3; /*!< Park label actuator 2 M3 */
    QLabel *_park_label_actuator3_M3; /*!< Park label actuator 3 M3 */
    QPushButton *_stop_park_actuator; /*!< Stop park actuator¨ button */

    QDoubleSpinBox *_milli_hand_control; /*!< Millimeter spinbox actuator */
    QLabel *_milli_label; /*!< Millimeter label actuator */
    QDoubleSpinBox *_micro_hand_control; /*!< Micrometer spinbox actuator */
    QLabel *_micro_label; /*!< Micrometer label actuator */
    QRadioButton *_JOG_actuator; /*!< JOG movement actuator */
    QRadioButton *_velocity_actuator; /*!< Velocity movement actuator */

    QPushButton *_forward_actuator1_abs; /*!< Actuator 1 absolute forward */
    QPushButton *_forward_actuator1_relat; /*!< Actuator 1 relative forward */
    QPushButton *_backward_actuator1_abs; /*!< Actuator 1 absolute backward */
    QPushButton *_backward_actuator1_relat; /*!< Actuator 1 relat backward */
    QRadioButton *_mirror1_actuator1; /*!< Actuator 1 of mirror 1 */
    QRadioButton *_mirror1_actuator2; /*!< Actuator 2 of mirror 1 */
    QRadioButton *_mirror1_actuator3; /*!< Actuator 3 of mirror 1 */

    QPushButton *_forward_actuator2_abs; /*!< Actuator 2 absolute forward */
    QPushButton *_forward_actuator2_relat; /*!< Actuator 2 relative forward */
    QPushButton *_backward_actuator2_abs; /*!< Actuator 2 absolute backward */
    QPushButton *_backward_actuator2_relat; /*!< Actuator 2 relat backward */
    QRadioButton *_mirror2_actuator1; /*!< Actuator 1 of mirror 2 */
    QRadioButton *_mirror2_actuator2; /*!< Actuator 2 of mirror 2 */
    QRadioButton *_mirror2_actuator3; /*!< Actuator 3 of mirror 2 */

    QPushButton *_forward_actuators_relat; /*!< Actuators relative forward */
    QPushButton *_backward_actuators_relat; /*!< Actuators relative backward */
    QPushButton *_stop_actuators_relat; /*!< Actuators stop button */

    QLabel *_travel_actuator1_M2; /*!< Travel label actuator 1 M2 */
    QLabel *_travel_actuator2_M2; /*!< Travel label actuator 2 M2 */
    QLabel *_travel_actuator3_M2; /*!< Travel label actuator 3 M2 */
    QLabel *_travel_actuator1_M3; /*!< Travel label actuator 1 M3 */
    QLabel *_travel_actuator2_M3; /*!< Travel label actuator 2 M3 */
    QLabel *_travel_actuator3_M3; /*!< Travel label actuator 3 M3 */
    QLabel *_velocity_label; /*!< Velocity label actuator */
    QLabel *_jog_actuator; /*!< Jog step label actuator */

    /////////////////////////ACTUATORS M4 M5 WIDGETS/////////////////////

    QGridLayout *_actuator_layout_M4_M5; /*!< Buttons grid actuator layout */
    map<QGridLayout*,int> _map_widgets_actuator_M4_M5; /*!< Map actuators */

    QPushButton *_full_home_actuator_M4; /*!< Full home actuators M4 button */
    QPushButton *_home_actuator1_M4; /*!< Home actuator 1 M4 button */
    QPushButton *_home_actuator2_M4; /*!< Home actuator 2 M4 button */
    QPushButton *_home_actuator3_M4; /*!< Home actuator 3 M4 button */
    QPushButton *_full_home_actuator_M5; /*!< Full home actuators M5 button */
    QPushButton *_home_actuator1_M5; /*!< Home actuator 1 M5 button */
    QPushButton *_home_actuator2_M5; /*!< Home actuator 2 M5 button */
    QPushButton *_home_actuator3_M5; /*!< Home actuator 3 M5 button */
    QPushButton *_stop_home_actuator_M4_M5; /*!< Stop home actuator button */

    QLabel *_logging_label_actuator_M4_M5; /*!< Logging label actuator */
    QString _string_logging_actuator_M4_M5; /*!< String Logging actuator */
    QScrollArea *_logging_scrollArea_actuator_M4_M5; /*!< Logging Scrollbar*/

    QPushButton *_park_actuator_M4_M5; /*!< Park actuator M4 M5 button */
    QPushButton *_unpark_actuator_M4_M5; /*!< Unpark actuator M4 M5 button */
    QDoubleSpinBox *_park_spinbox_actuator1_M4; /*!< Park spinbox actuator 1 */
    QDoubleSpinBox *_park_spinbox_actuator2_M4; /*!< Park spinbox actuator 2 */
    QDoubleSpinBox *_park_spinbox_actuator3_M4; /*!< Park spinbox actuator 3 */
    QLabel *_park_label_actuator1_M4; /*!< Park label actuator 1 M4 */
    QLabel *_park_label_actuator2_M4; /*!< Park label actuator 2 M4 */
    QLabel *_park_label_actuator3_M4; /*!< Park label actuator 3 M4 */
    QDoubleSpinBox *_park_spinbox_actuator1_M5; /*!< Park spinbox actuator 1 */
    QDoubleSpinBox *_park_spinbox_actuator2_M5; /*!< Park spinbox actuator 2 */
    QDoubleSpinBox *_park_spinbox_actuator3_M5; /*!< Park spinbox actuator 3 */
    QLabel *_park_label_actuator1_M5; /*!< Park label actuator 1 M5 */
    QLabel *_park_label_actuator2_M5; /*!< Park label actuator 2 M5 */
    QLabel *_park_label_actuator3_M5; /*!< Park label actuator 3 M5 */
    QPushButton *_stop_park_actuator_M4_M5; /*!< Stop park actuator¨ button */

    QDoubleSpinBox *_milli_hand_control_M4_M5; /*!< Millimeter spinbox */
    QLabel *_milli_label_M4_M5; /*!< Millimeter label actuator */
    QDoubleSpinBox *_micro_hand_control_M4_M5; /*!< Micrometer spinbox */
    QLabel *_micro_label_M4_M5; /*!< Micrometer label actuator */
    QRadioButton *_JOG_actuator_M4_M5; /*!< JOG movement actuator */
    QRadioButton *_velocity_actuator_M4_M5; /*!< Velocity movement actuator */

    QPushButton *_forward_abs_M4; /*!< M4 absolute forward */
    QPushButton *_forward_relat_M4; /*!< M4 relative forward */
    QPushButton *_backward_abs_M4; /*!< M4 absolute backward */
    QPushButton *_backward_relat_M4; /*!< M4 relat backward */
    QRadioButton *_mirror4_actuator1; /*!< Actuator 1 of mirror 4 */
    QRadioButton *_mirror4_actuator2; /*!< Actuator 2 of mirror 4 */
    QRadioButton *_mirror4_actuator3; /*!< Actuator 3 of mirror 4 */

    QPushButton *_forward_abs_M5; /*!< M5 absolute forward */
    QPushButton *_forward_relat_M5; /*!< M5 relative forward */
    QPushButton *_backward_abs_M5; /*!< M5 absolute backward */
    QPushButton *_backward_relat_M5; /*!< M5 relat backward */
    QRadioButton *_mirror5_actuator1; /*!< Actuator 1 of mirror 5 */
    QRadioButton *_mirror5_actuator2; /*!< Actuator 2 of mirror 5 */
    QRadioButton *_mirror5_actuator3; /*!< Actuator 3 of mirror 5 */

    QPushButton *_forward_actuators_relat_M4; /*!< Actuators relat forward */
    QPushButton *_backward_actuators_relat_M4; /*!< Actuators relat backward */
    QPushButton *_stop_actuators_relat_M4; /*!< Actuators stop button */

    QLabel *_travel_actuator1_M4; /*!< Travel label actuator 1 M4 */
    QLabel *_travel_actuator2_M4; /*!< Travel label actuator 2 M4 */
    QLabel *_travel_actuator3_M4; /*!< Travel label actuator 3 M4 */
    QLabel *_travel_actuator1_M5; /*!< Travel label actuator 1 M5 */
    QLabel *_travel_actuator2_M5; /*!< Travel label actuator 2 M5 */
    QLabel *_travel_actuator3_M5; /*!< Travel label actuator 3 M5 */
    QLabel *_velocity_label_M4_M5; /*!< Velocity label actuator */
    QLabel *_jog_actuator_M4_M5; /*!< Jog step label actuator */

    ////////////////////////////CAMERA WIDGETS///////////////////////

    QGridLayout *_camera_layout; /*!< Buttons grid camera layout */
    map<QGridLayout*,int> _map_widgets_camera; /*!< Widgets map camera */

    QLabel *_logging_label_camera; /*!< Logging label camera */
    QString _string_logging_camera; /*!< String Logging camera */
    QScrollArea *_logging_scrollArea_camera; /*!< Logging Scroll camera */

    QPushButton *_connect_camera; /*!< Connect camera button */
    QPushButton *_disconnect_camera; /*!< Disconnect camera button */
    QPushButton *_config_camera; /*!< Config camera button */
    QPushButton *_show_graph_camera; /*!< Show graph camera button */

    QLabel *_s_exposure; /*!< Exposure in seconds label member */
    QSpinBox *_s_exposure_box; /*!< Exposure in seconds spinbox */
    QLabel *_ms_exposure; /*!< Exposure in ms label member */
    QSpinBox *_ms_exposure_box; /*!< Exposure in ms spinbox */
    QPushButton *_set_exposure_camera; /*!< Set exposure camera button */
    QPushButton *_stop_exposure_camera; /*!< Stop exposure camera button */

    QLabel *_fps_video; /*!< FPS video label member */
    QSpinBox *_fps_video_box; /*!< FPS video spinbox */
    QPushButton *_on_video; /*!< ON video camera button */
    QPushButton *_off_video; /*!< OFF video camera button */
    QPushButton *_stop_video; /*!< Stop video camera button */
    QPushButton *_on_record_video; /*!< ON record video camera button */
    QPushButton *_off_record_video; /*!< OFF record video camera button */
    QPushButton *_file_record_button; /*!< File record video camera button */
    QString file_record_video; /*!< File record video string */

    QLabel *_camera_name; /*!< Camera name */
    QLabel *_camera_version; /*!< Camera version */
    QLabel *_camera_size_x; /*!< Camera size X */
    QLabel *_camera_size_y; /*!< Camera size Y */
    QLabel *_camera_min_exposure; /*!< Camera min exposure time */
    QLabel *_camera_max_exposure; /*!< Camera max exposure time */
    QLabel *_camera_pixel_x; /*!< Camera pixel size X */
    QLabel *_camera_pixel_y; /*!< Camera pixel size Y */
    QLabel *_camera_binning_x; /*!< Camera binning X */
    QLabel *_camera_binning_y; /*!< Camera binning Y */
    QLabel *_camera_temperature; /*!< Camera temperature */

    //////////////////////CONFIG CAMERA WIDGETS////////////////////////

    QGridLayout *_config_camera_layout; /*!< Config grid camera layout */
    string config_camera; /*!< Serialized config camera file */
    map<int,string> _list_config_camera; /*!< Map (int,string) config camera */
    QImage image_zwo_qt; /*!< ASI ZWO image Qt */

    QSpinBox *_gain_camera_spinbox; /*!< Gain camera spin box */
    QLabel *_gain_camera; /*!< Gain camera label member */
    QSpinBox *_bandwidth_camera_spinbox; /*!< Bandwidth camera spinbox */
    QLabel *_bandwidth_camera; /*!< Bandwidth camera label member */
    QComboBox *_format_camera_box; /*!< Format camera combobox */
    QLabel *_format_camera; /*!< Format camera label member */
    QComboBox *_cooler_camera_box; /*!< Cooler camera combobox */
    QLabel *_cooler_camera; /*!< Cooler camera label member */
    QPushButton *_set_control_camera; /*!< Set control camera button */
    QPushButton *_reset_control_camera; /*!< Reset control camera button */

    QSpinBox *_top_frame_spinbox; /*!< Top frame spin box */
    QLabel *_top_frame; /*!< Top frame label member */
    QSpinBox *_left_frame_spinbox; /*!< Left frame spinbox */
    QLabel *_left_frame; /*!< Left frame label member */
    QSpinBox *_height_frame_spinbox; /*!< Height frame spin box */
    QLabel *_height_frame; /*!< Height frame label member */
    QSpinBox *_width_frame_spinbox; /*!< Width frame spinbox */
    QLabel *_width_frame; /*!< Width frame label member */

    QLabel *_type_frame; /*!< Type frame label member */
    QComboBox *_type_frame_box; /*!< Type frame combobox */
    QPushButton *_set_frame_camera; /*!< Set frame camera button */
    QPushButton *_reset_frame_camera; /*!< Reset frame camera button */

    QLabel *_x_binning; /*!< Binning X label member */
    QSpinBox *_x_binning_box; /*!< Binning X spinbox */
    QLabel *_y_binning; /*!< Binning Y label member */
    QSpinBox *_y_binning_box; /*!< Binning Y spinbox */
    QPushButton *_set_binning_camera; /*!< Set binning camera button */
    QPushButton *_reset_binning_camera; /*!< Reset binning camera button */

    QLabel *_filterwheel_label; /*!< Filterwheel label member */
    QSpinBox *_filterwheel_box; /*!< Filterwheel spinbox */
    QPushButton *_set_filterwheel_camera; /*!< Set filterwheel camera button */
    QPushButton *_reset_filterwheel_camera; /*!< Reset filterwheel button */

    QLabel *_target_temp_label; /*!< Target temperature label member */
    QSpinBox *_target_temp_box; /*!< Target temperature spinbox */
    QPushButton *_set_target_temp_camera; /*!< Set target temp camera button */
    QPushButton *_reset_target_temp_camera; /*!< Reset target temp button */

    QLabel *_gain_label; /*!< Gain info label member */
    QLabel *_bandwidth_label; /*!< Bandwidth info label member */
    QLabel *_format_label; /*!< Format info label member */
    QLabel *_cooler_label; /*!< Cooler info label member */
    QLabel *_top_frame_label; /*!< Top frame info label member */
    QLabel *_left_frame_label; /*!< Left frame info label member */
    QLabel *_height_frame_label; /*!< Height frame info label member */
    QLabel *_width_frame_label; /*!< Width frame info label member */
    QLabel *_type_frame_label; /*!< Type frame info label member */
    QLabel *_x_binning_label; /*!< Binning X info label member */
    QLabel *_y_binning_label; /*!< Binning Y info label member */

    ////////////////////////////WEATHER PARAMETERS/////////////////////

    QGridLayout *_weather_layout; /*!< Buttons grid weather layout */
    map<int,string> _list_config_thresholds; /*!< Map config thresholds */
    string config_weather; /*!< Serialized config weather file */
    map<QGridLayout*,int> _map_weather; /*!< Widgets map (widget,level) */
    QGridLayout *_config_weather_layout; /*!< Config grid weather layout */
    QTimer *_timer_weather; /*!< Timer to check weather conditions */

    QPushButton *_show_graph_weather; /*!< Show graph weather button */
    QPushButton *_config_weather; /*!< Configuration weather button */
    QPushButton *_connect_weather; /*!< Connect weather button */
    QPushButton *_disconnect_weather; /*!< Disconnect weather button */
    QPushButton *_meteoblue; /*!< Meteoblue connection button */

    QLabel *_logging_weather; /*!< Logging label weather*/
    QString _string_log_weather; /*!< String Logging weather */
    QScrollArea *_logging_scroll_weather; /*!< Logging Scroll Area weather */
    QTextToSpeech *_speech; /*!< Speech weather dialog */

    float _wind_direction_min; /*!< Wind direction min */
    float _wind_direction_max; /*!< Wind direction max */
    float _wind_direction_ave; /*!< Wind direction average */
    float _wind_speed_min; /*!< Wind speed min */
    float _wind_speed_max; /*!< Wind speed max */
    float _wind_speed_ave; /*!< Wind speed average */
    float _air_temp; /*!< Air temperature */
    float _relat_humidity; /*!< Relative humidity */
    float _dew_point; /*!< Dew point */
    float _air_pressure; /*!< Air pressure */

    float _rain_accum; /*!< Rain accumulation */
    float _rain_duration; /*!< Rain duration */
    float _rain_intensity; /*!< Rain intensity */
    float _hail_accum; /*!< Hail accumulation */
    float _hail_duration; /*!< Hail duration */
    float _hail_intensity; /*!< Hail intensity */
    float _rain_peak_intensity; /*!< Rain peak intensity */
    float _hail_peak_intensity; /*!< Hail peak intensity */
    float _heating_temp; /*!< Heating temperature */
    float _heating_voltage; /*!< Heating voltage */
    float _supply_voltage; /*!< Supply voltage */
    float _ref_voltage; /*!< Reference voltage */

    QLabel *_wind_dir_min_label; /*!< Wind direction min label */
    QLabel *_wind_dir_max_label; /*!< Wind direction max label */
    QLabel *_wind_dir_ave_label; /*!< Wind direction average label */
    QLabel *_wind_speed_min_label; /*!< Wind speed min label */
    QLabel *_wind_speed_max_label; /*!< Wind speed max label */
    QLabel *_wind_speed_ave_label; /*!< Wind speed average label */
    QLabel *_air_temp_label; /*!< Air temperature label */
    QLabel *_relat_hum_label; /*!< Relative humidity label */
    QLabel *_dew_point_label; /*!< Dew point label */
    QLabel *_air_pressure_label; /*!< Air pressure label */

    QLabel *_rain_accum_label; /*!< Rain accumulation label */
    QLabel *_rain_duration_label; /*!< Rain duration label */
    QLabel *_rain_int_label; /*!< Rain intensity label */
    QLabel *_hail_accum_label; /*!< Hail accumulation label */
    QLabel *_hail_dur_label; /*!< Hail duration label */
    QLabel *_hail_int_label; /*!< Hail intensity label */
    QLabel *_rain_peak_int_label; /*!< Rain peak intensity label */
    QLabel *_hail_peak_int_label; /*!< Hail peak intensity label */
    QLabel *_heating_temp_label; /*!< Heating temperature label */
    QLabel *_heating_volt_label; /*!< Heating voltage label */
    QLabel *_supply_volt_label; /*!< Supply voltage label */
    QLabel *_ref_volt_label; /*!< Reference voltage label */

    //////////////////////CONFIG WEATHER WIDGETS////////////////////////

    QDoubleSpinBox *_wind_dir_spinbox; /*!< Limit wind direction spin box */
    QLabel *_limit_wind_dir; /*!< Limit wind direction label member */
    QDoubleSpinBox *_wind_speed_spinbox; /*!< Limit wind speed spin box */
    QLabel *_limit_wind_speed; /*!< Limit wind speed label member */
    QDoubleSpinBox *_temp_spinbox; /*!< Limit temperature spin box */
    QLabel *_limit_temp; /*!< Limit temperature label member */
    QDoubleSpinBox *_hum_spinbox; /*!< Limit humidity spin box */
    QLabel *_limit_hum; /*!< Limit humidity label member */
    QDoubleSpinBox *_dew_point_spinbox; /*!< Limit dew point spin box */
    QLabel *_limit_dew_point; /*!< Limit dew point label member */
    QDoubleSpinBox *_pressure_spinbox; /*!< Limit pressure spin box */
    QLabel *_limit_pressure; /*!< Limit pressure label member */
    QPushButton *_set_limit_wind; /*!< Set limit wind button */
    QPushButton *_reset_limit_wind; /*!< Reset limit wind button */

    QLabel *_wind_dir_config; /*!< Wind direction config label */
    QLabel *_wind_speed_config; /*!< Wind speed config label */
    QLabel *_temp_config; /*!< Air temperature config label */
    QLabel *_hum_config; /*!< Relative humidity config label */
    QLabel *_dew_point_config; /*!< Dew point config label */
    QLabel *_pressure_config; /*!< Air pressure config label */

    QDoubleSpinBox *_rain_accum_spinbox; /*!< Limit rain accumulation */
    QLabel *_limit_rain_accum; /*!< Limit rain accumulation label member */
    QDoubleSpinBox *_rain_dur_spinbox; /*!< Limit rain duration spin box */
    QLabel *_limit_rain_dur; /*!< Limit rain duration label member */
    QDoubleSpinBox *_rain_int_spinbox; /*!< Limit rain intensity spin box */
    QLabel *_limit_rain_int; /*!< Limit rain intensity label member */
    QDoubleSpinBox *_rain_peak_spinbox; /*!< Limit rain peak spin box */
    QLabel *_limit_rain_peak; /*!< Limit rain peak label member */
    QPushButton *_set_limit_rain; /*!< Set limit rain button */
    QPushButton *_reset_limit_rain; /*!< Reset limit rain button */

    QLabel *_rain_accum_config; /*!< Rain accumulation config label */
    QLabel *_rain_dur_config; /*!< Rain duration config label */
    QLabel *_rain_int_config; /*!< Rain intensity config label */
    QLabel *_rain_peak_int_config; /*!< Rain peak intensity config label */

    QDoubleSpinBox *_hail_accum_spinbox; /*!< Limit hail accumulation */
    QLabel *_limit_hail_accum; /*!< Limit hail accumulation label member */
    QDoubleSpinBox *_hail_dur_spinbox; /*!< Limit hail duration spin box */
    QLabel *_limit_hail_dur; /*!< Limit hail duration label member */
    QDoubleSpinBox *_hail_int_spinbox; /*!< Limit hail intensity spin box */
    QLabel *_limit_hail_int; /*!< Limit hail intensity label member */
    QDoubleSpinBox *_hail_peak_spinbox; /*!< Limit hail peak spin box */
    QLabel *_limit_hail_peak; /*!< Limit hail peak label member */
    QPushButton *_set_limit_hail; /*!< Set limit hail button */
    QPushButton *_reset_limit_hail; /*!< Reset limit hail button */

    QLabel *_hail_accum_config; /*!< Hail accumulation config label */
    QLabel *_hail_dur_config; /*!< Hail duration config label */
    QLabel *_hail_int_config; /*!< Hail intensity config label */
    QLabel *_hail_peak_int_config; /*!< Hail peak intensity config label */

    QDoubleSpinBox *_heat_temp_spinbox; /*!< Limit heating temperature */
    QLabel *_limit_heat_temp; /*!< Limit heating temperature label member */
    QDoubleSpinBox *_heat_volt_spinbox; /*!< Limit heating voltage spin box */
    QLabel *_limit_heat_volt; /*!< Limit heating voltage label member */
    QDoubleSpinBox *_supp_volt_spinbox; /*!< Limit supply voltage spin box */
    QLabel *_limit_supp_volt; /*!< Limit supply voltage label member */
    QDoubleSpinBox *_ref_volt_spinbox; /*!< Limit reference voltage spin box */
    QLabel *_limit_ref_volt; /*!< Limit reference voltage label member */
    QPushButton *_set_limit_volt; /*!< Set limit voltage button */
    QPushButton *_reset_limit_volt; /*!< Reset limit voltage button */

    QLabel *_heating_temp_config; /*!< Heating temperature config label */
    QLabel *_heating_volt_config; /*!< Heating voltage config label */
    QLabel *_supply_volt_config; /*!< Supply voltage config label */
    QLabel *_ref_volt_config; /*!< Reference voltage config label */

    ///////////////////////GRAPH WINDOW WEATHER///////////////////////

    QTabWidget *_graph_tabs_weather; /*!< Tabs of the Graph window weather */
    QWidget *_wind_direction_tab; /*!< Wind direction tab */
    QWidget *_wind_speed_tab; /*!< Wind speed tab */
    QWidget *_temperature_tab; /*!< Temperature tab */
    QWidget *_dew_point_tab; /*!< Dew point tab */
    QWidget *_humidity_tab; /*!< Humidity tab */
    QWidget *_pressure_tab; /*!< Pressure tab */
    QWidget *_voltage_tab; /*!< Voltage tab */
    QTimer *_timer_graph_weather; /*!< Timer graph weather */

    QChartView *_display_graph_wind_dir; /*!< Display wind direction graph */
    QChart *_graph_wind_dir; /*!< Wind direction graph representation */
    QLineSeries *_curves_wind_dir; /*!< Wind direction data */
    QValueAxis *axeX_wind_dir; /*!< X axis for wind direction */
    QValueAxis *axeY_wind_dir; /*!< Y axis for wind direction */
    int y_error_min_wind_dir = 50; /*!< Y min value for wind direction */
    int y_error_max_wind_dir = 200; /*!< Y max value for wind direction */
    int number_points_wind_dir = 1; /*!< Number of the wind direction */

    QChartView *_display_graph_wind_speed; /*!< Display wind speed graph */
    QChart *_graph_wind_speed; /*!< Wind speed graph representation */
    QLineSeries *_curves_wind_speed; /*!< Wind speed data */
    QValueAxis *axeX_wind_speed; /*!< X axis for wind speed */
    QValueAxis *axeY_wind_speed; /*!< Y axis for wind speed */
    int y_error_min_wind_speed = 0; /*!< Y min value for wind speed */
    int y_error_max_wind_speed = 30; /*!< Y max value for wind speed */
    int number_points_wind_speed = 1; /*!< Number of the wind speed */

    QChartView *_display_graph_temp; /*!< Display temperature graph */
    QChart *_graph_temp; /*!< Temperature graph representation */
    QLineSeries *_curves_temp; /*!< Temperature data */
    QValueAxis *axeX_temp; /*!< X axis for temperature */
    QValueAxis *axeY_temp; /*!< Y axis for temperature */
    int y_error_min_temp = -20; /*!< Y min value for temperature */
    int y_error_max_temp = 40; /*!< Y max value for temperature */
    int number_points_temp = 1; /*!< Number of the temperature */

    QChartView *_display_graph_dew_point; /*!< Display dew point graph */
    QChart *_graph_dew_point; /*!< Dew point graph representation */
    QLineSeries *_curves_dew_point; /*!< Dew point data */
    QValueAxis *axeX_dew_point; /*!< X axis for dew point */
    QValueAxis *axeY_dew_point; /*!< Y axis for dew point */
    int y_error_min_dew_point = -20; /*!< Y min value for dew point */
    int y_error_max_dew_point = 30; /*!< Y max value for dew point */
    int number_points_dew_point = 1; /*!< Number of the dew point */

    QChartView *_display_graph_hum; /*!< Display humidity graph */
    QChart *_graph_hum; /*!< Humidity graph representation */
    QLineSeries *_curves_hum; /*!< Humidity data */
    QValueAxis *axeX_hum; /*!< X axis for humidity */
    QValueAxis *axeY_hum; /*!< Y axis for humidity */
    int y_error_min_hum = 0; /*!< Y min value for humidity */
    int y_error_max_hum = 100; /*!< Y max value for humidity */
    int number_points_hum = 1; /*!< Number of the humidity */

    QChartView *_display_graph_press; /*!< Display pressure graph */
    QChart *_graph_press; /*!< Pressure graph representation */
    QLineSeries *_curves_press; /*!< Pressure data */
    QValueAxis *axeX_press; /*!< X axis for pressure */
    QValueAxis *axeY_press; /*!< Y axis for pressure */
    int y_error_min_press = 750; /*!< Y min value for pressure */
    int y_error_max_press = 1300; /*!< Y max value for pressure */
    int number_points_press = 1; /*!< Number of the pressure */

    QChartView *_display_graph_volt; /*!< Display voltage graph */
    QChart *_graph_volt; /*!< Voltage graph representation */
    QLineSeries *_curves_volt; /*!< Voltage data */
    QValueAxis *axeX_volt; /*!< X axis for voltage */
    QValueAxis *axeY_volt; /*!< Y axis for voltage */
    int y_error_min_volt = 0; /*!< Y min value for voltage */
    int y_error_max_volt = 10; /*!< Y max value for voltage */
    int number_points_volt = 1; /*!< Number of the voltage */

    ////////////////////////////POSITION PARAMETERS/////////////////////

    QGridLayout *_position_layout; /*!< Buttons grid position layout */
    QLabel *_label_telescope_pos; /*!< Label telescope position */
    QPixmap *_pixmap_telescope; /*!< Pixmap telescope position */
    QImage _image_telescope; /*!< Telescope position image */

    //////////////////////////////IMAGING PARAMETERS/////////////////////

    QGridLayout *_imaging_layout; /*!< Buttons grid imaging layout */
    QLabel *_label_imaging; /*!< Label imaging */
    QPixmap *_pixmap_imaging; /*!< Pixmap imaging */
    QCursor *_cursor_image; /*!< Cursor image associated to QPixmap */
    QImage _imaging_captured; /*!< Imaging captured */
    QTimer *_timer_imaging; /*!< Timer associated to cursor imaging */

    QPoint lastPoint; /*!< Point associated to Mouse Press event */
    QPoint endPoint; /*!< Point associated to Mouse Release event */
    QPoint cursor_position; /*!< Cursor position */
    int width_crop = 0; /*!< Crop width image */
    int height_crop = 0; /*!< Crop height image */

protected:
    /*!
     *  \brief Override mouse press event for imaging
     *  \param event : QMouseEvent *
     */
    void mousePressEvent(QMouseEvent *event) override;
    /*!
     *  \brief Override mouse move event to draw rectangle in image
     *  \param event : QMouseEvent *
     */
    void mouseMoveEvent(QMouseEvent *event) override;
    /*!
     *  \brief Override mouse release event for imaging
     *  \param event : QMouseEvent *
     */
    void mouseReleaseEvent(QMouseEvent *event) override;
};
#endif // VIEW_H
