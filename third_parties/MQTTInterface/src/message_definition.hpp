#ifndef MSGDEF_H_
#define MSGDEF_H_

#include <iostream>
#include <vector>
#include <list>
#include <string>



/**
* Define here the kind of message
* used to communicate accross
* the system
*/
class msg_mqtt
{
public:
	
	virtual void assign(const char* s, size_t n) = 0;
	virtual const char * data() = 0; 
	virtual size_t size() = 0;
};

union c2f
{
	float f;
	char c[4];
};

struct position_telescope
{
	float alt;
	float az;
};

struct timestamped_position_telescope
{
	float alt;
	float az;
	float timestamp;
};

class msg_position_telescope : public virtual msg_mqtt
{
	char a[8];
public:
	position_telescope _pos;	
	
	void assign(const char* s, size_t n)
	{
		if(n != 8)
		{
			std::cout<<"WARNING n="<<n<<std::endl;
			throw std::invalid_argument("Invalid size assigned to msg_position_telescope");
		}
		/*char a[4];
		memcpy(a,s,4);
		_pos.alt = atof(a);
		memcpy(a,s+4,4);
		_pos.az = atof(a);*/
		c2f converter;
		converter.c[0] = s[0];
		converter.c[1] = s[1];
		converter.c[2] = s[2];
		converter.c[3] = s[3];
		_pos.alt = converter.f;
		
		converter.c[0] = s[4];
		converter.c[1] = s[5];
		converter.c[2] = s[6];
		converter.c[3] = s[7];
		_pos.az = converter.f;
		
	}
	
	const char * data()
	{
		
		c2f converter;
		converter.f = _pos.alt;
		a[0] = converter.c[0];
		a[1] = converter.c[1];
		a[2] = converter.c[2];
		a[3] = converter.c[3];
		//memcpy(a,converter.c,4);
		
		converter.f = _pos.az;
		a[4] = converter.c[0];
		a[5] = converter.c[1];
		a[6] = converter.c[2];
		a[7] = converter.c[3];
		//memcpy(a+4,converter.c,4);
		
		return a;
	}
	
	size_t size() { return 8;}
};


class msg_timestamped_position_telescope : public virtual msg_mqtt
{
	char a[12];
public:
	timestamped_position_telescope _pos;	
	
	void assign(const char* s, size_t n)
	{
		if(n != 12)
		{
			std::cout<<"WARNING n="<<n<<std::endl;
			throw std::invalid_argument("Invalid size assigned to msg_position_telescope");
		}
		
		c2f converter;
		converter.c[0] = s[0];
		converter.c[1] = s[1];
		converter.c[2] = s[2];
		converter.c[3] = s[3];
		_pos.alt = converter.f;
		
		converter.c[0] = s[4];
		converter.c[1] = s[5];
		converter.c[2] = s[6];
		converter.c[3] = s[7];
		_pos.az = converter.f;
		
		converter.c[0] = s[8];
		converter.c[1] = s[9];
		converter.c[2] = s[10];
		converter.c[3] = s[11];
		_pos.timestamp = converter.f;
		
	}
	
	const char * data()
	{
		
		c2f converter;
		converter.f = _pos.alt;
		a[0] = converter.c[0];
		a[1] = converter.c[1];
		a[2] = converter.c[2];
		a[3] = converter.c[3];
		
		converter.f = _pos.az;
		a[4] = converter.c[0];
		a[5] = converter.c[1];
		a[6] = converter.c[2];
		a[7] = converter.c[3];
		
		converter.f = _pos.timestamp;
		a[8] = converter.c[0];
		a[9] = converter.c[1];
		a[10] = converter.c[2];
		a[11] = converter.c[3];

		return a;
	}
	
	size_t size() { return 12;}
};

#endif 
