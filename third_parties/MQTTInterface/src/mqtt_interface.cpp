#include "mqtt_interface.hpp"

template class MQTT_publisher<msg_position_telescope>;
template class MQTT_subscriber<msg_position_telescope>;
template class MQTT_subscriber<std::string>;

void action_listener::on_failure(const mqtt::token& tok) {
	std::cout << name_ << " failure";
	if (tok.get_message_id() != 0)
		std::cout << " for token: [" << tok.get_message_id() << "]" << std::endl;
	std::cout << std::endl;
}

void action_listener::on_success(const mqtt::token& tok)  {
	std::cout << name_ << " success";
	if (tok.get_message_id() != 0)
		std::cout << " for token: [" << tok.get_message_id() << "]" << std::endl;
	auto top = tok.get_topics();
	if (top && !top->empty())
		std::cout << "\ttoken topic: '" << (*top)[0] << "', ..." << std::endl;
	std::cout << std::endl;
}


/////////////////////////////////////////////////////////////////////////////


	void callback_pub::connection_lost(const std::string& cause)  {
		cout << "\nConnection lost" << endl;
		if (!cause.empty())
			cout << "\tcause: " << cause << endl;
	}

	void callback_pub::delivery_complete(mqtt::delivery_token_ptr tok) {
		/*cout << "\tDelivery complete for token: "
			<< (tok ? tok->get_message_id() : -1) << endl;*/
	}



/////////////////////////////////////////////////////////////////////////////////////


	void callback::reconnect() {
		std::this_thread::sleep_for(std::chrono::milliseconds(2500));
		try {
			cli_.connect(connOpts_, nullptr, *this);
		}
		catch (const mqtt::exception& exc) {
			std::cerr << "Error: " << exc.what() << std::endl;
			exit(1);
		}
	}

	// Re-connection failure
	void callback::on_failure(const mqtt::token& tok)  {
		std::cout << "Connection attempt failed" << std::endl;
		if (++nretry_ > 5)
			exit(1);
		reconnect();
	}

	// (Re)connection success
	// Either this or connected() can be used for callbacks.
	void callback::on_success(const mqtt::token& tok)  {}

	// (Re)connection success
	void callback::connected(const std::string& cause)  {
		std::cout << "\nConnection success" << std::endl;

		cli_.subscribe(_TOPIC_SUB, 1, nullptr, subListener_);
	}

	// Callback for when the connection is lost.
	// This will initiate the attempt to manually reconnect.
	void callback::connection_lost(const std::string& cause)  {
		std::cout << "\nConnection lost" << std::endl;
		if (!cause.empty())
			std::cout << "\tcause: " << cause << std::endl;

		std::cout << "Reconnecting..." << std::endl;
		nretry_ = 0;
		reconnect();
	}

	// Callback for when a message arrives.
	void callback::message_arrived(mqtt::const_message_ptr msg)  {
		_msg = msg;
		_msg_arrived = 1;
	}

	void callback::delivery_complete(mqtt::delivery_token_ptr token)  {}


/////////////////////////////////////////////////////////////

   template <class T>
   void MQTT_publisher<T>::sync_publish()
   {
   		_ok = 1;
   		while(_ok)
   		{
   			std::this_thread::sleep_for(std::chrono::duration<double,std::milli>(_periode));
			mqtt::message_ptr pubmsg = mqtt::make_message(_TOPIC_PUB, &_msg, sizeof(T));
			pubmsg->set_qos(1);
			cli_pub.publish(pubmsg)->wait_for(std::chrono::seconds(10));
   		}   		
   }
   
   template <class T>
   void MQTT_publisher<T>::stop_publish()
   {
   		_ok=0;
   }
   
   template <class T>
   void MQTT_publisher<T>::async_publish_msg(T &payload)
   {
		mqtt::message_ptr pubmsg = mqtt::make_message(_TOPIC_PUB, payload.data(), payload.size());
		pubmsg->set_qos(1);
		cli_pub.publish(pubmsg)->wait_for(std::chrono::seconds(10));
   }
   
   template <class T>
   void MQTT_publisher<T>::start(int sync)
   {
		connect();
		if(sync)
		{
			mqtt_thread = std::thread(&MQTT_publisher<T>::sync_publish, this);
			mqtt_thread.detach();
		}
   }
   
	template <class T>
	void MQTT_publisher<T>::disconnect()
   	{
	   try {
			cout << "\nDisconnecting..." << endl;
			cli_pub.disconnect()->wait();
			cout << "  ...OK" << endl;
		}
		catch (const mqtt::exception& exc) {
			cerr << exc.what() << endl;
			return;
		}
	}


///////////////////////////////////////////////////////////////////////////

  template <class T>
	void MQTT_subscriber<T>::connect()
	{
		std::cout<<"MQTT_subscriber running!"<<std::endl;
	   	// A subscriber often wants the server to remember its messages when its
		// disconnected. In that case, it needs a unique ClientID and a
		// non-clean session.

        connOpts_sub.set_clean_session(false);
        cli_sub.set_callback(cb_sub);
    
        try {
        	std::cout << "Connecting to the MQTT server..." << std::flush;
        	cli_sub.connect(connOpts_sub, nullptr, cb_sub);
        }
        catch (const mqtt::exception& exc) {
        	std::cerr << "\nERROR: Unable to connect to MQTT server for sub: '"<<
        	_SERVER_ADDRESS << "'" << exc << std::endl;
        	return;
        }

	}
   
   template <class T>
   	void MQTT_subscriber<T>::get_message(T &msg)
	{
		mqtt::binary_ref a = cb_sub._msg.get()->get_payload_ref();
		//const char* all_msg = a.data();
		std::cout<<"*************** size is "<<a.length()*sizeof(char)<<std::endl;
		//memcpy((char *)(&msg.data), a.data(), a.length()+1);
		msg.assign(a.data(),a.length());
		std::cout<<"*************** msg: "<<msg.size()<<std::endl;
		cb_sub._msg_arrived = 0;
	}

	
   template <class T>
   void MQTT_subscriber<T>::start()
   {
   		connect();
   }
   
   template <class T>
	void MQTT_subscriber<T>::disconnect()
   	{
	   try {
			cout << "\nDisconnecting..." << endl;
			cli_sub.disconnect()->wait();
			cout << "  ...OK" << endl;
		}
		catch (const mqtt::exception& exc) {
			cerr << exc.what() << endl;
			return;
		}
	}


//////////////////////////////////////////////////////////////////////////

   
   void MQTT_pubsub::connect_all()
   {
	   	std::cout<<"MQTT_pubsub running!"<<std::endl;
	   	// A subscriber often wants the server to remember its messages when its
		// disconnected. In that case, it needs a unique ClientID and a
		// non-clean session.

        connOpts_sub.set_clean_session(false);
        connOpts_pub = mqtt::connect_options_builder()
			.clean_session(true)
			.automatic_reconnect(true)
			.finalize();

        cli_sub.set_callback(cb);
        cli_pub.set_callback(cb_pub);
        
        try {
        	std::cout << "Connecting to the MQTT server..." << std::flush;
        	cli_sub.connect(connOpts_sub, nullptr, cb);
        }
        catch (const mqtt::exception& exc) {
        	std::cerr << "\nERROR: Unable to connect to MQTT server for sub: '"<<
        	_SERVER_ADDRESS << "'" << exc << std::endl;
        	return;
        }
        
        try {
			cout << "\nConnecting pub..." << endl;
			mqtt::token_ptr conntok = cli_pub.connect(connOpts_pub);
			cout << "Waiting for the connection..." << endl;
			conntok->wait();
			cout << "  ...OK" << endl;
		}
		catch (const mqtt::exception& exc) {
        	std::cerr << "\nERROR: Unable to connect to MQTT server for pub: '"<<
        	_SERVER_ADDRESS << "'" << exc << std::endl;
        	return;
        }

   }
   
   void MQTT_pubsub::publish_msg(std::string payload)
   {
		mqtt::message_ptr pubmsg = mqtt::make_message(_TOPIC_PUB, payload.c_str());
		pubmsg->set_qos(1);
		cli_pub.publish(pubmsg)->wait_for(std::chrono::seconds(10));
   }
   
   std::string MQTT_pubsub::get_message()
	{
        string str = cb._msg.get()->get_payload_str();
        cb._msg_arrived = 0;
		return str;
	}
   
   void MQTT_pubsub::start()
   {
   		connect_all();
   }
   
   void MQTT_pubsub::disconnect_all()
   {
	   try {
			cli_sub.disconnect()->wait();
			cli_pub.disconnect()->wait();
		}
		catch (const mqtt::exception& exc) {
			cerr << exc.what() << endl;
			return;
		}
	}

