#ifndef MQTTInterface_H_
#define MQTTInterface_H_

#include <iostream>
#include <vector>
#include <list>
#include <thread>
#include <pthread.h>
#include <chrono>
#include <string>
#include <cmath>

#include <mqtt/async_client.h>

#include "message_definition.hpp"


using namespace std;

/////////////////////////////////////////////////////////////////////////////

/**
* Simple conversion tool
**/
union s2b
{
	char char_msg[4];
	float float_msg;
};


/////////////////////////////////////////////////////////////////////////////

// Callbacks for the success or failures of requested actions.
// This could be used to initiate further action, but here we just log the
// results to the console.

class action_listener : public virtual mqtt::iaction_listener
{
	std::string name_;

	void on_failure(const mqtt::token& tok) override;

	void on_success(const mqtt::token& tok) override;

public:
	action_listener(const std::string& name) : name_(name) {}
};

/////////////////////////////////////////////////////////////////////////////


/**
 * A callback class for use with the main MQTT client.
 */
class callback_pub : public virtual mqtt::callback
{
public:
	void connection_lost(const std::string& cause) override;

	void delivery_complete(mqtt::delivery_token_ptr tok) override;
};


/**
 * Local callback & listener class for use with the client connection.
 * This is primarily intended to receive messages, but it will also monitor
 * the connection to the broker. If the connection is lost, it will attempt
 * to restore the connection and re-subscribe to the topic.
 */
class callback : public virtual mqtt::callback,
					public virtual mqtt::iaction_listener

{
	std::string _TOPIC_SUB;
	// Counter for the number of connection retries
	int nretry_;
	// The MQTT client
	mqtt::async_client& cli_;
	// Options to use if we need to reconnect
	mqtt::connect_options& connOpts_;
	// An action listener to display the result of actions.
	action_listener subListener_;

	// This demonstrates manually reconnecting to the broker by calling
	// connect() again. This is a possibility for an application that keeps
	// a copy of it's original connect_options, or if the app wants to
	// reconnect with different options.
	// Another way this can be done manually, if using the same options, is
	// to just call the async_client::reconnect() method.
	void reconnect();

	// Re-connection failure
	void on_failure(const mqtt::token& tok) override;

	// (Re)connection success
	// Either this or connected() can be used for callbacks.
	void on_success(const mqtt::token& tok) override;

	// (Re)connection success
	void connected(const std::string& cause) override;
	
	// Callback for when the connection is lost.
	// This will initiate the attempt to manually reconnect.
	void connection_lost(const std::string& cause) override;

	// Callback for when a message arrives.
	void message_arrived(mqtt::const_message_ptr msg) override;

	void delivery_complete(mqtt::delivery_token_ptr token) override;

public:
	void set_topic(std::string s)
	{
		_TOPIC_SUB = s;
	}
	
	callback(mqtt::async_client& cli, mqtt::connect_options& connOpts, std::string TOPIC_SUB)
				: nretry_(0), cli_(cli), connOpts_(connOpts), subListener_("Subscription"), _TOPIC_SUB(TOPIC_SUB) 
				{
					_msg_arrived = 0;
				}
				
	mqtt::const_message_ptr _msg;
	int _msg_arrived;

};


/////////////////////////////////////////////////////////////

template <class T>
class MQTT_publisher
{
	mqtt::async_client cli_pub;
	std::string _SERVER_ADDRESS;
	std::string _TOPIC_PUB;
	mqtt::connect_options connOpts_pub;
	
	bool _ok; // for synchronize publish
	T _msg; // Msg to be published
	float _periode;
   
public:
   callback_pub cb_pub;
   std::thread mqtt_thread;

   MQTT_publisher(const std::string SERVER_ADDRESS, const std::string ID_client_pub,
   				const std::string TOPIC_PUB):
   		cli_pub(SERVER_ADDRESS, ID_client_pub)
   {
   		_SERVER_ADDRESS = SERVER_ADDRESS;
   		_TOPIC_PUB = TOPIC_PUB;
   		_periode = 10;
   }
   
   MQTT_publisher(MQTT_publisher&&) = default;
   
   void connect()
   {
   	// A subscriber often wants the server to remember its messages when its
	// disconnected. In that case, it needs a unique ClientID and a
	// non-clean session.

        connOpts_pub = mqtt::connect_options_builder()
			.clean_session(true)
			.automatic_reconnect(true)
			.finalize();
        
        cli_pub.set_callback(cb_pub);
        
        try {
			cout << "\nConnecting MQTTpublisher..." << endl;
			mqtt::token_ptr conntok = cli_pub.connect(connOpts_pub);
			cout << "Waiting for the connection..." << endl;
			conntok->wait();
			cout << "  ...OK" << endl;
		}
		catch (const mqtt::exception& exc) {
        	std::cerr << "\nERROR: Unable to connect to MQTT server for pub: '"<<
        	_SERVER_ADDRESS << "'" << exc << std::endl;
        	return;
        }
   }
   
   void sync_publish();
   
   void stop_publish();
   
   void async_publish_msg(T &payload);
   
   void start(int sync);
   
	void disconnect();

};

///////////////////////////////////////////////////////////////////////////
template <class T>
class MQTT_subscriber
{
   mqtt::async_client cli_sub;
   std::string _SERVER_ADDRESS;
   std::string _TOPIC_SUB;
   mqtt::connect_options connOpts_sub;
   
public:
	callback cb_sub;

	MQTT_subscriber(const std::string SERVER_ADDRESS,const std::string ID_client_sub, std::string topic):
   		cli_sub(SERVER_ADDRESS, ID_client_sub), cb_sub(cli_sub, connOpts_sub, topic), _TOPIC_SUB(topic)
	{
		_SERVER_ADDRESS = SERVER_ADDRESS;
	}
   
	MQTT_subscriber(MQTT_subscriber&&) = default;
  
	void connect();
   
   	void get_message(T &msg);
   
    void start();
   
	void disconnect();

};

//////////////////////////////////////////////////////////////////////////

class MQTT_pubsub
{
   mqtt::async_client cli_sub;
   mqtt::async_client cli_pub;
   std::string _SERVER_ADDRESS;
   std::string _TOPIC_PUB;
   std::string _TOPIC_SUB;
   mqtt::connect_options connOpts_sub, connOpts_pub;
   
public:
   callback cb;
   callback_pub cb_pub;
   std::thread mqtt_thread;

   MQTT_pubsub(const std::string SERVER_ADDRESS,const std::string ID_client_sub,
   			const std::string ID_client_pub, const std::string TOPIC_PUB, const std::string TOPIC_SUB):
   		cli_sub(SERVER_ADDRESS, ID_client_sub),
   		cli_pub(SERVER_ADDRESS, ID_client_pub),
   		cb(cli_sub, connOpts_sub, TOPIC_SUB)
   {
   		_SERVER_ADDRESS = SERVER_ADDRESS;
   		_TOPIC_PUB = TOPIC_PUB;
   }
   
   void connect_all();
   
   void publish_msg(std::string payload);
   
   std::string get_message();
   
   void start();
   
   void disconnect_all();

};

#endif
